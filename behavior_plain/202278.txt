#202278 - Art Goddess of Entertainment, Ame no Uzume
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:35580) -> Flash of the Magatama
	Voids status ailments for 1 turn
	[1] ESNoSkyfall(127:35581) -> Dance of the Art Goddess
	No skyfall for 1 turn
	[2] ESGravity(50:35582) -> Heaven's Gate Dance
	Player -50% HP

#3: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESTargetedSkillHaste(139:35584) -> Basho Leaf Wind
	Haste random sub's skills by 2 turns
	[1] ESBindRandomSub(65:35585) -> Dance of Divine Possession
	Bind 1 random sub for 5 turns
	[2] ESEndBattle(40:35586) -> Favor of the Art Goddess
	Reduce self HP to 0

#4: ESEndPath(36:26) -> ESEndPath
end_turn