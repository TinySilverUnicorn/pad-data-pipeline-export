#202315 - divine queen hera
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESUnknown(152:40279) -> axe kick!
No description set, Deal 220% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESAbsorbCombo(67:40280) -> divine queen's breath
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Absorb damage when combos <= 10 for 5 turns, Deal 100% damage

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 11

#8: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#9: ESSpinnersFixed(110:40281) -> graviton bomb
Specific orbs change every 0.5s for 1 turn, Deal 85% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#12: ESColumnSpawnMulti(77:40282) -> giga☆grav
Change the 5th and 6th columns to Dark orbs, Deal 100% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn