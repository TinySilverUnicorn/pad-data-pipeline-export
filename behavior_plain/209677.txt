#209677 - Moosifer
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESBindAwoken(88:45018) -> Body Slam
Bind awoken skills for 1 turn, Deal 60% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESRowSpawnMulti(79:45019) -> Scorch
Change the 4th and 5th rows to Fire and Water orbs, Deal 100% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn