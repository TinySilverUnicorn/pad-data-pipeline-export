#102129 - Alt. Relentless Destroyer, Grand Tengu
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESStatusShield(20:2621) -> Here I come!
Voids status ailments for 999 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 40, target rnd 10

#7: ESBindRandom(1:2622) -> Prepare to be bound!!
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Bind 6 random cards for 4~5 turns

#8: ESAttackMultihit(15:2623) -> Get ready for destruction!!
Condition: 50% chance (ai:50 rnd:0)
Deal 140% damage (2 hits, 70% each)

#9: ESRandomSpawn(92:2624) -> I'll terminate you!!
Spawn 4 random Mortal Poison orbs, Deal 120% damage

#10: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 2, target rnd 13

#11: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b11

#12: ESInactivity66(66:2626) -> His fighting spirit increases
Do nothing

#13: ESAttackMultihit(15:2625) -> Sin Vanishment
Deal 1,200% damage (4 hits, 300% each)