#416934 - Heavenly Gleaming Priestess, Yuira
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESSkyfallLocked(96:38925) -> Crash!!
Locked random skyfall +25% for 10 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESAttackUPRemainingEnemies(17:38928) -> Heavenly Gleam's Fury
Condition: When < 50% HP , one-time use, when <= 1 enemies remain (ai:100 rnd:0) (cost: 1)
Increase damage to 150% for the next 999 turns, Deal 95% damage

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 11

#8: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#9: ESBoardChangeAttackBits(85:38926) -> Holy Flail
Change all orbs to Fire, Light, and Heal, Deal 105% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#12: ESRecoverEnemy86(86:38927) -> Holy Lightning
Enemy recover 30% HP, Deal 90% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESResolve(73:38924) -> ＿Resolve
Survive attacks with 1 HP when HP > 50%