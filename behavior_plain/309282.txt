#309282 - REMDrapurin
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESAbsorbThresholdBig(138:41294) -> Rare Pompurin Dance
Absorb damage when damage >= 1,000,000,000 for 2 turns, Deal 40% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
	[0] ESDebuffATK(130:41296) -> REMDrapurin's Dream
	ATK -50% for 5 turns
	[1] ESDebuffATKTarget(143:41297) -> REMDrapurin Power
	For 5 turns, 50% ATK for 4 random subs, Deal 100% damage

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 15

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESSpinnersFixed(110:41298) -> Purin's Silver Egg
Specific orbs change every 1.0s for 1 turn, Deal 90% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#13: ESRandomSpawn(92:41300) -> Purin's Gold Egg
Spawn 8 random Light orbs

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#16: ESSpinnersFixed(110:41299) -> Purin's Silver Egg
Specific orbs change every 1.0s for 1 turn, Deal 90% damage

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESAttributeResist(72:41292) -> ＿Rare Pompurin Dark halved
Reduce damage from Dark attrs by 50%

#19: ESSuperResolve(129:41293) -> ＿Rare Pompurin Super Resolve
Damage which would reduce HP from above 50% to below 50% is nullified