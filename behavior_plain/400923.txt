#400923 - True Elemental of True Death, Thanatos
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESUnknown(150:45919) -> Omen of the End
Condition: One-time use (ai:100 rnd:0) (cost: 1)
No description set

#3: ESSetCounterIf(38:177) -> ESSetCounterIf
set counter = 4 if counter == 0

#4: ESCountdown(37:178) -> ESCountdown
countdown

#5: ESAttackMultihit(15:45920) -> Sound of Death
Deal 100% damage (10 hits, 10% each)

#6: ESEndPath(36:26) -> ESEndPath
end_turn