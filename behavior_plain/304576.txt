#304576 - True Guardian of the Water City, Athena
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:39897) -> Guardian Deity's Blessing
	Voids status ailments for 999 turns
	[1] ESSpinnersFixed(110:39898) -> Deep Slumber
	Specific orbs change every 1.0s for 1 turn
	[2] ESVoidShieldBig(137:39899) -> Water City's Ephemerality
	Void damage >= 500,000,000 for 7 turns

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 7, target rnd 14

#5: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 3, target rnd 12

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 8

#7: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESNoSkyfall(127:39904) -> Guardian Deity's Prayer
	No skyfall for 1 turn
	[1] ESSpinnersFixed(110:39905) -> Deep Slumber
	Specific orbs change every 0.5s for 1 turn
	[2] ESBoardChangeAttackBits(85:39906) -> Apollo's Surge
	Change all orbs to Water and Wood, Deal 125% damage

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESAttackUPRemainingEnemies(17:39908) -> Guardian Deity's Wrath
	Increase damage to 150% for the next 999 turns
	[1] ESSpinnersFixed(110:39909) -> Deep Slumber
	Specific orbs change every 0.5s for 1 turn
	[2] ESBoardChangeAttackBits(85:39910) -> Apollo's Surge
	Change all orbs to Water and Wood, Deal 100% damage

#9: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESColumnSpawnMulti(77:39912) -> Ripples of the Divine Spear
	Change the 3rd column to Wood orbs and the 4th column to Water orbs, Deal 100% damage
	[1] ESUnknown(150:39913) -> Pure Light on the Water's Surface
	No description set

#10: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESColumnSpawnMulti(77:39915) -> Ripples of the Divine Spear
	Change the 1st column to Wood orbs and the 6th column to Water orbs, Deal 100% damage
	[1] ESUnknown(150:39916) -> Pure Light on the Water's Surface
	No description set

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: SkillSet:
	Condition: When < 5% HP (ai:100 rnd:0)
	[0] ESBindAwoken(88:39901) -> Vow to End the War
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:39902) -> Nike's Apteros - Roar
	Deal 1,000% damage (8 hits, 125% each)

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: SkillSet:
	[0] ESBindAwoken(88:39740) -> Azure Sky Surge
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:39741) -> Thousandfold Demise
	Deal 5,000% damage (5 hits, 1,000% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESAttributeResist(72:39731) -> Dark reduced
Reduce damage from Dark attrs by 50%