#9186 - クロコダイル
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESDebuffMovetime(39:40707) -> 少々…
	Movetime 50% for 3 turns
	[1] ESBlindStickySkyfall(128:40708) -> フザケが過ぎたな
	For 3 turns, 15% chance for skyfall orbs to be blinded for turn

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 2)
	[0] ESFixedStart(101:40710) -> …おれとお前では…
	Fix orb movement starting point to random position on the board
	[1] ESAttackUPRemainingEnemies(17:40711) -> 海賊の格が違うんだ…!!!
	Increase damage to 150% for the next 999 turns, Deal 100% damage

#7: SkillSet:
	Condition: When < 10% HP (ai:100 rnd:0)
	[0] ESBlind5(5:40713) -> 砂漠の…
	Blind all orbs on the board
	[1] ESAttackMultihit(15:40714) -> 金剛宝刀!!!!
	Deal 140% damage (4 hits, 35% each)

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 16

#9: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 13

#10: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#11: ESColumnSpawnMulti(77:40715) -> 砂漠の宝刀
Change the 1st column to Wood orbs and the 6th column to Dark orbs, Deal 100% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#14: ESSpinnersRandom(109:40716) -> 砂嵐
Random 1 orbs change every 1.0s for 1 turn, Deal 90% damage

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#17: ESOrbSealRow(100:40717) -> 砂漠の向日葵!!!
Seal the 5th row for 1 turn, Deal 105% damage

#18: ESEndPath(36:26) -> ESEndPath
end_turn