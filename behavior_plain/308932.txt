#308932 - Wang Yi
monster size: 5
new AI: True
start/max counter: 15
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESStatusShield(20:39317) -> We likely have no choice.
	Voids status ailments for 999 turns
	[1] ESSpinnersFixed(110:39318) -> Ahhh.
	Specific orbs change every 1.0s for 5 turns
	[2] ESBoardChange(84:39319) -> Shall we settle this?
	Change all orbs to Water and Dark

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 13

#7: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 8)
	[0] ESDamageShield(74:39321) -> Ahahahah...
	Reduce damage from all sources by 75% for 1 turn
	[1] ESDebuffATK(130:39322) -> Brilliantly done.
	ATK -50% for 3 turns, Deal 85% damage

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 4)
	[0] ESDamageShield(74:39324) -> Kokokoko.
	Reduce damage from all sources by 90% for 1 turn
	[1] ESAbsorbCombo(67:39325) -> What a nostalgic feeling.
	Absorb damage when combos <= 10 for 3 turns, Deal 90% damage

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESDamageShield(74:39327) -> It's truly been a long time...
	Reduce damage from all sources by 95% for 1 turn
	[1] ESAttackUPRemainingEnemies(17:39328) -> ...since my blood stirred like this.
	Increase damage to 200% for the next 999 turns, Deal 100% damage

#10: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESSkillDelay(89:39330) -> I really have no choice...
	Delay active skills by 5 turns
	[1] ESBlindStickySkyfall(128:39331) -> ...but to end you here!!
	For 5 turns, 15% chance for skyfall orbs to be blinded for turn, Deal 70% damage

#11: ESAttackMultihit(15:39333) -> It's over!!
Deal 150% damage (5 hits, 30% each)

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 2, target rnd 7

#14: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b11

#15: ESVoidShield(71:39332) -> Truly I stand on the verge of death.
Void damage >= 1,500,000,000 for 5 turns, Deal 90% damage

#16: ESEndPath(36:26) -> ESEndPath
end_turn

#17: ESSuperResolve(129:39315) -> ＿Wang Yi Super Resolve
Damage which would reduce HP from above 50% to below 50% is nullified