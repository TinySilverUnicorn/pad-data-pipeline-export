#203656 - Nargacuga
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 4)
	[0] ESStatusShield(20:41979) -> Menace
	Voids status ailments for 999 turns
	[1] ESBoardChangeAttackBits(85:41980) -> Tail Slam
	Change all orbs to Dark, Deal 110% damage

#3: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESInactivity66(66:41982) -> Its tail is stuck and can't move!
	Do nothing
	[1] ESAttackUPRemainingEnemies(17:41983) -> Enraged
	Increase damage to 150% for the next 999 turns

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#5: ESAttackMultihit(15:41989) -> Cutwing
Condition: 33% chance (ai:33 rnd:0)
Deal 102% damage (3 hits, 34% each)

#6: ESBlind62(62:41990) -> Springing Strike
Condition: 50% chance (ai:50 rnd:0)
Blind all orbs on the board, Deal 102% damage

#7: ESBindAttack(63:41991) -> Roar
Bind 2 random subs for 2 turns, Deal 100% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESRowSpawnMulti(79:41985) -> Tail Sweep
	Change the 2nd and 4th rows to Dark orbs, Deal 50% damage
	[1] ESAttackMultihit(15:41986) -> Spiked Tail Slam
	Deal 60% damage (3 hits, 20% each)

#10: ESDamageShield(74:41987) -> Dash
Condition: 50% chance (ai:50 rnd:0)
Reduce damage from all sources by 75% for 1 turn, Deal 100% damage

#11: ESRandomSpawn(92:41988) -> Tailspike Shot
Spawn 7 random Jammer orbs, Deal 101% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESAttributeResist(72:41992) -> Dark Halved
Reduce damage from Dark attrs by 50%