#706582 - Diamond REMDra (10 Pulls)
monster size: 3
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBlindStickyRandom(97:40202) -> Ka-Chak!
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Blind random 3 orbs for 3 turns

#3: ESBlindStickyRandom(97:40203) -> Ka-Chak-Chak!
Blind random 1 orbs for 10 turns, Deal 100% damage

#4: ESEndPath(36:26) -> ESEndPath
end_turn