#308294 - Reincarnated Brigid
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESStatusShield(20:44661) -> Spirit of the Sacred Fire Goddess
	Voids status ailments for 999 turns
	[1] ESAbsorbAttribute(53:44662) -> Fire Tool Reverse Technique
	Absorb Fire and Water damage for 6 turns
	[2] ESDebuffMovetime(39:44663) -> Time Forge
	Movetime 25% for 3 turns, Deal 165% damage

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 6, target rnd 16

#5: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 3, target rnd 14

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 10

#7: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESOrbLock(94:44667) -> Unyielding Flames
	Lock 15 random orbs
	[1] ESAttackMultihit(15:44668) -> Forge Blaze
	Deal 99% damage (3 hits, 33% each)

#8: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESColumnSpawnMulti(76:44670) -> Bright Flame
	Change the 1st column to Fire orbs and the 6th column to Light orbs
	[1] ESAttackMultihit(15:44671) -> Forge Blaze
	Deal 102% damage (3 hits, 34% each)

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 7

#11: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#12: ESCloud(104:44665) -> Forge Fog
A row of clouds appears for 3 turns at a random location, Deal 150% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESAttackMultihit(15:44664) -> Forge Blazing
Deal 960% damage (6 hits, 160% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: SkillSet:
	[0] ESBindAwoken(88:44525) -> Star Machine's Wave
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:44526) -> Apocalyptic Star Crushing
	Deal 5,000% damage (5 hits, 1,000% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESResolve(73:42619) -> Resolve
Survive attacks with 1 HP when HP > 50%