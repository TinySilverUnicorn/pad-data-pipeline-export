#9585 - Noriaki Kakyoin & Hierophant Green
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:43640) -> This calls for...
	Voids status ailments for 999 turns
	[1] ESAttributeBlock(107:43641) -> ...Hierophant Green.
	Unable to match Wood orbs for 2 turns, Deal 80% damage

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 15, target rnd 14

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#5: ESAttackMultihit(15:43650) -> It's time for your punishment.
Condition: 33% chance (ai:33 rnd:0)
Deal 102% damage (3 hits, 34% each)

#6: ESOrbChangeAttackBits(108:43651) -> Then what about this?
Condition: 50% chance (ai:50 rnd:0)
Change all Dark orbs to Light orbs, Deal 100% damage

#7: ESRandomSpawn(92:43652) -> Hierophant Green!!
Spawn 3 random Wood and Light orbs, Deal 100% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESOrbSealRow(100:43648) -> Hierophant's Extremities
	Seal the 1st row for 3 turns
	[1] ESRowSpawnMulti(79:43649) -> Emerald Splash
	Change the 3rd row to Wood orbs, Deal 102% damage

#10: ESAttackMultihit(15:43650) -> It's time for your punishment.
Condition: 33% chance (ai:33 rnd:0)
Deal 102% damage (3 hits, 34% each)

#11: ESOrbChangeAttackBits(108:43651) -> Then what about this?
Condition: 50% chance (ai:50 rnd:0)
Change all Dark orbs to Light orbs, Deal 100% damage

#12: ESRandomSpawn(92:43652) -> Hierophant Green!!
Spawn 3 random Wood and Light orbs, Deal 100% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 18

#15: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#16: ESBindAwoken(88:43642) -> Hierophant's Web
Bind awoken skills for 1 turn

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 2, target rnd 22

#19: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b11

#20: SkillSet:
	[0] ESColumnSpawnMulti(77:43644) -> Twenty-Meter Radius
	Change the 2nd column to Wood orbs, Deal 55% damage
	[1] ESRowSpawnMulti(79:43645) -> Emerald Splash
	Change the 3rd row to Wood orbs, Deal 55% damage

#21: ESEndPath(36:26) -> ESEndPath
end_turn

#22: ESColumnSpawnMulti(77:43646) -> My last Emerald Splash...
Change the 1st, 2nd, and 3rd columns to Wood orbs, Deal 150% damage

#23: ESSuperResolve(129:43653) -> Super resolve 50%
Damage which would reduce HP from above 50% to below 50% is nullified