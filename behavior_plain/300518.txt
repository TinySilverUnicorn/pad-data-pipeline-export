#300518 - Lil' Snow Globe Dragon Bleu
monster size: 4
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:3402) -> ESPreemptive
Enable preempt if level 4

#2: ESBranchLevel(35:25) -> ESBranchLevel
Branch on level >= 4, target rnd 4

#3: ESEndPath(36:26) -> ESEndPath
end_turn

#4: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 8

#5: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#6: SkillSet:
	[0] ESDebuffATKTarget(143:42964) -> Blue Gift
	For 1 turn, 1% ATK for 1 random sub
	[1] ESAbsorbThresholdBig(138:42965) -> Bell Bleue
	Absorb damage when damage >= 4,000,000,000 for 3 turns

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: ESRandomSpawn(92:42966) -> Lil' Gift
Spawn 10 random Water orbs, Deal 100% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn