#406958 - ブラックウィドウ
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESBlindStickyRandom(97:38901) -> ウィドウズ・バイト
Blind random 10 orbs for 1 turn

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 10

#7: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#8: ESBlindStickyFixed(98:38902) -> ウィドウズ・バイト
Blind orbs in specific positions for 1 turn, Deal 100% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#11: ESBlindStickyFixed(98:38903) -> ウィドウズ・バイト
Blind orbs in specific positions for 1 turn, Deal 100% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESAttributeResist(72:38900) -> ＿ウィドウ闇半減
Reduce damage from Dark attrs by 50%