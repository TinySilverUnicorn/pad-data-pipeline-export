#4604 - Vraska
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESUnknown(152:48069) -> 死の呪文
Condition: One-time use (ai:100 rnd:0) (cost: 1)
No description set

#3: ESAttackMultihit(15:48070) -> 貴重な発見
Condition: 25% chance (ai:0 rnd:25)
Deal 102% damage (3 hits, 34% each)

#4: ESOrbLock(94:48071) -> 石化に関わる魔法
Condition: 25% chance (ai:0 rnd:25)
Lock all Wood orbs, Deal 100% damage

#5: ESBlind62(62:48072) -> 隠密行動
Condition: 25% chance (ai:0 rnd:25)
Blind all orbs on the board, Deal 101% damage

#6: ESRandomSpawn(92:48073) -> 死をもたらす力
Condition: 25% chance (ai:0 rnd:25)
Spawn 4 random Poison orbs, Deal 100% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: ESAttributeResist(72:48074) -> 木半減
Reduce damage from Wood attrs by 50%