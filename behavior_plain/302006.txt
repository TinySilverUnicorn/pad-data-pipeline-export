#302006 - True Avowed Thief, Ishikawa Goemon
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESStatusShield(20:45643) -> Tenka Gomen
	Voids status ailments for 999 turns
	[1] ESVoidShieldBig(137:45644) -> Golden Dragon Scales
	Void damage >= 1,000,000,000 for 7 turns
	[2] ESBoardChangeAttackBits(85:45645) -> True Ninja Art Fireworks
	Change all orbs to Fire, Jammer, and Poison, Deal 100% damage

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 7, target rnd 16

#5: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 3, target rnd 14

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 10

#7: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESUnknown(151:45649) -> Golden Coins
	No description set
	[1] ESAttackMultihit(15:45650) -> Coin Barrage
	Deal 75~125% damage (3~5 hits, 25% each)

#8: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESOrbLock(94:45652) -> Golden Coins
	Lock 15 random orbs
	[1] ESBlind62(62:45653) -> Grand Smokescreen
	Blind all orbs on the board, Deal 100% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 7

#11: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#12: ESBoardChangeAttackBits(85:45647) -> Golden Pipe
Change all orbs to Fire, Deal 150% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESAttackMultihit(15:45646) -> Extreme Coin Barrage
Deal 996% damage (6 hits, 166% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: SkillSet:
	[0] ESBindAwoken(88:45533) -> Wave of a Trillion Evil Omens
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:45534) -> Doom of a Trillion Catastrophes
	Deal 5,000% damage (5 hits, 1,000% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESResolve(73:42619) -> Resolve
Survive attacks with 1 HP when HP > 50%