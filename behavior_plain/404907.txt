#404907 - Masked Rider 1
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESAbsorbCombo(67:43505) -> Rider Fight
	Absorb damage when combos <= 6 for 3 turns
	[1] ESSkyfallLocked(96:43506) -> Rider Jump
	Locked Wood skyfall +100% for 3 turns

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#4: ESDebuffATK(130:43394) -> Rider Kick
Condition: 25% chance (ai:0 rnd:25)
ATK -50% for 1 turn, Deal 100% damage

#5: ESAttackMultihit(15:43395) -> Rider Punch
Condition: 25% chance (ai:0 rnd:25)
Deal 100% damage

#6: ESTargetedSkillDelay(140:43396) -> Rider Chop
Condition: 25% chance (ai:0 rnd:25)
Delay both leaders' skills by 1 turn, Deal 100% damage

#7: ESBlindStickyRandom(97:43397) -> Rider Scissors
Condition: 25% chance (ai:0 rnd:25)
Blind random 4 orbs for 1 turn, Deal 101% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESColumnSpawnMulti(77:43392) -> Lightning Rider Kick
	Change the 1st and 6th columns to Wood orbs, Deal 101% damage
	[1] ESUnknown(151:43393) -> Rider Reversal
	No description set

#10: ESDebuffATK(130:43394) -> Rider Kick
Condition: 25% chance (ai:0 rnd:25)
ATK -50% for 1 turn, Deal 100% damage

#11: ESAttackMultihit(15:43395) -> Rider Punch
Condition: 25% chance (ai:0 rnd:25)
Deal 100% damage

#12: ESTargetedSkillDelay(140:43396) -> Rider Chop
Condition: 25% chance (ai:0 rnd:25)
Delay both leaders' skills by 1 turn, Deal 100% damage

#13: ESBlindStickyRandom(97:43397) -> Rider Scissors
Condition: 25% chance (ai:0 rnd:25)
Blind random 4 orbs for 1 turn, Deal 101% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESAttributeResist(72:43399) -> Wood Reduced
Reduce damage from Wood attrs by 50%

#16: ESDeathCry(69:43398) -> HENSHIN
Condition: On death (ai:0 rnd:0)
Show message: I'll never become the person you want me to be...|...even if I die!