#409106 - ザ・マイティ・ソー
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESSkyfall(68:38914) -> 雷の女神
Light skyfall +15% for 3 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESRandomSpawn(92:38915) -> 受け継がれたムジョルニア
Spawn 8 random Light orbs, Deal 100% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: ESAttributeResist(72:38913) -> ＿女ソー光半減
Reduce damage from Light attrs by 50%