#8932 - Wang Yi
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESStatusShield(20:39264) -> We likely have no choice.
Voids status ailments for 999 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 3, target rnd 18

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 15

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: SkillSet:
	[0] ESDamageShield(74:39266) -> Ahahahah...
	Reduce damage from all sources by 75% for 1 turn
	[1] ESDebuffATK(130:39267) -> Brilliantly done.
	ATK -50% for 1 turn, Deal 85% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#13: SkillSet:
	[0] ESDamageShield(74:39269) -> Kokokoko.
	Reduce damage from all sources by 90% for 1 turn
	[1] ESAbsorbCombo(67:39270) -> What a nostalgic feeling.
	Absorb damage when combos <= 8 for 1 turn, Deal 90% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#16: SkillSet:
	[0] ESDamageShield(74:39272) -> It's truly been a long time...
	Reduce damage from all sources by 95% for 1 turn
	[1] ESAttackUPRemainingEnemies(17:39273) -> ...since my blood stirred like this.
	Increase damage to 150% for the next 1 turn, Deal 100% damage

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#19: SkillSet:
	[0] ESSkillDelay(89:39275) -> I really have no choice...
	Delay active skills by 3 turns
	[1] ESBlindStickySkyfall(128:39276) -> ...but to end you here!!
	For 3 turns, 15% chance for skyfall orbs to be blinded for turn, Deal 80% damage

#20: ESEndPath(36:26) -> ESEndPath
end_turn

#21: ESResolve(73:39263) -> ＿Wang Yi Resolve
Survive attacks with 1 HP when HP > 50%