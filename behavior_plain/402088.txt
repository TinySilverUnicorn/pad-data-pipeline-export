#402088 - My Melody & Brachys
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESDebuffRCV(105:41347) -> Melody Wink
	RCV 25% for 2 turns
	[1] ESSkyfall(68:41348) -> The Energy of Kindness
	Wood skyfall +20% for 2 turns, Deal 90% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 10

#7: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#8: ESUnknown(151:41349) -> Gaia Breath
No description set, Deal 85% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#11: ESAttackMultihit(15:41350) -> Combination Attack
Deal 110% damage (2 hits, 55% each)

#12: ESEndPath(36:26) -> ESEndPath
end_turn