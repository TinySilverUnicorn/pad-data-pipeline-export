#508233 - 生滅の孔雀王・ユリシャ
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESStatusShield(20:32490) -> 孔雀王の結界
	Voids status ailments for 999 turns
	[1] ESNoSkyfall(127:32491) -> 災難即滅の羽根
	No skyfall for 99 turns
	[2] ESDisableAssists(141:32492) -> 孔雀の眼光
	Disable active skills for 7 turns
	[3] ESBlindStickySkyfall(128:32493) -> 満願成就の羽根
	For 7 turns, 20% chance for skyfall orbs to be blinded for turn

#3: SkillSet:
	[0] ESBindAwoken(88:32462) -> 孔雀の波動
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:32463) -> 孔雀王の大転変
	Deal 10,000% damage (5 hits, 2,000% each)

#4: ESEndPath(36:26) -> ESEndPath
end_turn

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESResolve(73:32500) -> 根性
Survive attacks with 1 HP when HP > 5%