#606361 - Reincarnated Nohime
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBoardSizeChange(126:39744) -> I'll do something nice for you.
Condition: One-time use (ai:100 rnd:0) (cost: 4)
Change board size to 7x6 for 2 turns

#3: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 4)
	[0] ESBindAwoken(88:39746) -> That looks like a lot of Orbs.
	Bind awoken skills for 1 turn
	[1] ESOrbSealColumn(99:39747) -> Since you have so much leeway...
	Seal the 1st column for 15 turns
	[2] ESCloud(104:39748) -> Would you forgive me if I did this?
	A row of clouds appears for 15 turns at 1st row, 1st column

#4: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#5: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 4, target rnd 19

#6: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:39750) -> Hehehehe...
	Voids status ailments for 999 turns
	[1] ESNoSkyfall(127:39751) -> I want to see you spare no effort!
	No skyfall for 1 turn
	[2] ESAbsorbCombo(67:39752) -> I wonder if you can overcome this...
	Absorb damage when combos <= 15 for 1 turn

#7: ESBranchCombo(113:6528) -> ESBranchCombo
Branch on combo >= 16, target rnd 16

#8: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 16

#9: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 13

#10: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#11: SkillSet:
	[0] ESSkillDelay(89:39754) -> Oh, what a shame.
	Delay active skills by 2 turns
	[1] ESVoidShieldBig(137:39755) -> I'll give you one last chance.
	Void damage >= 500,000,000 for 1 turn
	[2] ESAbsorbCombo(67:39756) -> I wonder if you can overcome this...
	Absorb damage when combos <= 10 for 1 turn

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESBranchCombo(113:6528) -> ESBranchCombo
Branch on combo >= 11, target rnd 16

#14: ESAttackMultihit(15:39761) -> You've entertained me.
Deal 1,000% damage (4 hits, 250% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESTargetedSkillHaste(139:39758) -> Hehehehe...
	Haste all cards' skills by 2 turns
	[1] ESUnknown(151:39759) -> You're pretty good, aren't you?
	No description set
	[2] ESAttackUPRemainingEnemies(17:39760) -> I will finish you with this.
	Increase damage to 1,000% for the next 1 turn

#17: ESAttackMultihit(15:39761) -> You've entertained me.
Deal 1,000% damage (4 hits, 250% each)

#18: ESEndPath(36:26) -> ESEndPath
end_turn

#19: SkillSet:
	[0] ESBindAwoken(88:39740) -> Azure Sky Surge
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:39741) -> Thousandfold Demise
	Deal 5,000% damage (5 hits, 1,000% each)

#20: ESEndPath(36:26) -> ESEndPath
end_turn

#21: ESSuperResolve(129:39737) -> Super resolve
Damage which would reduce HP from above 50% to below 50% is nullified

#22: ESAttributeResist(72:39730) -> Light reduced
Reduce damage from Light attrs by 50%

#23: SkillSet:
	Condition: On death (ai:0 rnd:0)
	[0] ESPoisonChangeRandomCount(60:39729) -> I curse you...
	Change 1 random orb to Poison orbs