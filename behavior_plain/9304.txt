#9304 - Magnamalo
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:42177) -> Roar
	Voids status ailments for 999 turns
	[1] ESAbsorbAttribute(53:42178) -> Hellfire Burst
	Absorb Fire and Dark damage for 5 turns
	[2] ESDisableAssists(141:42179) -> Hellfire Barrage
	Disable active skills for 3 turns

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 15, target rnd 12

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 8

#5: ESAttackMultihit(15:42191) -> Bite
Condition: 50% chance (ai:50 rnd:0)
Deal 102% damage (3 hits, 34% each)

#6: ESRandomSpawn(92:42192) -> Tail Stab
Spawn 5 random Fire and Dark orbs, Deal 101% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESColumnSpawnMulti(77:42185) -> Hellfire Spiral Tail Stab
	Change the 1st, 3rd, and 5th columns to Fire orbs and the 2nd, 4th, and 6th columns to Dark orbs, Deal 52% damage
	[1] ESSpinnersFixed(110:42186) -> Claw Swipe
	Specific orbs change every 1.0s for 3 turns, Deal 52% damage

#9: ESVoidShield(71:42187) -> Rolling Arm Slam
Condition: 50% chance (ai:50 rnd:0)
Void damage >= 1,000,000,000 for 1 turn, Deal 100% damage

#10: SkillSet:
	[0] ESBlind62(62:42189) -> Hellfire Divebomb
	Blind all orbs on the board, Deal 50% damage
	[1] ESDebuffATKTarget(143:42190) -> Arm Slam
	For 1 turn, 5% ATK for 2 random cards, Deal 51% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 16

#13: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#14: SkillSet:
	[0] ESBindAwoken(88:42181) -> Diving Tackle
	Bind awoken skills for 1 turn
	[1] ESBombRandomSpawn(102:42182) -> Hellfire Blastbite
	Spawn 15 random Bomb orbs

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESAttackMultihit(15:42183) -> Hellfire Soul Vortex
Deal 240% damage (3 hits, 80% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESSuperResolve(129:42193) -> Super Resolve 50%
Damage which would reduce HP from above 50% to below 50% is nullified