#9589 - Trish Una & Spicy Lady
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESUnknown(151:43671) -> Soften objects.
Condition: One-time use (ai:100 rnd:0) (cost: 2)
No description set

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 10, target rnd 12

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 8

#5: ESAttackMultihit(15:43676) -> Destroy it!!
Condition: 50% chance (ai:50 rnd:0)
Deal 102% damage (3 hits, 34% each)

#6: ESOrbLock(94:43677) -> I feel different than before.
Lock all Light and Dark orbs, Deal 100% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESRecoverEnemy86(86:43673) -> No one will be...
	Enemy recover 30% HP
	[1] ESDamageShield(74:43674) -> ...beyond saving.
	Reduce damage from all sources by 50% for 3 turns, Deal 101% damage

#9: ESAttackMultihit(15:43676) -> Destroy it!!
Condition: 50% chance (ai:50 rnd:0)
Deal 102% damage (3 hits, 34% each)

#10: ESOrbLock(94:43677) -> I feel different than before.
Lock all Light and Dark orbs, Deal 100% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESAttackMultihit(15:43675) -> Arrivederci.
Deal 135% damage (3 hits, 45% each)

#13: ESEndPath(36:26) -> ESEndPath
end_turn