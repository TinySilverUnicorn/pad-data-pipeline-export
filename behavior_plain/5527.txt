#5527 - Furious Rajang
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESSpinnersFixed(110:42108) -> Roar
	Specific orbs change every 1.0s for 3 turns
	[1] ESDamageShield(74:42109) -> Strongarm State
	Reduce damage from all sources by 50% for 2 turns

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 7

#4: ESAttackMultihit(15:42120) -> Arm Swing
Condition: 50% chance (ai:50 rnd:0)
Deal 102% damage (3 hits, 34% each)

#5: ESFixedStart(101:42121) -> High Speed Tackle
Fix orb movement starting point to random position on the board, Deal 103% damage

#6: ESEndPath(36:26) -> ESEndPath
end_turn

#7: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESBindAttack(63:42111) -> High Speed Binding Attack
	Bind player leader for 2 turns, Deal 50% damage
	[1] ESBoardChangeAttackBits(85:42112) -> Photon Breath
	Change all orbs to Wood, Light, and Jammer, Deal 55% damage

#8: SkillSet:
	Condition: 33% chance (ai:33 rnd:0)
	[0] ESOrbChangeAttackBits(108:42114) -> Thunderball
	Change all Wood and Dark orbs to Light orbs, Deal 100% damage
	[1] ESVoidShield(71:42115) -> Back Step
	Void damage >= 700,000,000 for 1 turn

#9: SkillSet:
	Condition: 50% chance (ai:50 rnd:0)
	[0] ESOrbChangeAttackBits(108:42117) -> Arena Throw
	Change all Fire and Heal orbs to Jammer orbs, Deal 50% damage
	[1] ESBlindStickyRandom(97:42118) -> Body Press
	Blind random 9 orbs for 3 turns, Deal 55% damage

#10: ESOrbSealRow(100:42119) -> Body Spin
Seal the 1st row for 1 turn, Deal 102% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESAttributeResist(72:42122) -> Wood Light Damage Reduced
Reduce damage from Wood and Light attrs by 50%