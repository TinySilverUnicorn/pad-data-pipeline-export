#9169 - ブルック
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESVoidShield(71:40678) -> パーティーミュージック!!
Void damage >= 100,000,000 for 3 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 2)
	[0] ESBlindStickyFixed(98:40680) -> お寒うござんす
	Blind orbs in specific positions for 1 turn
	[1] ESSpinnersFixed(110:40681) -> お気をつけて
	Specific orbs change every 1.0s for 1 turn, Deal 60% damage

#7: ESRandomSpawn(92:40682) -> 掠り唄 吹雪斬り
Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
Spawn 12 random Water orbs, Deal 120% damage

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 16

#9: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 13

#10: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#11: ESBlindStickyFixed(98:40683) -> ハイ もう斬っちゃいました!!
Blind orbs in specific positions for 1 turn

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#14: SkillSet:
	[0] ESSpinnersFixed(110:40685) -> キントーティアス
	Specific orbs change every 1.0s for 1 turn
	[1] ESAttackMultihit(15:40686) -> 幻想曲
	Deal 140% damage (4 hits, 35% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#17: SkillSet:
	[0] ESOrbLock(94:40688) -> 「凍える剣」!!
	Lock all orbs
	[1] ESSkyfallLocked(96:40689) -> コツとご覧あれ!!!
	Locked random skyfall +100% for 1 turn, Deal 100% damage

#18: ESEndPath(36:26) -> ESEndPath
end_turn