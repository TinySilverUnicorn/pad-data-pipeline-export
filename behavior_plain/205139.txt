#205139 - Mega Awoken Norn of the Future, Skuld
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESVoidShield(71:42464) -> Norn's Protection
	Void damage >= 300,000,000 for 2 turns
	[1] ESAbsorbAttribute(53:42465) -> Foreknowledge of the Future
	Absorb Water, Light, and Dark damage for 2 turns

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 6

#4: ESRandomSpawn(92:42467) -> Water Surge
Spawn 3 random Water orbs, Deal 100% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBoardChangeAttackBits(85:42466) -> Space-Time Rending Circle
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Change all orbs to Water, Deal 100% damage

#7: ESRandomSpawn(92:42467) -> Water Surge
Spawn 3 random Water orbs, Deal 100% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: ESSuperResolve(129:42468) -> Super Resolve
Damage which would reduce HP from above 40% to below 40% is nullified