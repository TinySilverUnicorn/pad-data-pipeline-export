#4600 - Nicol Bolas
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:48094) -> 生と死は取り替えが利く
	Voids status ailments for 999 turns
	[1] ESBoardChangeAttackBits(85:48095) -> 青、黒、赤の呪文
	Change all orbs to Fire, Water, and Dark, Deal 100% damage

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 8

#4: ESAttackMultihit(15:48099) -> 破滅の刻
Condition: 33% chance (ai:33 rnd:0)
Deal 102% damage (3 hits, 34% each)

#5: ESAbsorbAttribute(53:48100) -> 混沌と対立
Condition: 50% chance (ai:50 rnd:0)
Absorb Light damage for 1 turn, Deal 101% damage

#6: ESRowSpawnMulti(79:48101) -> もはや救済は不可能
Change the 3rd row to Dark orbs, Deal 100% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESDebuffMovetime(39:48097) -> 次の策謀
	Movetime 50% for 3 turns
	[1] ESDebuffATKTarget(143:48098) -> 悪逆なる破壊の力
	For 1 turn, 50% ATK for both leaders, Deal 101% damage

#9: ESAttackMultihit(15:48099) -> 破滅の刻
Condition: 33% chance (ai:33 rnd:0)
Deal 102% damage (3 hits, 34% each)

#10: ESAbsorbAttribute(53:48100) -> 混沌と対立
Condition: 50% chance (ai:50 rnd:0)
Absorb Light damage for 1 turn, Deal 101% damage

#11: ESRowSpawnMulti(79:48101) -> もはや救済は不可能
Change the 3rd row to Dark orbs, Deal 100% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESTypeResist(118:48102) -> 攻撃半減
Reduce damage from Attacker types by 50%