#9544 - Joseph Joestar
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESAbsorbCombo(67:43617) -> Let me show you!
	Absorb damage when combos <= 5 for 3 turns
	[1] ESSpinnersFixed(110:43618) -> Attack drawing a trajectory from all directions.
	Specific orbs change every 1.0s for 1 turn, Deal 80% damage

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#4: SkillSet:
	Condition: 25% chance (ai:0 rnd:25)
	[0] ESColumnSpawnMulti(77:43623) -> That was...
	Change the 1st column to Wood orbs, Deal 50% damage
	[1] ESColumnSpawnMulti(77:43624) -> ...my Clacker Boomerang!
	Change the 6th column to Dark orbs, Deal 50% damage

#5: ESRandomSpawn(92:43625) -> Ata!
Condition: 25% chance (ai:0 rnd:25)
Spawn 4 random Wood orbs, Deal 100% damage

#6: ESAttackMultihit(15:43626) -> I call it my Hamon Clacker Volley.
Condition: 25% chance (ai:0 rnd:25)
Deal 102% damage (3 hits, 34% each)

#7: ESAttackMultihit(15:43627) -> Take this!!
Condition: 25% chance (ai:0 rnd:25)
Deal 101% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESBlind62(62:43620) -> Me?
	Blind all orbs on the board, Deal 100% damage
	[1] ESAttackUPRemainingEnemies(17:43621) -> I'm dead serious.
	Increase damage to 150% for the next 999 turns

#10: SkillSet:
	Condition: 25% chance (ai:0 rnd:25)
	[0] ESColumnSpawnMulti(77:43623) -> That was...
	Change the 1st column to Wood orbs, Deal 50% damage
	[1] ESColumnSpawnMulti(77:43624) -> ...my Clacker Boomerang!
	Change the 6th column to Dark orbs, Deal 50% damage

#11: ESRandomSpawn(92:43625) -> Ata!
Condition: 25% chance (ai:0 rnd:25)
Spawn 4 random Wood orbs, Deal 100% damage

#12: ESAttackMultihit(15:43626) -> I call it my Hamon Clacker Volley.
Condition: 25% chance (ai:0 rnd:25)
Deal 102% damage (3 hits, 34% each)

#13: ESAttackMultihit(15:43627) -> Take this!!
Condition: 25% chance (ai:0 rnd:25)
Deal 101% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn