#202299 - Alt. Fire PreDRA
monster size: 3
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESDisableAssists(141:39435) -> Brilliant Fire
Condition: One-time use (ai:100 rnd:0) (cost: 2)
Disable active skills for 6 turns

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 11

#5: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 4, target rnd 9

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 8

#7: ESChangeAttribute(46:39438) -> Brilliant Change
Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESDebuffATKTarget(143:39439) -> Brilliant Attack
Condition: One-time use (ai:100 rnd:0) (cost: 1)
For 1 turn, 1% ATK for 2 random cards, Deal 1,000% damage

#12: ESChangeAttribute(46:39438) -> Brilliant Change
Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESSuperResolve(129:39457) -> ＿Super resolve
Damage which would reduce HP from above 50% to below 50% is nullified