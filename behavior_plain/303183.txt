#303183 - True Flame Spore Crusher, Hammer Troll
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBindSkill(14:39773) -> Knockdown
Condition: One-time use (ai:100 rnd:0) (cost: 2)
Bind active skills for 5 turns, Deal 40% damage

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 7, target rnd 16

#5: ESBranchRemainingEnemies(120:8595) -> ESBranchRemainingEnemies
Branch on remaining enemies <= 1, target rnd 12

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 8

#7: ESVoidShieldBig(137:39780) -> Troll Barrier
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Void damage >= 500,000,000 for 999 turns

#8: ESDamageShield(74:39781) -> Troll Barrier
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Reduce damage from all sources by 50% for 999 turns

#9: ESTargetedSkillHaste(139:39774) -> Troll Dance
Condition: 50% chance (ai:0 rnd:50)
Haste both leaders' skills by 1 turn, Deal 100% damage

#10: ESSkillDelay(89:39775) -> Troll Dance
Condition: 50% chance (ai:0 rnd:50)
Delay active skills by 1~2 turns, Deal 100% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 9

#13: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#14: SkillSet:
	[0] ESCloud(104:39777) -> Leaves Turning to Clouds
	A row of clouds appears for 3 turns at a random location
	[1] ESUnknown(151:39778) -> Ashflame Shackles
	No description set
	[2] ESAttackUPRemainingEnemies(17:39779) -> Ignis Enhancement
	Increase damage to 1,000% for the next 1 turn

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESAttackMultihit(15:39741) -> Thousandfold Demise
Deal 5,000% damage (5 hits, 1,000% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn