#403288 - Alt. Frostmare
monster size: 2
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESOrbLock(94:41500) -> Freezing Thorns
Lock all orbs

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchRemainingEnemies(120:8595) -> ESBranchRemainingEnemies
Branch on remaining enemies <= 1, target rnd 10

#7: ESSkyfallLocked(96:41501) -> Powder Snow
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Locked random skyfall +100% for 2 turns, Deal 93% damage

#8: ESAttackMultihit(15:41502) -> Nightmare Barrage
Deal 88% damage (4 hits, 22% each)

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 2, target rnd 7

#11: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b11

#12: ESSkillDelay(89:41503) -> Glacial Solitude
Delay active skills by 5 turns, Deal 100% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn