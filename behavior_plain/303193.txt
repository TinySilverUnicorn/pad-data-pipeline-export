#303193 - True Guardian of the Imperial Capital, Athena
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:39876) -> Guardian Deity's Blessing
	Voids status ailments for 999 turns
	[1] ESBlindStickySkyfall(128:39877) -> Shadowed Empire
	For 1 turn, 30% chance for skyfall orbs to be blinded for turn
	[2] ESAbsorbCombo(67:39878) -> Sanctuary
	Absorb damage when combos <= 10 for 7 turns

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 7, target rnd 14

#5: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 3, target rnd 12

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 8

#7: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESAttributeBlock(107:39883) -> All-Seeing Evil Darkness
	Unable to match Dark orbs for 5 turns
	[1] ESUnknown(151:39884) -> Underworld Enmity
	No description set
	[2] ESDebuffMovetime(39:39885) -> Gorgon's Stare
	Movetime 25% for 1 turn, Deal 100% damage

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESAttributeBlock(107:39887) -> All-Seeing Evil Light
	Unable to match Light orbs for 5 turns
	[1] ESUnknown(151:39888) -> Underworld Enmity
	No description set
	[2] ESDebuffRCV(105:39889) -> Gorgon's Stare
	RCV 25% for 1 turn, Deal 75% damage

#9: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESSpinnersFixed(110:39891) -> Encirclement
	Specific orbs change every 1.0s for 1 turn
	[1] ESDebuffATKTarget(143:39892) -> Victory Song
	For 1 turn, 1% ATK for both leaders, Deal 100% damage

#10: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESOrbLock(94:39894) -> Besiegement
	Lock 15 random orbs
	[1] ESDebuffATKTarget(143:39895) -> Victory Song
	For 1 turn, 1% ATK for both leaders, Deal 100% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: SkillSet:
	[0] ESBindAwoken(88:39880) -> Vow to End the War
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:39881) -> Aegis Crescenda
	Deal 1,000% damage (8 hits, 125% each)

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: SkillSet:
	[0] ESBindAwoken(88:39740) -> Azure Sky Surge
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:39741) -> Thousandfold Demise
	Deal 5,000% damage (5 hits, 1,000% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESAttributeResist(72:39730) -> Light reduced
Reduce damage from Light attrs by 50%