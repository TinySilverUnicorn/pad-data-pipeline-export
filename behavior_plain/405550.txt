#405550 - Reincarnated Xiahou Dun
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESStatusShield(20:42694) -> Spirit of the Brutal Warrior General
	Voids status ailments for 999 turns
	[1] ESBoardChange(84:42695) -> Lively Scowl
	Change all orbs to Fire, Wood, and Dark
	[2] ESAttributeBlock(107:42696) -> Darkflame Stroke
	Unable to match Fire and Dark orbs for 3 turns, Deal 100% damage

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 7, target rnd 17

#5: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 13

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 6, target rnd 11

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 3, target rnd 11

#8: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESRandomSpawn(92:42700) -> Raging Flames
	Spawn 3 random Fire orbs
	[1] ESUnknown(151:42701) -> Dragonflame Saber
	No description set, Deal 100% damage

#9: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESRandomSpawn(92:42703) -> Raging Flames
	Spawn 3 random Fire orbs
	[1] ESOrbLock(94:42704) -> Dragonflame Saber
	Lock all Fire orbs, Deal 100% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: SkillSet:
	[0] ESRandomSpawn(92:42706) -> Raging Flames
	Spawn 3 random Fire orbs
	[1] ESAttributeBlock(107:42707) -> Dragonflame Saber
	Unable to match Fire orbs for 1 turn, Deal 100% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#14: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#15: ESVoidShieldBig(137:42698) -> Burning Frenzy
Void damage >= 1,000,000,000 for 999 turns, Deal 125% damage

#16: ESEndPath(36:26) -> ESEndPath
end_turn

#17: SkillSet:
	[0] ESBindAwoken(88:42623) -> Longevity Surge
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:42624) -> Universal Demise
	Deal 5,000% damage (5 hits, 1,000% each)

#18: ESEndPath(36:26) -> ESEndPath
end_turn

#19: SkillSet:
	Condition: On death (ai:0 rnd:0)
	[0] ESBindSkill(14:42762) -> Tch... You got me...
	Bind active skills for 5 turns