#309549 - Jotaro Kujo & Star Platinum The World
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:43894) -> Star Platinum
	Voids status ailments for 999 turns
	[1] ESUnknown(150:43895) -> Bring it!
	No description set
	[2] ESAbsorbAttribute(53:43896) -> Star Finger
	Absorb Light and Dark damage for 5 turns

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 15, target rnd 12

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 8

#5: ESAttackMultihit(15:43904) -> Punch Rush
Condition: 50% chance (ai:50 rnd:0)
Deal 102% damage (3 hits, 34% each)

#6: ESRandomSpawn(92:43905) -> Ora
Spawn 2 random Light, Dark, and Jammer orbs, Deal 101% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESVoidShield(71:43900) -> Good grief.
	Void damage >= 10,000,000 for 2 turns
	[1] ESAbsorbCombo(67:43901) -> You pissed me off.
	Absorb damage when combos <= 7 for 2 turns, Deal 101% damage

#9: ESSpinnersRandom(109:43902) -> Time is moving again.
Condition: 50% chance (ai:50 rnd:0)
Random 1 orbs change every 0.5s for 1 turn, Deal 100% damage

#10: ESDebuffATK(130:43903) -> Ora!
ATK -50% for 1 turn, Deal 100% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 16

#13: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#14: ESFixedStart(101:43897) -> Star Platinum. The World.
Fix orb movement starting point to random position on the board, Deal 103% damage

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESAttackMultihit(15:43898) -> Ora ora ora ora ora ora ora
Deal 180% damage (3 hits, 60% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESSuperResolve(129:43906) -> Super Resolve 50%
Damage which would reduce HP from above 50% to below 50% is nullified