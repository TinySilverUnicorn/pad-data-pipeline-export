#403222 - Crimson Orchid Black Witch, Xiang Mei
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESAbsorbCombo(67:41429) -> What a coincidence seeing you here.
	Absorb damage when combos <= 9 for 3 turns
	[1] ESDispel(6:41430) -> Still, it'll be our little secret, okay?
	Voids player buff effects, Deal 100% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 14

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 11

#8: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#9: ESDebuffRCV(105:41431) -> Violet Orchid Moon
RCV 0% for 1 turn, Deal 190% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#12: ESSkyfall(68:41432) -> Twilight Black Magic
Dark skyfall +20% for 1 turn, Deal 200% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#15: ESBlindStickyFixed(98:41433) -> Mystic Broom
Blind orbs in specific positions for 1 turn, Deal 195% damage

#16: ESEndPath(36:26) -> ESEndPath
end_turn