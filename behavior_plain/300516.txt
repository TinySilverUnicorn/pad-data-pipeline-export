#300516 - Duke Vampire Lord
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESStatusShield(20:45631) -> Protection of Darkness
	Voids status ailments for 999 turns
	[1] ESVoidShieldBig(137:45632) -> Duke's Armor
	Void damage >= 1,000,000,000 for 4 turns
	[2] ESCloud(104:45633) -> Bloody Cloud
	A 3×1 rectangle of clouds appears for 4 turns at a random location, Deal 130% damage

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 7, target rnd 16

#5: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 3, target rnd 14

#6: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 4, target rnd 10

#7: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESBlindStickyRandom(97:45637) -> Blood Spear
	Blind random 7 orbs for 1 turn
	[1] ESRecoverEnemy86(86:45638) -> Draining Fangs
	Enemy recover 10% HP, Deal 100% damage

#8: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESUnknown(151:45640) -> Blood Spear
	No description set
	[1] ESRecoverEnemy86(86:45641) -> Draining Fangs
	Enemy recover 5%~15% HP, Deal 80% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 7

#11: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#12: ESAbsorbThresholdBig(138:45635) -> Power of Darkness
Absorb damage when damage >= 1,000,000,000 for 3 turns

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESAttackMultihit(15:45634) -> Bloody Nightmare
Deal 1,000% damage (5 hits, 200% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: SkillSet:
	[0] ESBindAwoken(88:45533) -> Wave of a Trillion Evil Omens
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:45534) -> Doom of a Trillion Catastrophes
	Deal 5,000% damage (5 hits, 1,000% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESResolve(73:42619) -> Resolve
Survive attacks with 1 HP when HP > 50%