#409272 - Colorful Dragon Caller, Ideal Cinnamon
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESDebuffATK(130:41360) -> Memories of Cinnamon
ATK -75% for 3 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESSkillDelay(89:41361) -> Nap Time
Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
Delay active skills by 3 turns, Deal 110% damage

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 15

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESBoardChangeAttackBits(85:41362) -> Happiness of Cinnamon
Change all orbs to Fire, Water, Wood, Light, and Dark, Deal 105% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#13: ESChangeAttribute(46:41363) -> Heart of the Colorful Dragon
Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#16: ESCloud(104:41364) -> Flying Cinnamon
A 2×2 square of clouds appears for 1 turn at a random location, Deal 90% damage

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESSuperResolve(129:41359) -> ＿ Cinnamon Super Resolve
Damage which would reduce HP from above 50% to below 50% is nullified