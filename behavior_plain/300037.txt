#300037 - Alt. Big Flamie
monster size: 2
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESDebuffMovetime(39:45500) -> Flaring Up
Condition: One-time use (ai:100 rnd:0) (cost: 2)
Movetime 25% for 3 turns

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 7, target rnd 12

#5: ESBranchRemainingEnemies(120:8595) -> ESBranchRemainingEnemies
Branch on remaining enemies <= 2, target rnd 10

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 4, target rnd 9

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 9

#8: ESChangeAttribute(46:45505) -> Big Lil' Flash
Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESAttackUPCooldown(19:45536) -> Release of a Trillion Evil Omens
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Increase damage to 1,000% for the next 999 turns

#11: ESChangeAttribute(46:45505) -> Big Lil' Flash
Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage

#12: ESAttackMultihit(15:45534) -> Doom of a Trillion Catastrophes
Deal 5,000% damage (5 hits, 1,000% each)

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESResolve(73:42619) -> Resolve
Survive attacks with 1 HP when HP > 50%