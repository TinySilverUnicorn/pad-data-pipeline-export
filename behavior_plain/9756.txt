#9756 - ジ・O
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESUnknown(152:46173) -> 不愉快だな………この感覚は！
No description set

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
	[0] ESSkillDelay(89:46178) -> プレッシャー
	Delay active skills by 1 turn
	[1] ESOrbSealRow(100:46179) -> 道を誤ったのだよ！貴様は！！
	Seal the 1st row for 5 turns

#7: SkillSet:
	Condition: When < 20% HP (ai:100 rnd:0)
	[0] ESInactivity66(66:46175) -> ジ………ジ・Ｏ！
	Do nothing
	[1] ESFixedStart(101:46176) -> 何故動かん………！
	Fix orb movement starting point to random position on the board, Deal 20% damage

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 16

#9: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 13

#10: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#11: ESBoardChangeAttackBits(85:46180) -> 墜ちろ！カトンボッ！！
Change all orbs to Light, Dark, Heal, and Jammer, Deal 80% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#14: ESRowSpawnMulti(79:46181) -> ビーム・ライフル
Change the 1st, 2nd, and 3rd rows to Light and Dark orbs, Deal 100% damage

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#17: ESSpinnersFixed(110:46182) -> ビーム・ソード
Specific orbs change every 1.0s for 1 turn, Deal 90% damage

#18: ESEndPath(36:26) -> ESEndPath
end_turn

#19: ESResolve(73:46172) -> ＿ジオ根性
Survive attacks with 1 HP when HP > 50%