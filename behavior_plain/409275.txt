#409275 - White Speedster, Daytona Kuromi
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESBoardChangeAttackBits(85:41374) -> Wings of Rudeness
Change all orbs to Light, Deal 30% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBoardChangeAttackBits(85:41375) -> Wings of Rudeness
Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
Change all orbs to Light, Deal 110% damage

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 15

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESSkyfall(68:41376) -> Flying Heart
Heal skyfall +20% for 1 turn, Deal 90% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#13: ESDebuffATKTarget(143:41377) -> Devastating Wink
For 1 turn, 50% ATK for 2 random subs, Deal 95% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#16: ESOrbLock(94:41378) -> Vroom Vroom Flight
Lock all orbs, Deal 105% damage

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESSuperResolve(129:41373) -> ＿Kuromi Super Resolve
Damage which would reduce HP from above 50% to below 50% is nullified