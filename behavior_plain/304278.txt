#304278 - Awoken Set
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESStatusShield(20:44760) -> Spirit of the Burning God
	Voids status ailments for 999 turns
	[1] ESUnknown(152:44761) -> Burning Shadesoul
	No description set
	[2] ESAbsorbCombo(67:44762) -> Burning Instinct
	Absorb damage when combos <= 10 for 1 turn

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 6, target rnd 16

#5: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 3, target rnd 14

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 10

#7: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESAbsorbCombo(67:44766) -> Burning Instinct
	Absorb damage when combos <= 8 for 1 turn
	[1] ESUnknown(150:44767) -> Ground Collapse
	No description set, Deal 100% damage

#8: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESAbsorbCombo(67:44769) -> Burning Instinct
	Absorb damage when combos <= 9 for 1 turn
	[1] ESNoSkyfall(127:44770) -> Desert Anomaly
	No skyfall for 1 turn

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 7

#11: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#12: ESAbsorbCombo(67:44764) -> Burning Instinct
Absorb damage when combos <= 11 for 1 turn

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESAttackMultihit(15:44763) -> Burning Emotion
Deal 996% damage (6 hits, 166% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: SkillSet:
	[0] ESBindAwoken(88:44525) -> Star Machine's Wave
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:44526) -> Apocalyptic Star Crushing
	Deal 5,000% damage (5 hits, 1,000% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESResolve(73:42619) -> Resolve
Survive attacks with 1 HP when HP > 50%