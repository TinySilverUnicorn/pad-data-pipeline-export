#508492 - Toon Sprite Illusionary Artist, Melty
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESBlind5(5:43169) -> How dare you ruin my art...
	Blind all orbs on the board
	[1] ESAttackUPRemainingEnemies(17:43170) -> I'll never forgive you!!
	Increase damage to 150% for the next 4 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 3, target rnd 18

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 15

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESRandomSpawn(92:43171) -> Violet Spray
Spawn 8 random Dark orbs, Deal 90% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#13: ESOrbSealColumn(99:43172) -> In-line Skates
Seal the 1st and 6th columns for 1 turn, Deal 100% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#16: ESDebuffRCV(105:43173) -> Stencil Layer
RCV 50% for 1 turn, Deal 92% damage

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#19: ESAbsorbCombo(67:43174) -> Sprite Booster
Absorb damage when combos <= 5 for 1 turn, Deal 96% damage

#20: ESEndPath(36:26) -> ESEndPath
end_turn