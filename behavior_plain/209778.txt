#209778 - リック・ディアス
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESCloud(104:46743) -> クレイ・バズーカ
A 2×2 square of clouds appears for 2 turns at a random location, Deal 50% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 14

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 11

#8: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#9: ESDebuffATKTarget(143:46744) -> ビームサーベル
For 1 turn, 50% ATK for 1 random sub, Deal 90% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#12: ESDebuffRCV(105:46745) -> ビーム・ピストル
RCV 50% for 1 turn, Deal 100% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#15: ESVoidShieldBig(137:46746) -> バルカンファランクス
Void damage >= 500,000,000 for 1 turn

#16: ESEndPath(36:26) -> ESEndPath
end_turn