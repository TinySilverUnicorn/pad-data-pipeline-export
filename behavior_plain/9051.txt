#9051 - Super Reincarnated Ra Dragon
monster size: 5
new AI: True
start/max counter: 15
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 8)
	[0] ESStatusShield(20:38708) -> True Spirit of the Sun God Dragon
	Voids status ailments for 999 turns
	[1] ESDebuffMovetime(39:38709) -> Pressure of the Sun
	Movetime 25% for 4 turns
	[2] ESNoSkyfall(127:38710) -> World of the Sun
	No skyfall for 4 turns
	[3] ESDamageShield(74:38711) -> Almighty Protection
	Reduce damage from all sources by 90% for 4 turns

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 15, target rnd 18

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#5: ESSetCounterIf(38:177) -> ESSetCounterIf
set counter = 4 if counter == 0

#6: ESCountdown(37:178) -> ESCountdown
countdown

#7: ESAttackMultihit(15:38743) -> Sunlight Feathers
Deal 1,190% damage (7 hits, 170% each)

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 4)
	[0] ESBindAwoken(88:38719) -> Very well.
	Bind awoken skills for 1 turn
	[1] ESBindRandom(1:38720) -> I'll give you a trial.
	Bind 6 random cards for 1 turn
	[2] ESBoardChange(84:38721) -> Try erasing...
	Change all orbs to Fire, Water, Wood, Light, Dark, and Heal
	[3] ESBlindStickyRandom(97:38722) -> ...6 colors on this board.
	Blind random 4 orbs for 1 turn

#10: ESBranchEraseAttr(117:38756) -> ESBranchEraseAttr
Branch on color erased == 0, target rnd 12

#11: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESOrbSealRow(100:38728) -> Is that all you've got...?
	Seal the 1st row for 2 turns
	[1] ESBoardChange(84:38729) -> True Shining Blast
	Change all orbs to Water, Light, Heal, and Jammer
	[2] ESGravity(50:38730) -> Sun Impact
	Player -75% HP

#12: SkillSet:
	Condition: One-time use & Fire, Water, Wood, Light, Dark, and Heal are erased (ai:100 rnd:0) (cost: 2)
	[0] ESTargetedSkillHaste(139:38724) -> Impressive.
	Haste all cards' skills by 2 turns
	[1] ESBoardChange(84:38725) -> Shining Blast
	Change all orbs to Water, Light, and Heal
	[2] ESGravity(50:38726) -> Sun Impact
	Player -50% HP

#13: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESVoidShield(71:38741) -> Almighty Divine Shield
	Void damage >= 200,000,000 for 1 turn
	[1] ESAttackMultihit(15:38742) -> Sunlight Crown
	Deal 102% damage (3 hits, 34% each)

#14: SkillSet:
	Condition: 34% chance (ai:0 rnd:34)
	[0] ESDamageShield(74:38732) -> Almighty Protection
	Reduce damage from all sources by 50% for 1 turn
	[1] ESOrbLock(94:38733) -> Shining Lock
	Lock all Water and Light orbs, Deal 100% damage

#15: SkillSet:
	Condition: 33% chance (ai:0 rnd:33)
	[0] ESDamageShield(74:38735) -> Almighty Protection
	Reduce damage from all sources by 75% for 1 turn
	[1] ESCloud(104:38736) -> Shining Cloud
	A 2×2 square of clouds appears for 1 turn at a random location, Deal 101% damage

#16: SkillSet:
	Condition: 33% chance (ai:0 rnd:33)
	[0] ESDamageShield(74:38738) -> Almighty Protection
	Reduce damage from all sources by 50% for 1 turn
	[1] ESSpinnersRandom(109:38739) -> Rotation
	Random 2 orbs change every 1.0s for 1 turn, Deal 100% damage

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 22

#19: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#20: SkillSet:
	[0] ESAttributeBlock(107:38713) -> Imminent Demise
	Unable to match Heal orbs for 1 turn
	[1] ESOrbLock(94:38714) -> Divine Eyes of the Sun God Dragon
	Lock all orbs

#21: ESEndPath(36:26) -> ESEndPath
end_turn

#22: SkillSet:
	[0] ESChangeAttribute(46:38716) -> Iridescent Sun
	Change own attribute to random one of Fire, Water, Wood, Light, or Dark
	[1] ESAttackMultihit(15:38717) -> Sunlight Feathers
	Deal 1,190% damage (7 hits, 170% each)

#23: ESEndPath(36:26) -> ESEndPath
end_turn

#24: ESSuperResolve(129:38744) -> Super Resolve 50%
Damage which would reduce HP from above 50% to below 50% is nullified

#25: ESTypeResist(118:38745) -> Healer Halved
Reduce damage from Healer types by 50%

#26: ESNone(136:38746) -> ESNone
nothing