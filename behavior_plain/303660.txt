#303660 - Astalos
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESVoidShield(71:42336) -> Thunder-charged
Condition: One-time use (ai:100 rnd:0) (cost: 2)
Void damage >= 300,000,000 for 3 turns

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 11

#4: SkillSet:
	Condition: 17% chance (ai:0 rnd:17)
	[0] ESAttackMultihit(15:42342) -> Rushing Bite
	Deal 51% damage (3 hits, 17% each)
	[1] ESOrbLock(94:42341) -> Wingtalon Sweep
	Lock all Light and Dark orbs, Deal 50% damage

#5: SkillSet:
	Condition: 17% chance (ai:0 rnd:17)
	[0] ESAttackMultihit(15:42345) -> Rushing Bite
	Deal 51% damage (3 hits, 17% each)
	[1] ESOrbLock(94:42344) -> Tailspin
	Lock all Fire and Wood orbs, Deal 50% damage

#6: SkillSet:
	Condition: 17% chance (ai:0 rnd:17)
	[0] ESAttackMultihit(15:42348) -> Rushing Bite
	Deal 51% damage (3 hits, 17% each)
	[1] ESOrbLock(94:42347) -> Crest Swing
	Lock all Fire and Water orbs, Deal 50% damage

#7: SkillSet:
	Condition: 17% chance (ai:0 rnd:17)
	[0] ESAttackMultihit(15:42351) -> Rushing Bite
	Deal 51% damage (3 hits, 17% each)
	[1] ESOrbLock(94:42350) -> Wingtalon Strike
	Lock all Water and Dark orbs, Deal 50% damage

#8: SkillSet:
	Condition: 16% chance (ai:0 rnd:16)
	[0] ESAttackMultihit(15:42354) -> Rushing Bite
	Deal 51% damage (3 hits, 17% each)
	[1] ESOrbLock(94:42353) -> Charged Tail Strike
	Lock all Wood and Light orbs, Deal 50% damage

#9: SkillSet:
	Condition: 16% chance (ai:0 rnd:16)
	[0] ESBindAttack(63:42357) -> Roar
	Bind 1 random sub for 2 turns, Deal 100% damage
	[1] ESRandomSpawn(92:42356) -> Spinning Dive
	Spawn 3 random Light and Dark orbs, Deal 50% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESOrbLock(94:42338) -> Lightning Breath
	Lock 9 random orbs
	[1] ESAttackUPRemainingEnemies(17:42339) -> Enraged
	Increase damage to 150% for the next 2 turns

#12: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 4

#13: ESEndPath(36:26) -> ESEndPath
end_turn