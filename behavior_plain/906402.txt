#906402 - Moon Key Heir, Euchs
monster size: 3
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESDamageShield(74:40216) -> Owl Barrier
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Reduce damage from all sources by 50% for 3 turns

#3: ESRandomSpawn(92:40218) -> Dark Owl
Condition: 50% chance (ai:0 rnd:50)
Spawn 4 random Dark orbs, Deal 100% damage

#4: ESAttackMultihit(15:40217) -> Owl Feathers
Condition: 50% chance (ai:0 rnd:50)
Deal 105% damage (3 hits, 35% each)

#5: ESEndPath(36:26) -> ESEndPath
end_turn