#209770 - ザクII
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchLevel(35:25) -> ESBranchLevel
Branch on level >= 4, target rnd 5

#3: ESSkyfallLocked(96:46523) -> クラッカー
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Locked random skyfall +100% for 5 turns

#4: ESEndPath(36:26) -> ESEndPath
end_turn

#5: ESSkillDelay(89:46524) -> ザク・バズーカ
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Delay active skills by 5 turns

#6: ESAttackMultihit(15:46526) -> ザク・マシンガン
Condition: 50% chance (ai:0 rnd:50)
Deal 70% damage (5 hits, 14% each)

#7: ESUnknown(151:46527) -> ヒート・ホーク
Condition: 50% chance (ai:0 rnd:50)
No description set, Deal 65% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn