#309689 - 超転生オルファリオン
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESStatusShield(20:44820) -> My name is Orpharion!
	Voids status ailments for 999 turns
	[1] ESVoidShieldBig(137:44821) -> Ali Protettrici
	Void damage >= 2,000,000,000 for 999 turns
	[2] ESCloud(104:44822) -> Rannuvolamento
	A 3×1 rectangle of clouds appears for 15 turns at a random location, Deal 135% damage

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 5, target rnd 14

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 10

#5: SkillSet:
	Condition: 25% chance (ai:0 rnd:25)
	[0] ESUnknown(151:44831) -> Declino
	No description set
	[1] ESDamageShield(74:44832) -> Armatura di Luce
	Reduce damage from all sources by 50% for 1 turn, Deal 100% damage

#6: SkillSet:
	Condition: 25% chance (ai:0 rnd:25)
	[0] ESUnknown(151:44834) -> Declino
	No description set
	[1] ESOrbLock(94:44835) -> Chiave a Spina
	Lock 10 random orbs, Deal 80% damage

#7: SkillSet:
	Condition: 25% chance (ai:0 rnd:25)
	[0] ESAbsorbCombo(67:44837) -> Piuma del Paradiso
	Absorb damage when combos <= 8 for 1 turn
	[1] ESNoSkyfall(127:44838) -> Pioggia d'Ali
	No skyfall for 1 turn

#8: SkillSet:
	Condition: 25% chance (ai:0 rnd:25)
	[0] ESAbsorbCombo(67:44840) -> Piuma del Paradiso
	Absorb damage when combos <= 9 for 1 turn
	[1] ESSpinnersFixed(110:44841) -> Cambiamento
	Specific orbs change every 1.0s for 1 turn, Deal 100% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 5

#11: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#12: SkillSet:
	[0] ESGravity(50:44824) -> Arma Bianca
	Player -99% HP
	[1] ESAbsorbCombo(67:44825) -> Piuma del Paradiso
	Absorb damage when combos <= 10 for 1 turn
	[2] ESDebuffRCV(105:44826) -> Ristoro in Declino
	RCV 50% for 1 turn, Deal 200% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: SkillSet:
	[0] ESBindAwoken(88:44828) -> Nessuno Spazio
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:44829) -> Innumerevoli Ferite
	Deal 5,000% damage (10 hits, 500% each)

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESSuperResolve(129:31386) -> Super Resolve
Damage which would reduce HP from above 50% to below 50% is nullified

#17: ESAttributeResist(72:44518) -> Dark Reduced
Reduce damage from Dark attrs by 50%