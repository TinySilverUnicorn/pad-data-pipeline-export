#403656 - Nargacuga
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 4)
	[0] ESStatusShield(20:42283) -> Menace
	Voids status ailments for 999 turns
	[1] ESBoardChange(84:42284) -> Tail Slam
	Change all orbs to Dark

#3: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESInactivity66(66:42286) -> Its tail is stuck and can't move!
	Do nothing
	[1] ESAttackUPRemainingEnemies(17:42287) -> Enraged
	Increase damage to 150% for the next 2 turns

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#5: ESAttackMultihit(15:42293) -> Cutwing
Condition: 33% chance (ai:33 rnd:0)
Deal 102% damage (3 hits, 34% each)

#6: ESBlind62(62:42294) -> Springing Strike
Condition: 50% chance (ai:50 rnd:0)
Blind all orbs on the board, Deal 101% damage

#7: ESAttackMultihit(15:42295) -> Roar
Deal 100% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESRowSpawnMulti(79:42289) -> Tail Sweep
	Change the 3rd row to Dark orbs, Deal 52% damage
	[1] ESAttackMultihit(15:42290) -> Spiked Tail Slam
	Deal 51% damage (3 hits, 17% each)

#10: ESDamageShield(74:42291) -> Dash
Condition: 50% chance (ai:50 rnd:0)
Reduce damage from all sources by 75% for 1 turn, Deal 100% damage

#11: ESRandomSpawn(92:42292) -> Tailspike Shot
Spawn 3 random Jammer orbs, Deal 100% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESAttributeResist(72:42296) -> Dark Halved
Reduce damage from Dark attrs by 50%