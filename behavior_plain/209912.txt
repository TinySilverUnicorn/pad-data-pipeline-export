#209912 - GS・デイトナ
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESVoidShieldBig(137:46856) -> ダンスの構成はオレにまかせて
	Void damage >= 1,000,000,000 for 3 turns
	[1] ESBindSkill(14:46857) -> ちゃんとついてきてよね
	Bind active skills for 1 turn

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 10, target rnd 13

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#5: ESAttackMultihit(15:46861) -> 次はもっと強く！
Condition: 33% chance (ai:0 rnd:33)
Deal 110% damage (2 hits, 55% each)

#6: ESTargetedSkillDelay(140:46862) -> ペースダウンしなよ
Condition: 33% chance (ai:0 rnd:33)
Delay all cards' skills by 1~3 turns, Deal 105% damage

#7: ESUnknown(150:46863) -> だめでしょ
Condition: 34% chance (ai:0 rnd:34)
No description set, Deal 100% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 5

#10: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#11: SkillSet:
	[0] ESOrbLock(94:46859) -> 計算し尽くされた演出を
	Lock 15 random orbs
	[1] ESNoSkyfall(127:46860) -> みせてあげるよ
	No skyfall for 1 turn

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESAttackMultihit(15:46895) -> はい、おわり
Deal 200% damage (4 hits, 50% each)

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESResolve(73:46885) -> ＿根性
Survive attacks with 1 HP when HP > 50%

#16: ESTypeResist(118:46888) -> ＿ドラゴン半減
Reduce damage from Dragon types by 50%