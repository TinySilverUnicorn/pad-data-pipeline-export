#709727 - サザビー
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESSkillDelay(89:46790) -> ならば今すぐ愚民ども全てに
	Delay active skills by 3 turns
	[1] ESUnknown(150:46791) -> 英知を授けてみせろ！
	No description set

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
	[0] ESAttackMultihit(15:46793) -> メガ粒子砲
	Deal 110% damage (5 hits, 22% each)
	[1] ESDebuffATKTarget(143:46794) -> パワーダウンだと………！？
	For 2 turns, 1% ATK for 4 random subs

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 3, target rnd 19

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 16

#9: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 13

#10: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#11: ESBindAttack(63:46795) -> ビーム・トマホーク投擲
Bind 1 random sub for 5 turns, Deal 95% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#14: SkillSet:
	[0] ESDamageShield(74:46797) -> そんなもので私が倒せるものか！
	Reduce damage from all sources by 75% for 1 turn
	[1] ESBlindStickyRandom(97:46798) -> ファンネル
	Blind random 6 orbs for 3 turns, Deal 100% damage

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#17: ESRandomSpawn(92:46799) -> ビーム・ショット・ライフル
Spawn 10 random Fire orbs, Deal 110% damage

#18: ESEndPath(36:26) -> ESEndPath
end_turn

#19: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#20: SkillSet:
	[0] ESFixedStart(101:46801) -> キサマがいなければッ！！
	Fix orb movement starting point to random position on the board
	[1] ESColumnSpawnMulti(77:46802) -> ビームサーベル
	Change the 1st and 6th columns to Fire orbs, Deal 100% damage

#21: ESEndPath(36:26) -> ESEndPath
end_turn

#22: ESSuperResolve(129:46788) -> ＿サザビー超根性
Damage which would reduce HP from above 50% to below 50% is nullified