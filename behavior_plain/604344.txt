#604344 - 学園のアイドル・ウルカ
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESInactivity66(66:47396) -> 次の演目は～
	Do nothing
	[1] ESEndBattle(40:47397) -> 素敵なステージにしましょうね～
	Reduce self HP to 0

#5: ESEndPath(36:26) -> ESEndPath
end_turn