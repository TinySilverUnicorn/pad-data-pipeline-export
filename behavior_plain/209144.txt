#209144 - エドワード・ニューゲート
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESAttributeBlock(107:40887) -> 振動
	Unable to match Dark orbs for 5 turns
	[1] ESBoardSizeChange(126:40888) -> 海震
	Change board size to 7x6 for 5 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESDebuffMovetime(39:40890) -> グララララララ!!!
	Movetime 50% for 3 turns
	[1] ESAbsorbCombo(67:40891) -> 若僧が……!!!
	Absorb damage when combos <= 13 for 3 turns

#7: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESDamageShield(74:40894) -> まだまだ!!!
	Reduce damage from all sources by 75% for 2 turns
	[1] ESUnknown(152:40893) -> おれァ“白ひげ”だァア!!!!
	No description set, Deal 265% damage

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESBoardChangeAttackBits(85:40895) -> 邪魔だなおい…!!
Change all orbs to Water, Wood, Light, and Heal, Deal 110% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#13: ESDebuffATKTarget(143:40896) -> 勝機アリだ…!!
For 1 turn, 1% ATK for 1 random sub, Deal 105% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSuperResolve(129:40885) -> ＿白ひげ超根性
Damage which would reduce HP from above 50% to below 50% is nullified

#16: ESTypeResist(118:40884) -> ＿白ひげバランス半減
Reduce damage from Balanced types by 50%