#806398 - Forest Key Heir, Amlynea
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESSkillDelay(89:37589) -> Deer Wind
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Delay active skills by 3 turns

#3: ESRandomSpawn(92:37590) -> Deer Antlers
Condition: 50% chance (ai:0 rnd:50)
Spawn 4 random Wood orbs, Deal 100% damage

#4: ESAttackMultihit(15:37591) -> Deer Blade
Condition: 50% chance (ai:0 rnd:50)
Deal 110% damage (2 hits, 55% each)

#5: ESEndPath(36:26) -> ESEndPath
end_turn