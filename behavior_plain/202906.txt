#202906 - black-winged goddess, valkyrie claire
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESAbsorbThresholdBig(138:40272) -> claire phantom
Absorb damage when damage >= 2,000,000,000 for 3 turns, Deal 50% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 10

#7: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#8: ESOrbLock(94:40273) -> shadow tag
Lock all orbs

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#11: ESRandomSpawn(92:40274) -> darklight bravery
Spawn 15 random Dark orbs, Deal 60% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn