#9719 - 破神機・ラグナロク＝ドラゴン -ELF-
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 4)
	[0] ESStatusShield(20:44372) -> ラグナロクシールド
	Voids status ailments for 999 turns
	[1] ESUnknown(152:44373) -> ジャミングレイン
	No description set
	[2] ESAttackMultihit(15:44400) -> カタストロフィソード
	Deal 102% damage (3 hits, 34% each)
	[3] ESUnknown(155:44374) -> オールドロップリフレッシュ
	No description set

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 15, target rnd 15

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#5: SkillSet:
	Condition: 33% chance (ai:33 rnd:0)
	[0] ESDebuffATKTarget(143:44399) -> ダブルクラッシュ
	For 1 turn, 5% ATK for 2 random subs
	[1] ESAttackMultihit(15:44400) -> カタストロフィソード
	Deal 102% damage (3 hits, 34% each)

#6: SkillSet:
	Condition: 50% chance (ai:50 rnd:0)
	[0] ESRandomSpawn(92:44402) -> ドロップチャージ
	Spawn 3 random Wood, Dark, and Jammer orbs
	[1] ESAttackMultihit(15:44403) -> カタストロフィソード
	Deal 102% damage (3 hits, 34% each)

#7: SkillSet:
	[0] ESBombRandomSpawn(102:44405) -> ダークネスエクスプロージョン
	Spawn 7 random locked Bomb orbs
	[1] ESAttackMultihit(15:44406) -> カタストロフィソード
	Deal 102% damage (3 hits, 34% each)

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESDamageShield(74:44382) -> メタリックアーマー
	Reduce damage from all sources by 90% for 1 turn
	[1] ESMaxHPChange(111:44383) -> ラグナロクアイズ
	Change player HP to 3,000,000 for 2 turns
	[2] ESDebuffRCV(105:44384) -> ダウンレーザー
	RCV 50% for 1 turn
	[3] ESAttackUPRemainingEnemies(17:44385) -> イグナイトブースト
	Increase damage to 500% for the next 1 turn

#10: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESBoardChangeAttackBits(85:44387) -> ダークネスバースト
	Change all orbs to Wood and Dark, Deal 46% damage
	[1] ESAttackMultihit(15:44388) -> カタストロフィソード
	Deal 45% damage (3 hits, 15% each)

#11: SkillSet:
	Condition: 33% chance (ai:33 rnd:0)
	[0] ESColumnSpawnMulti(76:44390) -> ドロップチャージ・改
	Change the 2nd and 5th columns to Wood, Dark, and Poison orbs
	[1] ESUnknown(151:44391) -> カタストロフィキャノン
	No description set, Deal 101% damage

#12: SkillSet:
	Condition: 50% chance (ai:50 rnd:0)
	[0] ESGravity(50:44393) -> グラビティウェーブ
	Player -110% HP
	[1] ESAttackMultihit(15:44394) -> カタストロフィソード
	Deal 60% damage (3 hits, 20% each)

#13: SkillSet:
	[0] ESDebuffATK(130:44396) -> ダークネスウィング
	ATK -75% for 1 turn
	[1] ESAbsorbCombo(67:44397) -> カタストロフィブラスト
	Absorb damage when combos <= 8 for 1 turn, Deal 101% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 19

#16: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#17: SkillSet:
	[0] ESDebuffMovetime(39:44376) -> タイムブレイク
	Movetime 25% for 1 turn
	[1] ESUnknown(155:44377) -> オールドロップリフレッシュ
	No description set

#18: ESEndPath(36:26) -> ESEndPath
end_turn

#19: SkillSet:
	[0] ESBindAwoken(88:44379) -> パワーデリート
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:44380) -> カタストロフィミサイル
	Deal 546% damage (7 hits, 78% each)

#20: ESEndPath(36:26) -> ESEndPath
end_turn

#21: ESSuperResolve(129:44407) -> 50％超根性
Damage which would reduce HP from above 50% to below 50% is nullified

#22: ESTypeResist(118:44408) -> 神ドラゴン半減
Reduce damage from Dragon and God types by 50%

#23: ESNone(136:44412) -> ESNone
nothing