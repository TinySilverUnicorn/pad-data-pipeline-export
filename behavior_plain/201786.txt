#201786 - incarnation of suzaku, leilan
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 7

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESNoSkyfall(127:40252) -> stay calm...
No skyfall for 10 turns

#5: ESSkyfall(68:40254) -> dance of the suzaku
Fire, Wood, and Light skyfall +100% for 1 turn

#6: ESEndPath(36:26) -> ESEndPath
end_turn

#7: ESDebuffATKTarget(143:40250) -> minimizing roar
Condition: 50% chance (ai:0 rnd:50)
For 1 turn, 50% ATK for 1 random sub, Deal 90% damage

#8: ESUnknown(151:40251) -> minimizing orbs
Condition: 50% chance (ai:0 rnd:50)
No description set, Deal 100% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESAttributeResist(72:40253) -> ＿Leilan Fire Reduced
Reduce damage from Fire attrs by 50%