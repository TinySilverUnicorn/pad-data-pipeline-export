#8398 - Super Earth Knight
monster size: 4
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:41851) -> Earth Knight's Willpower
	Voids status ailments for 999 turns
	[1] ESAttributeBlock(107:41899) -> Knight Magic
	Unable to match Fire orbs for 1 turn

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 11

#4: ESDamageShield(74:41852) -> Armor of the Knight
Condition: 17% chance (ai:0 rnd:17)
Reduce damage from all sources by 75% for 1 turn, Deal 100% damage

#5: ESAttributeBlock(107:41853) -> Knight Magic
Condition: 17% chance (ai:0 rnd:17)
Unable to match Fire orbs for 1 turn, Deal 101% damage

#6: ESAttributeBlock(107:41854) -> Knight Magic
Condition: 17% chance (ai:0 rnd:17)
Unable to match Water orbs for 1 turn, Deal 101% damage

#7: ESAttributeBlock(107:41855) -> Knight Magic
Condition: 17% chance (ai:0 rnd:17)
Unable to match Wood orbs for 1 turn, Deal 101% damage

#8: ESAttributeBlock(107:41856) -> Knight Magic
Condition: 16% chance (ai:0 rnd:16)
Unable to match Light orbs for 1 turn, Deal 100% damage

#9: ESAttributeBlock(107:41857) -> Knight Magic
Condition: 16% chance (ai:0 rnd:16)
Unable to match Dark orbs for 1 turn, Deal 100% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESDamageShield(74:41845) -> Armor of the Knight
	Reduce damage from all sources by 50% for 3 turns
	[1] ESUnknown(152:41846) -> Earth Knight's Prayer
	No description set
	[2] ESBoardChangeAttackBits(85:41847) -> Earth Impact
	Change all orbs to Wood, Deal 110% damage

#12: SkillSet:
	[0] ESBindAwoken(88:41849) -> Wave of the Knight
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:41850) -> Guard Break - Wood
	Deal 770% damage (7 hits, 110% each)

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESTurnChangePassive(106:41858) -> Enemy's next turn changed
Enemy turn counter change to 3 when HP <= 50%

#15: ESSuperResolve(129:41859) -> Super Resolve 50%
Damage which would reduce HP from above 50% to below 50% is nullified