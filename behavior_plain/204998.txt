#204998 - Alt. Enshrined Primordial Divinity, Kamimusubi
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESRowSpawnMulti(79:44150) -> So you've come this far, have you...
	Change the 1st row to Fire orbs and the 3rd row to Fire orbs, Deal 50% damage
	[1] ESColumnSpawnMulti(77:44151) -> Let's celebrate together!
	Change the 2nd column to Fire orbs and the 5th column to Fire orbs, Deal 50% damage
	[2] ESCloud(104:44152) -> Divine Gate of the Cloud Sea
	A row of clouds appears for 5 turns at 5th row, 1st column

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 10

#7: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#8: SkillSet:
	[0] ESRowSpawnMulti(79:44154) -> Allow me to fill your cup.
	Change the 1st row to Heal orbs and the 3rd row to Heal orbs, Deal 50% damage
	[1] ESColumnSpawnMulti(77:44155) -> It's a great feeling, isn't it?
	Change the 2nd column to Heal orbs and the 5th column to Heal orbs, Deal 50% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#11: SkillSet:
	[0] ESBlind5(5:44157) -> Now...close your eyes.
	Blind all orbs on the board
	[1] ESBlindStickySkyfall(128:44158) -> Can you hear the gods celebrating?
	For 1 turn, 30% chance for skyfall orbs to be blinded for turn, Deal 95% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESTypeResist(118:44147) -> ＿Kamimusubi God Halved
Reduce damage from God types by 50%

#14: ESResolve(73:44148) -> ＿Kamimusubi Resolve
Survive attacks with 1 HP when HP > 50%