#708935 - Pang Nuan
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: ESVoidShield(71:39111) -> I am Pang Nuan, god of war!
Void damage >= 50,000 for 5 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESTargetedSkillHaste(139:39112) -> Damn thine impudence!
Condition: One-time use (ai:100 rnd:0) (cost: 2)
Haste both leaders' skills by 99 turns, Deal 90% damage

#7: SkillSet:
	Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
	[0] ESSkillDelay(89:39114) -> ...This perpetually enveloping...
	Delay active skills by 2 turns
	[1] ESSkyfall(68:39115) -> ...sense of irritation.
	Fire and Dark skyfall +15% for 3 turns, Deal 100% damage

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESOrbChangeAttack(48:39116) -> Do not interfere.
Change a random attribute to Fire orbs, Deal 90% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#13: ESAttackMultihit(15:39117) -> A waste of breath.
Deal 90% damage (2 hits, 45% each)

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSuperResolve(129:39110) -> ＿Pang Nuan Super Resolve
Damage which would reduce HP from above 50% to below 50% is nullified