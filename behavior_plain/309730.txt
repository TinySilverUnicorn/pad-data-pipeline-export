#309730 - Zガンダム
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESAbsorbThresholdBig(138:46755) -> 貴様のような者がいるから
	Absorb damage when damage >= 2,000,000,000 for 2 turns
	[1] ESAttributeBlock(107:46756) -> 戦いは終わらないんだ！！
	Unable to match Fire and Water orbs for 2 turns, Deal 80% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
	[0] ESSkyfall(68:46758) -> 俺の身体をみんなに貸すぞ！！
	Heal skyfall +20% for 1 turn
	[1] ESBoardChangeAttackBits(85:46759) -> ここからいなくなれッ！！
	Change all orbs to Fire, Water, and Heal, Deal 120% damage

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 15

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESUnknown(150:46760) -> 遊びでやってるんじゃないんだよぉッ！
No description set, Deal 80% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#13: ESDebuffRCV(105:46761) -> まだ抵抗するのならッ！！
RCV 50% for 1 turn, Deal 100% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#16: ESColumnSpawnMulti(77:46762) -> ハイパー・メガ・ランチャー
Change the 1st and 2nd columns to Heal orbs, Deal 110% damage

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESResolve(73:46753) -> ＿Z根性
Survive attacks with 1 HP when HP > 50%