#406914 - キャプテン・マーベル【バイナリー】
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESAbsorbThresholdBig(138:38966) -> バイナリーモード覚醒
	Absorb damage when damage >= 2,000,000,000 for 3 turns
	[1] ESSkillDelay(89:38967) -> より高く、より遠く、より速く
	Delay active skills by 5 turns, Deal 80% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESColumnSpawnMulti(77:38968) -> フォトン・ブラスト
Condition: One-time use (ai:100 rnd:0) (cost: 2)
Change the 1st and 6th columns to Light orbs, Deal 100% damage

#7: ESColumnSpawnMulti(77:38969) -> フォトン・ブラスト
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Change the 1st and 6th columns to Light orbs and the 2nd and 5th columns to Light orbs, Deal 150% damage

#8: ESBoardChangeAttackBits(85:38997) -> フォトン・ブラスト
Change all orbs to Light, Deal 450% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESResolve(73:38964) -> ＿マーベル根性
Survive attacks with 1 HP when HP > 50%