#308972 - Wan Ji
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:3402) -> ESPreemptive
Enable preempt if level 4

#2: ESBranchLevel(35:25) -> ESBranchLevel
Branch on level >= 4, target rnd 4

#3: ESEndPath(36:26) -> ESEndPath
end_turn

#4: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 8

#5: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#6: SkillSet:
	[0] ESRandomSpawn(92:39119) -> Can... can you feel it?
	Spawn 5 random Dark orbs
	[1] ESSkyfall(68:39120) -> C-Can you feel our darkness?
	Dark skyfall +15% for 5 turns

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESBlind62(62:39122) -> Can... you see it?
	Blind all orbs on the board, Deal 100% damage
	[1] ESOrbLock(94:39123) -> D-Dwelling within us orphans...
	Lock all Dark orbs
	[2] ESDebuffATKTarget(143:39124) -> The grudge of Ch-Changping?
	For 5 turns, 25% ATK for both leaders

#9: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 14

#10: ESGravity(50:39125) -> Y-You do not fully understand.
Condition: 33% chance (ai:0 rnd:33)
Player -99% HP

#11: ESBlindStickyRandom(97:39126) -> Curse of Changping
Condition: 33% chance (ai:0 rnd:33)
Blind random 5 orbs for 2 turns, Deal 80% damage

#12: ESSpinnersRandom(109:39127) -> Whirlpool of Resentment
Condition: 34% chance (ai:0 rnd:34)
Random 3 orbs change every 1.0s for 1 turn, Deal 75% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 2, target rnd 10

#15: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b11

#16: SkillSet:
	[0] ESBindRandom(1:39129) -> The curse...
	Bind 6 random cards for 2 turns
	[1] ESBoardChangeAttackBits(85:39130) -> ...is upon all of hu-humanity!
	Change all orbs to Dark, Deal 120% damage

#17: ESEndPath(36:26) -> ESEndPath
end_turn