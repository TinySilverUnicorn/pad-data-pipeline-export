#209188 - ボア・ハンコック
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESStatusShield(20:40555) -> ――何をしようとも
	Voids status ailments for 999 turns
	[1] ESAbsorbThresholdBig(138:40556) -> わらわは許される!!
	Absorb damage when damage >= 1,000,000,000 for 3 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESDebuffRCV(105:40558) -> そうよわらわが
	RCV 50% for 3 turns
	[1] ESSkyfall(68:40559) -> 美しいから!!!
	Heal skyfall +15% for 3 turns, Deal 100% damage

#7: SkillSet:
	Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
	[0] ESBindAwoken(88:40561) -> 生かしてはおかぬ
	Bind awoken skills for 1 turn
	[1] ESBoardChangeAttackBits(85:40562) -> こんなに怒りを覚えた事はない!!!
	Change all orbs to Heal, Deal 80% damage

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 16

#9: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 13

#10: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#11: ESOrbLock(94:40563) -> メロメロ甘風!!!
Lock all orbs, Deal 90% damage

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#14: ESSkyfallLocked(96:40564) -> 虜の矢
Locked random skyfall +100% for 1 turn, Deal 90% damage

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#17: ESAttackMultihit(15:40565) -> 芳香脚
Deal 99% damage (3 hits, 33% each)

#18: ESEndPath(36:26) -> ESEndPath
end_turn

#19: ESResolve(73:40553) -> ＿ハンコック根性
Survive attacks with 1 HP when HP > 50%

#20: ESTypeResist(118:40552) -> ＿ハンコック回復半減
Reduce damage from Healer types by 50%