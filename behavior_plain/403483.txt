#403483 - Alt. Reincarnated Idunn & Idunna
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESBlindStickySkyfall(128:44736) -> Welcome!
	For 6 turns, 20% chance for skyfall orbs to be blinded for turn
	[1] ESBoardChangeAttackBits(85:44737) -> We'll give you our hearts!
	Change all orbs to Heal, Deal 100% damage
	[2] ESBombFixedSpawn(103:44738) -> Lovely Bomb
	Spawn locked Bomb orbs in the specified positions

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 6, target rnd 16

#5: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 3, target rnd 14

#6: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 10

#7: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESBombRandomSpawn(102:44742) -> Lovely Bomb
	Spawn 6 random Bomb orbs
	[1] ESColumnSpawnMulti(77:44743) -> Blót Gandr
	Change the 1st column to Water orbs and the 6th column to Heal orbs, Deal 100% damage

#8: SkillSet:
	Condition: 50% chance (ai:0 rnd:50)
	[0] ESBombRandomSpawn(102:44745) -> Lovely Bomb
	Spawn 6 random Bomb orbs
	[1] ESColumnSpawnMulti(77:44746) -> Blót Gandr
	Change the 1st column to Poison orbs and the 6th column to Water orbs, Deal 100% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 7

#11: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#12: ESBindAwoken(88:44740) -> We won't forgive you!
Bind awoken skills for 3 turns

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESBoardChangeAttackBits(85:44739) -> We'll give you something first-rate!
Change all orbs to Mortal Poison, Deal 1,000% damage

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: SkillSet:
	[0] ESBindAwoken(88:44525) -> Star Machine's Wave
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:44526) -> Apocalyptic Star Crushing
	Deal 5,000% damage (5 hits, 1,000% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESResolve(73:42619) -> Resolve
Survive attacks with 1 HP when HP > 50%