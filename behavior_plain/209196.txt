#209196 - ユースタス・キッド
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESSkyfall(68:40546) -> スキを見せたら
	Jammer skyfall +20% for 3 turns
	[1] ESBindSkill(14:40547) -> 死ぬと思えェ!!!
	Bind active skills for 3 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESRandomSpawn(92:40548) -> 磁気弦!!!
Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 1)
Spawn 15 random Jammer orbs, Deal 110% damage

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 15

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESDebuffATK(130:40549) -> コイツはおれの獲物だ!!
ATK -50% for 1 turn, Deal 90% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#13: ESDamageShield(74:40550) -> 反発
Reduce damage from all sources by 75% for 1 turn, Deal 100% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#16: ESAttackMultihit(15:40551) -> 磁気ピストルズ!!
Deal 96% damage (6 hits, 16% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn

#18: ESTypeResist(118:40543) -> ＿キッドマシン半減
Reduce damage from Machine types by 50%

#19: ESResolve(73:40544) -> ＿キッド根性
Survive attacks with 1 HP when HP > 50%