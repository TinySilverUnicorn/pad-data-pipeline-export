#305971 - Watchful Grove Dragon Healer, Alynna
monster size: 5
new AI: True
start/max counter: 127
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 64)
	[0] ESStatusShield(20:39519) -> Wish for Peace
	Voids status ailments for 999 turns
	[1] ESBindAwoken(88:39520) -> Nature's Grove
	Bind awoken skills for 4 turns
	[2] ESVoidShieldBig(137:39521) -> Here I gooo!
	Void damage >= 500,000,000 for 999 turns

#3: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 32)
	[0] ESBoardChange(84:39528) -> Light Butterflies
	Change all orbs to Fire, Water, Wood, Light, Dark, and Heal
	[1] ESAttributeBlock(107:39529) -> Nature's Call
	Unable to match Fire orbs for 1 turn, Deal 100% damage

#4: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 16)
	[0] ESBoardChange(84:39531) -> Light Butterflies
	Change all orbs to Fire, Water, Wood, Light, Dark, and Heal
	[1] ESAttributeBlock(107:39532) -> Nature's Call
	Unable to match Water orbs for 1 turn, Deal 125% damage

#5: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 8)
	[0] ESBoardChange(84:39534) -> Light Butterflies
	Change all orbs to Fire, Water, Wood, Light, Dark, and Heal
	[1] ESAttributeBlock(107:39535) -> Nature's Call
	Unable to match Wood orbs for 1 turn, Deal 150% damage

#6: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 4)
	[0] ESBoardChange(84:39537) -> Light Butterflies
	Change all orbs to Fire, Water, Wood, Light, Dark, and Heal
	[1] ESAttributeBlock(107:39538) -> Nature's Call
	Unable to match Light orbs for 1 turn, Deal 175% damage

#7: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESBoardChange(84:39540) -> Light Butterflies
	Change all orbs to Fire, Water, Wood, Light, Dark, and Heal
	[1] ESAttributeBlock(107:39541) -> Nature's Call
	Unable to match Dark orbs for 1 turn, Deal 200% damage

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESDebuffRCV(105:39523) -> Oh my...!
	RCV 50% for 5 turns
	[1] ESBindAwoken(88:39524) -> I'll do my best!
	Bind awoken skills for 1 turn
	[2] ESDamageShield(74:39525) -> Butterfly Guard
	Reduce damage from all sources by 75% for 1 turn

#9: ESAttackMultihit(15:39526) -> Nature's Forest
Deal 1,000% damage (10 hits, 100% each)

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESResolve(73:39629) -> Resolve
Survive attacks with 1 HP when HP > 1%