#602822 - 新聞部の新人・ウォーロン
monster size: 4
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESFixedStart(101:47344) -> 一瞬のかがやき…
	Fix orb movement starting point to random position on the board
	[1] ESSpinnersFixed(110:47345) -> スクープの命でやんす！
	Specific orbs change every 0.5s for 1 turn

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 14

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 11

#8: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#9: ESFixedStart(101:47347) -> 魂をこめるでやんす！
Fix orb movement starting point to random position on the board, Deal 100% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#12: ESFixedStart(101:47349) -> 角度が命でやんす！
Fix orb movement starting point to random position on the board, Deal 100% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#15: ESFixedStart(101:47351) -> まだまだ練習が必要でやんす！
Fix orb movement starting point to random position on the board, Deal 100% damage

#16: ESEndPath(36:26) -> ESEndPath
end_turn