#403652 - Rathalos
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 4)
	[0] ESVoidShield(71:42251) -> Roar
	Void damage >= 1,000,000,000 for 3 turns
	[1] ESDebuffMovetime(39:42252) -> Talon Strike
	Movetime 50% for 3 turns, Deal 100% damage

#3: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESBindSkill(14:42254) -> Dash
	Bind active skills for 3 turns, Deal 50% damage
	[1] ESDebuffATKTarget(143:42255) -> Aerial Sweeping Fire Breath
	For 2 turns, 25% ATK for 3 random cards, Deal 53% damage

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#5: ESRandomSpawn(92:42259) -> Aerial Tailspin
Condition: 33% chance (ai:0 rnd:33)
Spawn 7 random Fire orbs, Deal 50% damage

#6: ESOrbChangeAttackBits(108:42260) -> Aerial Fire Breath
Condition: 50% chance (ai:0 rnd:50)
Change all Water and Heal orbs to Fire orbs, Deal 101% damage

#7: ESAttackMultihit(15:42261) -> Grounded Biting Fire Breath Combo
Deal 102% damage (3 hits, 34% each)

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESRowSpawnMulti(79:42257) -> Evading Fire Breath
	Change the 3rd and 4th rows to Fire orbs, Deal 50% damage
	[1] ESOrbLock(94:42258) -> Binding Attack
	Lock all Fire orbs, Deal 52% damage

#10: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 5

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESAttributeResist(72:42262) -> Fire Halved
Reduce damage from Fire attrs by 50%