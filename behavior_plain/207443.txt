#207443 - インセクター羽蛾＆インセクト女王
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:47107) -> これから教えてやるぜ！
	Voids status ailments for 999 turns
	[1] ESSkyfall(68:47108) -> スーパーインセクトデッキの恐ろしさを！
	Mortal Poison skyfall +15% for 3 turns

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#4: ESAbsorbCombo(67:47110) -> 魔法カード「虫除けバリアー」
Condition: 25% chance (ai:0 rnd:25)
Absorb damage when combos <= 6 for 1 turn, Deal 100% damage

#5: ESAttackMultihit(15:47111) -> クイーンズ・ヘル・ブレス
Condition: 25% chance (ai:0 rnd:25)
Deal 102% damage (3 hits, 34% each)

#6: ESRandomSpawn(92:47112) -> クイーンズ・インパクト
Condition: 25% chance (ai:0 rnd:25)
Spawn 4 random Wood orbs, Deal 100% damage

#7: ESPoisonChangeRandomAttack(64:47113) -> さぁ出番だ、女王様！
Condition: 25% chance (ai:0 rnd:25)
Change 3 random orbs to Poison orbs, Deal 100% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: ESAttackUPRemainingEnemies(17:47109) -> 魔法カード「寄生虫の暴走」
Condition: One-time use, when <= 1 enemies remain (ai:100 rnd:0) (cost: 1)
Increase damage to 150% for the next 999 turns

#10: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 4

#11: ESEndPath(36:26) -> ESEndPath
end_turn