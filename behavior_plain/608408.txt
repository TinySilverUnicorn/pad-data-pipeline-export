#608408 - Alt. Super Emerald Dragon
monster size: 3
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBindRandom(1:40352) -> Emerald Aura
Condition: One-time use (ai:100 rnd:0) (cost: 2)
Bind 6 random cards for 5 turns

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 6, target rnd 12

#5: ESBranchRemainingEnemies(120:8595) -> ESBranchRemainingEnemies
Branch on remaining enemies <= 1, target rnd 10

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 4, target rnd 9

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 9

#8: ESChangeAttribute(46:40355) -> Jewel Claw
Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESAttackUPCooldown(19:34037) -> Cosmic Liberation
Condition: One-time use (ai:100 rnd:0) (cost: 1)
Increase damage to 1,000% for the next 999 turns

#11: ESChangeAttribute(46:40355) -> Jewel Claw
Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage

#12: ESAttackMultihit(15:34046) -> Cosmic Destruction
Deal 5,000% damage (5 hits, 1,000% each)

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESResolve(73:34043) -> Resolve
Survive attacks with 1 HP when HP > 50%