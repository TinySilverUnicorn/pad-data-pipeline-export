#209579 - Bruno Bucciarati & Zipper Man
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:43719) -> In any case...
	Voids status ailments for 999 turns
	[1] ESAbsorbAttribute(53:43720) -> ...I just need to take him down, right?
	Absorb Water and Light damage for 3 turns, Deal 100% damage

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 15, target rnd 19

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 1, target rnd 8

#5: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#6: ESRandomSpawn(92:43725) -> I will fulfill my duty.
Spawn 6 random Light orbs, Deal 100% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 2, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESDebuffRCV(105:43726) -> I will protect my subordinates.
RCV 25% for 1 turn, Deal 101% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 3, target rnd 16

#13: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#14: ESDebuffATK(130:43727) -> Are you prepared?
ATK -75% for 1 turn, Deal 102% damage

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#17: ESAttackMultihit(15:43728) -> Because I am.
Deal 120% damage (3 hits, 40% each)

#18: ESEndPath(36:26) -> ESEndPath
end_turn

#19: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESBoardChangeAttackBits(85:43722) -> This is...
	Change all orbs to Water, Light, Heal, and Jammer, Deal 100% damage
	[1] ESDebuffMovetime(39:43723) -> ...what it means to have resolve...
	Movetime 50% for 1 turn

#20: ESAttackMultihit(15:43724) -> Arrivederci.
Deal 198% damage (3 hits, 66% each)

#21: ESAttributeResist(72:43729) -> Light halved
Reduce damage from Light attrs by 50%