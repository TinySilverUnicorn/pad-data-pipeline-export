#509910 - GS・ノーチラス
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESOrbSealRow(100:47310) -> 準備はバッチリだよ！
	Seal the 5th row for 5 turns
	[1] ESColumnSpawnMulti(76:47311) -> ミュージックスタート！
	Change the 1st and 6th columns to Heal orbs

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 14

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 11

#8: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#9: ESRandomSpawn(92:47312) -> いっくよー！
Spawn 6 random Wood orbs, Deal 100% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#12: ESUnknown(150:47313) -> テンポアップ！
No description set, Deal 95% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#15: ESUnknown(151:47314) -> ちょっと難しい曲になるよ～
No description set, Deal 90% damage

#16: ESEndPath(36:26) -> ESEndPath
end_turn

#17: SkillSet:
	Condition: On death (ai:0 rnd:0)
	[0] ESEndBattle(40:47316) -> 素敵な時間をありがとう！
	Reduce self HP to 0