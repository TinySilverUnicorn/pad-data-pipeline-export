#509416 - 響想のボーカリスト・イデアル
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESAbsorbCombo(67:47372) -> ふふっ
	Absorb damage when combos <= 8 for 5 turns
	[1] ESComboSkyfall(131:47373) -> 盛り上がっていきましょう！
	For 20 turns, None% chance for combo orb skyfall.

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 2, target rnd 14

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 11

#8: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#9: ESDebuffATKTarget(143:47374) -> まだまだいきます！
For 1 turn, 50% ATK for ???, Deal 85% damage

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#12: ESChangeAttribute(46:47375) -> テンションあげていきますよっ！
Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 80% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn

#14: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#15: ESUnknown(151:47376) -> もう息切れしちゃいましたか？
No description set, Deal 90% damage

#16: ESEndPath(36:26) -> ESEndPath
end_turn