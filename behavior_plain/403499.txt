#403499 - True Reincarnated Liu Bei
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESSkillDelay(89:42792) -> Moral Commanding Deity's Wave
	Delay active skills by 3 turns
	[1] ESNoSkyfall(127:42793) -> This one packs quite a punch...
	No skyfall for 1 turn
	[2] ESDebuffATK(130:42794) -> Take this!
	ATK -50% for 1 turn, Deal 95% damage

#3: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#4: ESBranchCounter(32:22) -> ESBranchCounter
Branch on counter >= 7, target rnd 6

#5: SkillSet:
	[0] ESChangeAttribute(46:42796) -> This one's gonna hurt like hell...
	Change own attribute to random one of Fire, Water, Wood, Light, or Dark
	[1] ESAttackMultihit(15:42797) -> Storm Strike in the Roiling Void
	Deal 120% damage (4 hits, 30% each)

#6: SkillSet:
	[0] ESBindAwoken(88:42623) -> Longevity Surge
	Bind awoken skills for 1 turn
	[1] ESAttackMultihit(15:42624) -> Universal Demise
	Deal 5,000% damage (5 hits, 1,000% each)

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: SkillSet:
	Condition: On death (ai:0 rnd:0)
	[0] ESDebuffRCV(105:42799) -> Here's a souvenir for the afterlife...
	RCV 50% for 3 turns