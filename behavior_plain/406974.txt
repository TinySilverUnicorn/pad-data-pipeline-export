#406974 - 磁界王・マグニートー
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESStatusShield(20:38972) -> 次世代人種ことミュータントが、
	Voids status ailments for 999 turns
	[1] ESBlind62(62:38973) -> この世界を支配する
	Blind all orbs on the board, Deal 80% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBlindStickySkyfall(128:38974) -> いい気になられては困るな、小僧
Condition: When < 50% HP , one-time use (ai:100 rnd:0) (cost: 2)
For 5 turns, 15% chance for skyfall orbs to be blinded for 2 turns, Deal 100% damage

#7: ESBoardChangeAttackBits(85:38977) -> 地球そのものに殴られた気分は？
Condition: When < 20% HP , one-time use (ai:100 rnd:0) (cost: 1)
Change all orbs to Fire, Deal 120% damage

#8: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 12

#9: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#10: ESVoidShieldBig(137:38975) -> マグネトキネシス
Void damage >= 2,000,000,000 for 1 turn, Deal 95% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#13: ESDebuffATK(130:38976) -> マグネトキネシス
ATK -75% for 1 turn, Deal 105% damage

#14: ESEndPath(36:26) -> ESEndPath
end_turn

#15: ESSuperResolve(129:38970) -> ＿マグニートー超根性
Damage which would reduce HP from above 50% to below 50% is nullified