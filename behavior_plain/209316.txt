#209316 - Goss Harag
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESColumnSpawnMulti(77:42223) -> Charging Ice Blade Attack
	Change the 2nd column to Water orbs, Deal 50% damage
	[1] ESAttributeBlock(107:42224) -> Ice Breath
	Unable to match Water orbs for 3 turns
	[2] ESAbsorbThreshold(87:42225) -> Double Arm Slam
	Absorb damage when damage >= 800,000,000 for 3 turns, Deal 50% damage

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 15, target rnd 12

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 9

#5: ESAttackMultihit(15:42231) -> Ice Blade Punisher
Condition: 33% chance (ai:33 rnd:0)
Deal 102% damage (3 hits, 34% each)

#6: ESSpinnersRandom(109:42232) -> Double Ice Blade Sweep
Condition: 50% chance (ai:50 rnd:0)
Random 2 orbs change every 1.0s for 1 turn, Deal 100% damage

#7: ESOrbChangeAttackBits(108:42233) -> Jumping Ice Blade Slam
Change all Heal orbs to Water orbs, Deal 100% damage

#8: ESEndPath(36:26) -> ESEndPath
end_turn

#9: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESRowSpawnMulti(79:42229) -> Ice Blade Rampage
	Change the 1st and 2nd rows to Water orbs, Deal 50% damage
	[1] ESOrbLock(94:42230) -> Plunging Blade
	Lock all Water orbs, Deal 51% damage

#10: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 50, target rnd 5

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 16

#13: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#14: ESDebuffATKTarget(143:42226) -> Flurry
For 1 turn, 25% ATK for 2 random subs, Deal 101% damage

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESAttackMultihit(15:42227) -> Ice Shard Attack
Deal 180% damage (3 hits, 60% each)

#17: ESEndPath(36:26) -> ESEndPath
end_turn