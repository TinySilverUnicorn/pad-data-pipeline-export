#701340 - True Rich Gold Dragon
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESOrbLock(94:44172) -> Vault Lock
	Lock all orbs
	[1] ESSkyfallLocked(96:44173) -> Gold Rush
	Locked random skyfall +100% for 2 turns

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESDispel(6:44174) -> Gold Leaf Coating
Voids player buff effects, Deal 90% damage

#7: ESDebuffATK(130:44175) -> New Year's Luxurious Gift
Condition: 50% chance (ai:0 rnd:50)
ATK -75% for 1 turn, Deal 100% damage

#8: ESAttackMultihit(15:44176) -> Gulp!
Condition: 50% chance (ai:0 rnd:50)
Deal 110% damage (2 hits, 55% each)

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESResolve(73:44170) -> ＿Rich Resolve
Survive attacks with 1 HP when HP > 50%