#9595 - Weather Forecast
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESSkyfall(68:43694) -> Weather Forecast
	Water skyfall +15% for 3 turns
	[1] ESCloud(104:43693) -> Clouds
	A 1×2 rectangle of clouds appears for 3 turns at a random location

#3: ESRowSpawnMulti(79:43695) -> Severe Rainstorm
Condition: 33% chance (ai:33 rnd:0)
Change the 3rd row to Water orbs, Deal 100% damage

#4: ESDamageShield(74:43696) -> Cloud Suits
Condition: 50% chance (ai:50 rnd:0)
Reduce damage from all sources by 25% for 1 turn, Deal 100% damage

#5: ESOrbChangeAttackBits(108:43697) -> Flame of Aerodynamic Friction
Change all Wood orbs to Fire orbs, Deal 101% damage

#6: ESEndPath(36:26) -> ESEndPath
end_turn