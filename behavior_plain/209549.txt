#209549 - Jotaro Kujo & Star Platinum The World
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 2)
	[0] ESStatusShield(20:43800) -> Star Platinum
	Voids status ailments for 999 turns
	[1] ESUnknown(150:43801) -> Bring it!
	No description set
	[2] ESRowSpawnMulti(79:43802) -> Star Finger
	Change the 1st, 3rd, and 5th rows to Light orbs and the 2nd and 4th rows to Dark orbs, Deal 100% damage

#3: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 15, target rnd 12

#4: ESBranchHP(28:18) -> ESBranchHP
Branch on hp < 70, target rnd 8

#5: ESAttackMultihit(15:43810) -> Punch Rush
Condition: 50% chance (ai:50 rnd:0)
Deal 102% damage (3 hits, 34% each)

#6: ESRandomSpawn(92:43811) -> Ora
Spawn 4 random Light, Dark, and Jammer orbs, Deal 101% damage

#7: ESEndPath(36:26) -> ESEndPath
end_turn

#8: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESVoidShield(71:43806) -> Good grief.
	Void damage >= 1,000,000,000 for 3 turns
	[1] ESAbsorbCombo(67:43807) -> You pissed me off.
	Absorb damage when combos <= 7 for 3 turns, Deal 101% damage

#9: ESSpinnersRandom(109:43808) -> Time is moving again.
Condition: 50% chance (ai:50 rnd:0)
Random 2 orbs change every 0.5s for 1 turn, Deal 100% damage

#10: ESDebuffATK(130:43809) -> Ora!
ATK -75% for 1 turn, Deal 100% damage

#11: ESEndPath(36:26) -> ESEndPath
end_turn

#12: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 16

#13: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#14: ESDebuffMovetime(39:43803) -> Star Platinum. The World.
Movetime 25% for 1 turn

#15: ESEndPath(36:26) -> ESEndPath
end_turn

#16: ESAttackMultihit(15:43804) -> Ora ora ora ora ora ora ora
Deal 210% damage (3 hits, 70% each)

#17: ESSuperResolve(129:43812) -> Super Resolve 90%
Damage which would reduce HP from above 70% to below 70% is nullified