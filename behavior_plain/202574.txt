#202574 - Alt. Osechi Party Fox, Mitsuki
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESBoardChangeAttackBits(85:44109) -> Today's a day to celebrate!
	Change all orbs to Heal, Deal 55% damage
	[1] ESUnknown(151:44110) -> Hey, come join me!
	No description set

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 10

#7: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#8: SkillSet:
	[0] ESBoardChangeAttackBits(85:44112) -> Not really getting into the spirit, huh...
	Change all orbs to Jammer, Deal 40% damage
	[1] ESAttackMultihit(15:44113) -> Egg-wrapped, kelp-wrapped, tail-wrapped ♪
	Deal 60% damage (4 hits, 15% each)

#9: ESEndPath(36:26) -> ESEndPath
end_turn

#10: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#11: SkillSet:
	[0] ESBoardChangeAttackBits(85:44115) -> Are you feeling better now?
	Change all orbs to Poison, Deal 100% damage
	[1] ESTargetedSkillHaste(139:44116) -> Try to liven up, at least for today!
	Haste all cards' skills by 1 turn

#12: ESEndPath(36:26) -> ESEndPath
end_turn

#13: ESTypeResist(118:44107) -> ＿Mitsuki God Halved
Reduce damage from God types by 50%