#209739 - ウイングガンダムゼロ
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0

#1: ESPreemptive(49:318) -> ESPreemptive
Enable preempt if level 1

#2: ESBranchFlag(43:10) -> ESBranchFlag
Branch on flag & 1, target rnd 6

#3: ESFlagOperation(22:12) -> ESFlagOperation
flag SET 0b1

#4: SkillSet:
	[0] ESDebuffRCV(105:46481) -> ゼロよ………
	RCV 0% for 2 turns
	[1] ESBlind62(62:46482) -> 俺を導いてくれ………！
	Blind all orbs on the board, Deal 100% damage

#5: ESEndPath(36:26) -> ESEndPath
end_turn

#6: SkillSet:
	Condition: One-time use (ai:100 rnd:0) (cost: 1)
	[0] ESAbsorbAttribute(53:46484) -> ゼロシステム
	Absorb Fire, Water, and Wood damage for 1 turn
	[1] ESDamageShield(74:46485) -> 俺は………俺は死なない！
	Reduce damage from all sources by 50% for 1 turn, Deal 90% damage

#7: ESBranchCounter(31:21) -> ESBranchCounter
Branch on counter = 1, target rnd 11

#8: ESSetCounter(26:16) -> ESSetCounter
counter + 1

#9: ESAttackMultihit(15:46486) -> マシンキャノン
Deal 102% damage (6 hits, 17% each)

#10: ESEndPath(36:26) -> ESEndPath
end_turn

#11: ESSetCounter(25:15) -> ESSetCounter
counter = 0

#12: ESColumnSpawnMulti(77:46487) -> ツインバスターライフル
Change the 1st and 3rd columns to Light orbs, Deal 100% damage

#13: ESEndPath(36:26) -> ESEndPath
end_turn