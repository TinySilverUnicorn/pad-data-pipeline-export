#509591 - Ermes Costello & Smack
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 509591
approved: False

level: 1
 type: PREEMPT
 | (43907:67) Now, scream all you want.
 | Absorb damage when combos <= 9 for 1 turn
 group:
 | group:
 | condition: always turn 1
   | (43908:107) Uworiyaaaaa!
   | Unable to match Fire and Wood orbs for 2 turns, Deal 100% damage
 | group:
   | condition: 50% chance
   | (43915:15) Kick
   | Deal 101% damage
   | (43916:92) I found you!
   | Spawn 4 random Wood orbs, Deal 100% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: execute repeatedly, turn 1 of 2
     | (43909:83) Place a sticker. + Create a copy of the object.
     | Seal the 1st and 6th columns for 1 turn + Random 2 orbs change every 0.5s for 1 turn, Deal 101% damage
   | group:
   | condition: execute repeatedly, turn 2 of 2
     | (43912:83) Peel the sticker. + It causes a fissure and you can destroy it.
     | No description set + Deal 102% damage (3 hits, 34% each), Deal 102% damage (3 hits, 34% each)