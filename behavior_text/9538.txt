#9538 - Jonathan Joestar
monster size: 5
new AI: True
start/max counter: 1
counter increment: 1
monster_id: 9538
approved: False

level: 1
 type: PREEMPT
 | (43598:83) I shall rid the world... + ...of your demonic ambition!
 | Voids status ailments for 999 turns + Reduce damage from all sources by 25% for 1 turn
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 2
   | (43595:17) I'll transfer my Hamon!
   | Increase damage to 150% for the next 1 turn
 | group:
 | condition: execute repeatedly, turn 2 of 2
   | (43596:15) And slice you with Blueford's sword of luck and pluck!
   | Deal 69% damage (3 hits, 23% each)