#309316 - Goss Harag
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 309316
approved: False

level: 1
 type: PREEMPT
 | (42298:107) Ice Breath
 | Unable to match Water orbs for 2 turns
 group:
 | group:
   | condition: 33% chance
   | (42303:15) Ice Blade Punisher
   | Deal 102% damage (3 hits, 34% each)
   | condition: 50% chance
   | (42304:15) Jumping Ice Blade Slam
   | Deal 101% damage
   | (42305:94) Flurry
   | Lock 4 random orbs, Deal 100% damage
 | group:
 | condition: turn 1, hp <= 49
   | (42300:83) Ice Blade Rampage + Plunging Blade
   | Change the 1st row to Water orbs + Lock all Water orbs, Deal 51% damage