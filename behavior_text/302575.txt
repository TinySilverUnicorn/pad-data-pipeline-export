#302575 - Alt. Ancient Fire Dragon, Anomalocaris
monster size: 4
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 302575
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (44505:1) Fire Dragon's Mainspring Key
 | Bind 6 random cards for 3 turns
 group:
 | group:
 | condition: turn 1
   | (44510:46) Mainspring Key Change
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
 | condition: turn 3
   | (44510:46) Mainspring Key Change
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
 | condition: turn 5
   | (44510:46) Mainspring Key Change
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
   | (44526:15) Apocalyptic Star Crushing
   | Deal 5,000% damage (5 hits, 1,000% each)
 type: REMAINING
 condition: when 2 enemies remain, hp <= 100
 | group:
 | condition: turn 1
   | (44528:19) Star Machine's Release
   | Increase damage to 1,000% for the next 999 turns
 | group:
 | condition: turns 2-5
   | (44510:46) Mainspring Key Change
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage