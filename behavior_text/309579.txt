#309579 - Bruno Bucciarati & Zipper Man
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 309579
approved: False

level: 1
 type: PREEMPT
 | (43822:83) In any case... + ...I just need to take him down, right?
 | Voids status ailments for 999 turns + Spawn 2 random Water, Light, and Dark orbs
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 4
   | (43825:92) I will fulfill my duty.
   | Spawn 4 random Light orbs, Deal 100% damage
 | group:
 | condition: execute repeatedly, turn 2 of 4
   | (43826:105) I will protect my subordinates.
   | RCV 50% for 1 turn, Deal 101% damage
 | group:
 | condition: execute repeatedly, turn 3 of 4
   | (43827:130) Are you prepared?
   | ATK -50% for 1 turn, Deal 102% damage
 | group:
 | condition: execute repeatedly, turn 4 of 4
   | (43828:15) Because I am.
   | Deal 114% damage (3 hits, 38% each)