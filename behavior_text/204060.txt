#204060 - Abyssal Hands War Goddess, Nemain
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 204060
approved: False

level: 1
 type: PASSIVE
 | (41549:73) ＿Nemain Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (41550:83) Caliginous Lakeshore + Voice of Madness
 | Disable active skills for 3 turns + RCV 0% for 3 turns
 group:
 | group:
 | condition: turn 1
   | (41553:83) Omen of Misfortune + Caledfwlch
   | Bind 3 random subs for 5 turns + Player -99% HP
 | group:
 | condition: turn 2
   | (41556:83) Bloodstained Armor + Caledfwlch
   | For 3 turns, 50% ATK for both leaders + Player -99% HP
 | group:
 | condition: turn 3
   | (41558:83) Hexing Scream + Caledfwlch
   | For 3 turns, 50% ATK for 4 random subs + Player -99% HP
 | group:
 | condition: turn 4
   | (41560:83) Abyssal Madness + Caledfwlch
   | ATK -50% for 3 turns + Player -99% HP
 | group:
   | (41562:83) Slumber + Deathly Roar of Madness
   | No description set + Deal 120% damage (3 hits, 40% each), Deal 120% damage (3 hits, 40% each)