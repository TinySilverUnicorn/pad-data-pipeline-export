#203656 - Nargacuga
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0
monster_id: 203656
approved: False

level: 1
 type: PASSIVE
 | (41992:72) Dark Halved
 | Reduce damage from Dark attrs by 50%
 type: PREEMPT
 | (41978:83) Menace + Tail Slam
 | Voids status ailments for 999 turns + Change all orbs to Dark, Deal 110% damage
 group:
 | group:
 | condition: always turn 1
   | (41981:83) Its tail is stuck and can't move! + Enraged
   | Do nothing + Increase damage to 150% for the next 999 turns
 | group:
   | condition: 33% chance
   | (41989:15) Cutwing
   | Deal 102% damage (3 hits, 34% each)
   | condition: 50% chance
   | (41990:62) Springing Strike
   | Blind all orbs on the board, Deal 102% damage
   | (41991:63) Roar
   | Bind 2 random subs for 2 turns, Deal 100% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: turn 2
     | (41984:83) Tail Sweep + Spiked Tail Slam
     | Change the 2nd and 4th rows to Dark orbs + Deal 60% damage (3 hits, 20% each), Deal 50% damage
   | group:
     | condition: 50% chance
     | (41987:74) Dash
     | Reduce damage from all sources by 75% for 1 turn, Deal 100% damage
     | (41988:92) Tailspike Shot
     | Spawn 7 random Jammer orbs, Deal 101% damage