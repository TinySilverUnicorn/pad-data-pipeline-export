#404836 - Alt. Raging Thunder Almighty God, Zeus - Giga
monster size: 5
new AI: True
start/max counter: 4
counter increment: 1
monster_id: 404836
approved: False

level: 1
 type: PREEMPT
 | (33289:83) Protection of the Gods + Raging Thunder's Might + Divine King's Armor
 | Voids status ailments for 999 turns + Absorb damage when combos <= 10 for 10 turns + Void damage >= 100,000,000 for 999 turns
 group:
 | group:
   | group:
   | condition: turns 1-4 while HP > 49
     | (33288:15) Divine King's Imperial Lightning
     | Deal 1,250% damage
   | group:
   | condition: turn 5 while HP > 6
     | (33285:83) Divine King's Changing Cloak + Divine King's Imperial Lightning
     | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Deal 1,250% damage, Deal 1,250% damage
   | group:
   | condition: turns 6-9 while HP > 6
     | (33288:15) Divine King's Imperial Lightning
     | Deal 1,250% damage
   | group:
   | condition: turn 10 while HP > 6
     | (11684:74) Divine King's Lightning Cloak
     | Reduce damage from all sources by 75% for 999 turns
   | group:
   | condition: execute repeatedly, turn 1 of 5
     | (33285:83) Divine King's Changing Cloak + Divine King's Imperial Lightning
     | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Deal 1,250% damage, Deal 1,250% damage
   | group:
   | condition: execute repeatedly, turn 2-5 of 5
     | (33288:15) Divine King's Imperial Lightning
     | Deal 1,250% damage
 | group:
 | condition: turn 1 while HP > 6, hp <= 49
   | (33286:83) Almighty God's Majesty + Divine King's Imperial Lightning
   | Voids player buff effects + Deal 1,250% damage, Deal 1,250% damage
 | group:
 | condition: hp <= 6
   | group:
   | condition: turn 1
     | (11683:17) Almighty God's Fighting Spirit
     | Increase damage to 500% for the next 99 turns
   | group:
     | (33287:83) Divine King's Wave + Divine King's Imperial Lightning
     | Bind awoken skills for 1 turn + Deal 1,250% damage, Deal 1,250% damage