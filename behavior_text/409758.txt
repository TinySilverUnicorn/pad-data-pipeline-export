#409758 - キュベレイ
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 409758
approved: False

level: 4
 type: PREEMPT
 | (46054:83) このキュベレイ……… + 見くびってもらっては困る！
 | Mortal Poison skyfall +15% for 2 turns + No skyfall for 2 turns
 group:
 | group:
   | group:
   | condition: execute repeatedly, turn 1 of 3
     | (46058:109) ファンネル
     | Random 3 orbs change every 1.0s for 1 turn, Deal 95% damage
   | group:
   | condition: execute repeatedly, turn 2 of 3
     | (46059:92) ビーム・ガン
     | Spawn 5 random Fire and Dark orbs, Deal 90% damage
   | group:
   | condition: execute repeatedly, turn 3 of 3
     | (46060:85) 恥を知れ！俗物！
     | Change all orbs to Fire and Dark, Deal 115% damage
 | group:
 | condition: turn 1, hp <= 49
   | (46057:88) プレッシャー
   | Bind awoken skills for 1 turn, Deal 20% damage