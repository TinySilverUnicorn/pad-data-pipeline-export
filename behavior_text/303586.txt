#303586 - Flashblade Mechanical Star God, Algedi
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 303586
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (44565:83) You're gonna fight me now. + Let's see how you dance! + Come at me anytime!
 | Voids status ailments for 999 turns + Specific orbs change every 1.0s for 1 turn + Fix orb movement starting point to random position on the board
 group:
 | group:
   | group:
   | condition: turn 1 while HP > 49
     | (44570:83) Mechanical Star God's Gaze + ...Is that really the best you can do? + You lack training.
     | Player -99% HP + Increase damage to 150% for the next 6 turns + For 3 turns, 1% ATK for both leaders
   | group:
   | condition: turns 2-5 while HP > 2
     | (44578:15) Fakhir Yuqtal
     | Deal 100% damage (10 hits, 10% each)
   | group:
     | (44524:83) Star Machine's Wave + Apocalyptic Star Crushing
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turn 1 while HP > 2, hp <= 49
   | (44574:83) Mechanical Star God's Gaze + You're quite good, but... + ...this is where the real fight begins!
   | Player -99% HP + Reduce damage from all sources by 75% for 6 turns + For 3 turns, 1% ATK for 2 random subs
 | group:
 | condition: turns 1-5, hp <= 2
   | (44569:15) Fakhir Yuqtal Gestalt
   | Deal 1,000% damage (10 hits, 100% each)