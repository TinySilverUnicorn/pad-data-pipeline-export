#706398 - Forest Key Heir, Amlynea
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 706398
approved: False

level: 1
 type: PREEMPT
 | (37589:89) Deer Wind
 | Delay active skills by 3 turns
 group:
 | condition: 50% chance
 | (37590:92) Deer Antlers
 | Spawn 4 random Wood orbs, Deal 100% damage
 | condition: 50% chance
 | (37591:15) Deer Blade
 | Deal 110% damage (2 hits, 55% each)