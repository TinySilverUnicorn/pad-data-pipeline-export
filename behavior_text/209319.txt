#209319 - Pukei-Pukei
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 209319
approved: False

level: 1
 type: PASSIVE
 | (41966:72) Wood Halved
 | Reduce damage from Wood attrs by 50%
 type: PREEMPT
 | (41957:83) Poison Spit + Poison Cloud Spray
 | Spawn 6 random Poison orbs + No description set
 group:
 | group:
   | condition: 33% chance
   | (41963:63) Tongue Whip
   | Bind 2 random subs for 2 turns, Deal 100% damage
   | condition: 50% chance
   | (41964:15) Gliding Claw Combo
   | Deal 102% damage (3 hits, 34% each)
   | (41965:64) Enraged Poison Cloud Spray
   | Change 4 random orbs to Poison orbs, Deal 101% damage
 | group:
 | condition: turn 1, hp <= 49
   | (41960:83) Enraged Poison Triple Spit + Dash
   | Change all orbs to Wood, Dark, and Poison + Player -99% HP, Deal 105% damage