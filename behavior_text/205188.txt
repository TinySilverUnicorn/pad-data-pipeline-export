#205188 - Gore Magala
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 205188
approved: False

level: 1
 type: PASSIVE
 | (42106:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (42096:83) Roar + Frenzy Burst
 | Voids status ailments for 999 turns + Change all orbs to Dark, Jammer, and Poison, Deal 100% damage
 group:
 | group:
   | condition: 25% chance
   | (42102:15) Continuous Aerial Lunge
   | Deal 102% damage (3 hits, 34% each)
   | condition: 25% chance
   | (42103:102) Close Range Frenzy Burst
   | Spawn 7 random locked Bomb orbs, Deal 100% damage
   | condition: 25% chance
   | (42104:71) Aerial Lunge
   | Void damage >= 700,000,000 for 1 turn, Deal 101% damage
   | condition: 25% chance
   | (42105:77) Tackle Slam
   | Change the 2nd and 6th columns to Dark orbs, Deal 102% damage
 | group:
 | condition: hp <= 1
   | (42099:83) Frenzy + Frenzied Breath
   | Enemy recover 100% HP + No description set, Deal 100% damage