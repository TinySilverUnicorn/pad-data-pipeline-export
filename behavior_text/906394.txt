#906394 - Flame Key Heir, Gileon
monster size: 3
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 906394
approved: False

level: 1
 type: PREEMPT
 | (41013:85) Blazing Book
 | Change all orbs to Fire, Heal, and Jammer, Deal 120% damage
 group:
 | condition: 50% chance
 | (41014:92) Blazing Lamp
 | Spawn 4 random Fire orbs, Deal 100% damage
 | condition: 50% chance
 | (41015:15) Blazing Claw
 | Deal 120% damage (4 hits, 30% each)