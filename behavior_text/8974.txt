#8974 - Wu Fengming
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 8974
approved: False

level: 4
 type: PREEMPT
 | (39131:67) Massive Siege Tower
 | Absorb damage when combos <= 5 for 3 turns
 group:
 | group:
   | group:
   | condition: turn 1 while HP > 49
     | (39132:68) Mobilize all our archers.
     | Water skyfall +15% for 3 turns
   | group:
   | condition: turns 2-3 while HP > 49
     | (39133:83) We'll come up on either side... + ...and crush them.
     | Change the 1st and 6th columns to Water and Light orbs + Deal 60% damage (3 hits, 20% each), Deal 50% damage
   | group:
   | condition: turn 4 while HP > 49
     | (39136:96) Ballista Brigade
     | Locked Water skyfall +100% for 3 turns
   | group:
     | (39133:83) We'll come up on either side... + ...and crush them.
     | Change the 1st and 6th columns to Water and Light orbs + Deal 60% damage (3 hits, 20% each), Deal 50% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: turn 1
     | (39137:17) Send in the main army's elite unit!
     | Increase damage to 150% for the next 999 turns
   | group:
   | condition: turn 2
     | (39132:68) Mobilize all our archers.
     | Water skyfall +15% for 3 turns
   | group:
   | condition: turn 4
     | (39133:83) We'll come up on either side... + ...and crush them.
     | Change the 1st and 6th columns to Water and Light orbs + Deal 60% damage (3 hits, 20% each), Deal 50% damage
   | group:
   | condition: turn 5
     | (39136:96) Ballista Brigade
     | Locked Water skyfall +100% for 3 turns