#209574 - Jean Pierre Polnareff & Silver Chariot
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0
monster_id: 209574
approved: False

level: 1
 type: PREEMPT
 | (43739:74) Silver Chariot
 | Reduce damage from all sources by 50% for 1 turn, Deal 100% damage
 group:
 | group:
 | condition: always turn 1
   | (43740:83) Armorless + Afterimages of My Stand
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Specific orbs change every 1.0s for 3 turns, Deal 100% damage
 | group:
   | condition: 33% chance
   | (43746:15) Die!
   | Deal 102% damage (3 hits, 34% each)
   | condition: 50% chance
   | (43747:79) I'm consumed by a burning will to fight.
   | Change the 2nd row to Water orbs, Deal 100% damage
   | (43748:77) I can't run!
   | Change the 5th column to Light orbs, Deal 100% damage
 | group:
 | condition: turn 2, hp <= 49
   | (43744:17) Uwoooooooooooooo!!
   | Increase damage to 150% for the next 999 turns