#403542 - True Clever Egg Dragon, Bacches
monster size: 4
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 403542
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (42613:127) Egg Guard
 | No skyfall for 3 turns
 group:
 | group:
 | condition: turn 1
   | (42614:46) Egg Strike
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
 | condition: turn 3
   | (42614:46) Egg Strike
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
 | condition: turns 5-6
   | (42614:46) Egg Strike
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
   | (42624:15) Universal Demise
   | Deal 5,000% damage (5 hits, 1,000% each)
 type: REMAINING
 condition: when 1 enemies remain, hp <= 100
 | group:
 | condition: turn 1
   | (42625:17) Longevity Release
   | Increase damage to 1,000% for the next 999 turns
 | group:
 | condition: turns 2-6
   | (42614:46) Egg Strike
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage