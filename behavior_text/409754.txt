#409754 - グフ
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 409754
approved: False

level: 1
 type: PREEMPT
 | (46595:83) フフ……… + この風、この肌触りこそ戦争よ！
 | Change all orbs to Water and Dark + A row of clouds appears for 5 turns at 5th row, 1st column, Deal 85% damage
 group:
 | group:
 | condition: turn 1
   | (46598:83) 不慣れな奴だな………！ + 気の毒だが次でいただく！
   | Bind active skills for 5 turns + Increase damage to 200% for the next 1 turn
 | group:
 | condition: turn 2
   | (46601:143) ザクとは違うのだよ！ザクとは！！
   | For 3 turns, 1% ATK for both leaders, Deal 102% damage
 | group:
 | condition: execute repeatedly, turn 1 of 2
   | (46602:15) 5連装75mm機関砲
   | Deal 85% damage (5 hits, 17% each)
 | group:
 | condition: execute repeatedly, turn 2 of 2
   | (46603:65) ヒート・ロッド
   | Bind 2 random subs for 3 turns