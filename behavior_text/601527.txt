#601527 - True High Water Ninja
monster size: 4
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 601527
approved: False

level: 1
 type: PREEMPT
 | (39713:150) Water Ninja Art
 | No description set
 group:
 | group:
 | condition: turn 2
   | (39717:46) Ninpo - Transformation
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
 | condition: turn 4
   | (39717:46) Ninpo - Transformation
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
   | (39741:15) Thousandfold Demise
   | Deal 5,000% damage (5 hits, 1,000% each)
 type: REMAINING
 condition: when 1 enemies remain, hp <= 100
 | group:
 | condition: turn 1
   | (39743:19) Azure Sky Release
   | Increase damage to 1,000% for the next 999 turns
 | group:
 | condition: turns 2-6
   | (39717:46) Ninpo - Transformation
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage