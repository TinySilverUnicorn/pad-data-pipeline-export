#300045 - Alt. Big Baddie
monster size: 2
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 300045
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 | condition: when 2 enemies remain
 | (45615:122) Enemy's next turn changed
 | Enemy turn counter change to 1
 group:
 | group:
 | condition: turns 1-6
   | (45506:15) Big Lil' Blast
   | Deal 2,000% damage (5 hits, 400% each)
 | group:
   | (45534:15) Doom of a Trillion Catastrophes
   | Deal 5,000% damage (5 hits, 1,000% each)
 type: REMAINING
 condition: when 2 enemies remain, hp <= 100
 | group:
 | condition: turn 1
   | (45536:19) Release of a Trillion Evil Omens
   | Increase damage to 1,000% for the next 999 turns
 | group:
 | condition: turns 2-6
   | (45506:15) Big Lil' Blast
   | Deal 2,000% damage (5 hits, 400% each)