#203585 - Blade Pincers Mechanical Star God, Acubens
monster size: 5
new AI: True
start/max counter: 63
counter increment: 0
monster_id: 203585
approved: False

level: 1
 type: PREEMPT
 | (40449:83) Cancer's Carapace + I wonder if you can strike me down... + ...in just 6 turns?
 | Void damage >= 3,000,000,000 for 999 turns + Voids status ailments for 6 turns + For 6 turns, 25% ATK for 6 random cards
 group:
 | group:
 | condition: turn 1
   | (40454:107) Cancer's Pincers
   | Unable to match Fire orbs for 1 turn, Deal 100% damage
 | group:
 | condition: turn 2
   | (40455:107) Cancer's Pincers
   | Unable to match Water orbs for 1 turn, Deal 100% damage
 | group:
 | condition: turn 3
   | (40456:107) Cancer's Pincers
   | Unable to match Wood orbs for 1 turn, Deal 100% damage
 | group:
 | condition: turn 4
   | (40457:107) Cancer's Pincers
   | Unable to match Light orbs for 1 turn, Deal 100% damage
 | group:
 | condition: turn 5
   | (40458:107) Cancer's Pincers
   | Unable to match Dark orbs for 1 turn, Deal 100% damage
 | group:
   | (40453:15) This is the end for you!
   | Deal 1,000% damage (2 hits, 500% each)