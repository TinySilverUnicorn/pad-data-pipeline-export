#209659 - The True Dark King, Vearn
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 209659
approved: False

level: 1
 type: PASSIVE
 | (45082:129) ＿True Vearn Super Resolve
 | Damage which would reduce HP from above 70% to below 70% is nullified
 type: PREEMPT
 | (45083:83) Now! Witness this!! + Demonic Guard of Heaven and Earth
 | Specific orbs change every 1.0s for 2 turns + Reduce damage from all sources by 75% for 1 turn
 group:
 | group:
 | condition: always turn 1
   | (45087:83) Phoenix Wing! + Calamitous End! + Kaiser Phoenix!
   | For 1 turn, 50% ATK for 4 random subs + Absorb damage when combos <= 8 for 1 turn + Change all orbs to Fire and Mortal Poison, Deal 105% damage
 | group:
   | group:
   | condition: execute repeatedly, turn 1 of 2
     | (45091:99) Palm Strike
     | Seal the 1st column for 1 turn, Deal 95% damage
   | group:
   | condition: execute repeatedly, turn 2 of 2
     | (45092:151) Push Force
     | No description set, Deal 97% damage
 | group:
 | condition: hp <= 69
   | group:
   | condition: turn 2
     | (45086:74) Demonic Guard of Heaven and Earth
     | Reduce damage from all sources by 75% for 1 turn
   | group:
   | condition: turn 3
     | (45087:83) Phoenix Wing! + Calamitous End! + Kaiser Phoenix!
     | For 1 turn, 50% ATK for 4 random subs + Absorb damage when combos <= 8 for 1 turn + Change all orbs to Fire and Mortal Poison, Deal 105% damage