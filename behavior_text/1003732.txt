#1003732 - Violent Dragon Lord of Tyranny, Valten
monster size: 5
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 1003732
approved: False

level: 1
 type: PREEMPT
 | (33961:126) Prominence Shout
 | Change board size to 7x6 for 2 turns
 group:
 | (33961:126) Prominence Shout
 | Change board size to 7x6 for 2 turns