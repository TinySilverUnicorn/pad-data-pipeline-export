#9036 - Mefilas (Shin Ultraman)
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 9036
approved: False

level: 1
 type: PASSIVE
 | (38550:118) Attacker Halved
 | Reduce damage from Attacker types by 50%
 type: PREEMPT
 | (38544:83) Demonstration + Suggestion for Joint Struggle
 | No skyfall for 3 turns + Poison skyfall +15% for 3 turns
 group:
 | condition: 33% chance
 | (38547:104) Kick Up
 | A 2×2 square of clouds appears for 1 turn at a random location, Deal 101% damage
 | condition: 50% chance
 | (38548:109) Roundhouse Kick
 | Random 1 orbs change every 0.5s for 1 turn, Deal 100% damage
 | (38549:99) Grip Beam
 | Seal the 1st column for 1 turn, Deal 100% damage