#204610 - リリアナ・ヴェス
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 204610
approved: False

level: 1
 type: PREEMPT
 | (48017:53) 悪魔の王たちとの契約
 | Absorb Dark damage for 3 turns
 group:
 | condition: 25% chance
 | (48018:94) 鎖のヴェール
 | Lock 5 random orbs, Deal 100% damage
 | condition: 25% chance
 | (48019:153) 剃刀の刃のような鋭さ
 | No description set, Deal 100% damage
 | condition: 25% chance
 | (48020:151) 生きているものを腐敗させる
 | No description set, Deal 100% damage
 | condition: 25% chance
 | (48021:15) 黒中の黒の魔法
 | Deal 102% damage (3 hits, 34% each)