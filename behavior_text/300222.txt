#300222 - Siegfried, the Blue Champion
monster size: 3
new AI: True
start/max counter: 15
counter increment: 0
monster_id: 300222
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (45603:83) Nibelungen + Blue Sword of Domination + Balmung
 | No description set + Bind 4 random subs for 4 turns + No description set, Deal 130% damage
 group:
 | group:
 | condition: always turns 4-6
   | (45611:155) Balmung
   | No description set, Deal 1,000% damage
 | group:
   | group:
   | condition: turn 1 while HP > 2
     | (45608:155) Balmung
     | No description set, Deal 50% damage
   | group:
   | condition: turn 2 while HP > 2
     | (45609:155) Balmung
     | No description set, Deal 100% damage
   | group:
   | condition: turn 3 while HP > 2
     | (45610:155) Balmung
     | No description set, Deal 150% damage
   | group:
     | (45532:83) Wave of a Trillion Evil Omens + Doom of a Trillion Catastrophes
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turns 1-3, hp <= 2
   | (45611:155) Balmung
   | No description set, Deal 1,000% damage