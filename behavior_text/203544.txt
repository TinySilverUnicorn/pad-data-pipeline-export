#203544 - 賢玉龍・アポルォ
monster size: 4
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 203544
approved: False

level: 1
 type: PREEMPT
 | (41620:71) エッグシェル
 | Void damage >= 1,000,000 for 2 turns
 group:
 | (41622:15) 転がった！
 | Deal 99% damage (3 hits, 33% each)