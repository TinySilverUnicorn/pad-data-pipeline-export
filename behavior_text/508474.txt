#508474 - Geometric Dragon Illusionary Artist, Prim
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 508474
approved: False

level: 1
 type: PREEMPT
 | (43155:83) Here's my Illusionary Art! + Polygon Dragon
 | Absorb damage when combos <= 4 for 3 turns + Change board size to 7x6 for 3 turns
 group:
 | group:
   | group:
   | condition: turns 1-9 while HP > 49
     | condition: 25% chance
     | (43158:100) Thrilling Edge
     | Seal the 1st row for 1 turn
     | condition: 25% chance
     | (43159:109) Round Pivot
     | Random 1 orbs change every 1.0s for 1 turn
     | condition: 25% chance
     | (43160:108) Smooth Clay
     | Change all Heal orbs to Fire and Water orbs, Deal 100% damage
     | condition: 25% chance
     | (43161:77) Texturing
     | Change the 1st column to Water orbs and the 6th column to Fire orbs, Deal 100% damage
   | group:
     | (43147:92) Make sure to erase 4 Orbs!
     | Spawn 4 random Water, Wood, and Light orbs, Deal 50% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: turn 1
     | (43162:83) Are you enjoying my art? + If you just gape away like that... + ...you might miss something important!
     | Player recover 100% HP + Fix orb movement starting point to random position on the board + Increase damage to 150% for the next 999 turns
   | group:
   | condition: turn 2
     | (43166:85) High Polygon Breath
     | Change all orbs to Fire, Water, Wood, and Light, Deal 70% damage
   | group:
   | condition: turn 3
     | (43167:79) Low Polygon Fang
     | Change the 1st and 5th rows to Heal orbs, Deal 80% damage
   | group:
   | condition: turn 4
     | (43166:85) High Polygon Breath
     | Change all orbs to Fire, Water, Wood, and Light, Deal 70% damage
   | group:
   | condition: turn 5
     | (43167:79) Low Polygon Fang
     | Change the 1st and 5th rows to Heal orbs, Deal 80% damage
   | group:
   | condition: turn 6
     | (43166:85) High Polygon Breath
     | Change all orbs to Fire, Water, Wood, and Light, Deal 70% damage
   | group:
   | condition: turn 7
     | (43167:79) Low Polygon Fang
     | Change the 1st and 5th rows to Heal orbs, Deal 80% damage
   | group:
   | condition: turn 8
     | (43166:85) High Polygon Breath
     | Change all orbs to Fire, Water, Wood, and Light, Deal 70% damage
   | group:
   | condition: turn 9
     | (43167:79) Low Polygon Fang
     | Change the 1st and 5th rows to Heal orbs, Deal 80% damage