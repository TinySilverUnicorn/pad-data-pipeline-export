#309716 - 封印されし凶兆
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 309716
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (45821:83) Sealing Barrier + A Trillion Omens of Desperation + Release of a Trillion Evil Omens
 | Voids status ailments for 999 turns + Absorb damage when combos <= 13 for 1 turn + Increase damage to 1,000% for the next 999 turns
 group:
 | (45826:83) Trillion Dragon's Mutation + Doom of a Trillion Catastrophes
 | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Deal 1,000% damage (4 hits, 250% each), Deal 1,000% damage (4 hits, 250% each)