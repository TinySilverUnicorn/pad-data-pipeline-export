#806402 - Moon Key Heir, Euchs
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 806402
approved: False

level: 1
 type: PREEMPT
 | (40216:74) Owl Barrier
 | Reduce damage from all sources by 50% for 3 turns
 group:
 | condition: 50% chance
 | (40218:92) Dark Owl
 | Spawn 4 random Dark orbs, Deal 100% damage
 | condition: 50% chance
 | (40217:15) Owl Feathers
 | Deal 105% damage (3 hits, 35% each)