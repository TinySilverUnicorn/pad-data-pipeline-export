#609760 - ガンダムエピオン
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 609760
approved: False

level: 1
 type: PREEMPT
 | (46705:83) 私が全てを正してみせる！ + ゼロシステム
 | Reduce damage from all sources by 50% for 3 turns + Absorb Water and Light damage for 3 turns
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 2
   | (46708:83) その程度で私の相手が + 務まると思うな………！
   | ATK -50% for 1 turn + Change all orbs to Fire, Water, Wood, Light, Dark, and Heal, Deal 90% damage
 | group:
 | condition: execute repeatedly, turn 2 of 2
   | (46711:83) 完全平和のためには + 必要な犠牲なのだッ！
   | Lock all orbs + Deal 105% damage (5 hits, 21% each), Deal 105% damage (5 hits, 21% each)