#201789 - incarnation of kirin, sakuya
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 201789
approved: False

level: 1
 type: PASSIVE
 | (40259:72) ＿Sakuya Light Halved
 | Reduce damage from Light attrs by 50%
 type: PREEMPT
 | (40252:127) stay calm...
 | No skyfall for 10 turns
 group:
 | condition: 50% chance
 | (40250:143) minimizing roar
 | For 1 turn, 50% ATK for 1 random sub, Deal 90% damage
 | condition: 50% chance
 | (40251:151) minimizing orbs
 | No description set, Deal 100% damage
 type: UNKNOWN_USE
 | (40260:68) dance of the kirin
 | Fire, Water, Wood, and Light skyfall +100% for 1 turn