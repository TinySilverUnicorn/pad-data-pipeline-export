#404830 - Masquerade Toy Dragon Caller, Cotton
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 404830
approved: False

level: 1
 type: PASSIVE
 | (41442:73) ＿Cotton Resolve
 | Survive attacks with 1 HP when HP > 50%
 | (41443:72) ＿Cotton Dark Halved
 | Reduce damage from Dark attrs by 50%
 type: PREEMPT
 | (41444:83) BOO!! + The ghosts have arrived!!
 | Player -50% HP + Deal 60% damage (3 hits, 20% each), Deal 60% damage (3 hits, 20% each)
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 3
   | (41448:48) Candy Gourd Stance!
   | Change a random attribute to Heal orbs, Deal 110% damage
 | group:
 | condition: execute repeatedly, turn 2 of 3
   | (41449:130) Toy Dragon Chainsaw!
   | ATK -75% for 1 turn, Deal 100% damage
 | group:
 | condition: execute repeatedly, turn 3 of 3
   | (41450:68) What trick should we play next?
   | Heal skyfall +20% for 1 turn, Deal 95% damage