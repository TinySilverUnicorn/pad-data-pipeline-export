#200176 - Alt. Metal Dragon
monster size: 1
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 200176
approved: False

level: 1
 type: PASSIVE
 | (34043:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (37022:53) Metal Dark
 | Absorb Dark damage for 7 turns
 group:
 | group:
 | condition: turn 2
   | (37024:92) Purple Bite
   | Spawn 5 random Dark orbs, Deal 100% damage
 | group:
 | condition: turn 4
   | (37024:92) Purple Bite
   | Spawn 5 random Dark orbs, Deal 100% damage
 | group:
   | (37042:15) Hellfire Blaze
   | Deal 5,000% damage (5 hits, 1,000% each)
 type: REMAINING
 condition: when 2 enemies remain, hp <= 100
 | group:
 | condition: turn 1
   | (37044:19) Hellfire Release
   | Increase damage to 1,000% for the next 999 turns
 | group:
 | condition: turns 2-6
   | (37024:92) Purple Bite
   | Spawn 5 random Dark orbs, Deal 100% damage