#404198 - Starry Cloud Dragon Caller, Erika
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 404198
approved: False

level: 1
 type: PASSIVE
 | (37039:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 | (37020:72) Dark reduced
 | Reduce damage from Dark attrs by 50%
 type: PREEMPT
 | (37160:83) Eternal Dragon Shield + Silent Moon Talisman + Starry Fox's Moon Clouds
 | Voids status ailments for 999 turns + Bind awoken skills for 1 turn + A 3×1 rectangle of clouds appears for 1 turn at a random location
 group:
 | group:
   | group:
   | condition: turns 1-6 while HP > 49
     | condition: 50% chance
     | (37169:83) Starry Fox's Moon Clouds + Dimension Formation - Five Clouds
     | A 2×2 square of clouds appears for 1 turn at a random location + Change all orbs to Wood, Light, Dark, Heal, and Jammer, Deal 100% damage
     | condition: 50% chance
     | (37172:83) Starry Fox's Moon Clouds + Dimension Formation - Five Clouds
     | A 2×2 square of clouds appears for 1 turn at a random location + Change all orbs to Wood, Light, Dark, Heal, and Poison, Deal 100% damage
   | group:
     | (37040:83) Hellfire Wave + Hellfire Blaze
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turn 1 while HP > 2, hp <= 49
   | (37165:83) Silent Moon Seal + Star Shield + Starry Fox's Moon Clouds
   | RCV 50% for 3 turns + Void damage >= 500,000,000 for 6 turns + A 3×2 rectangle of clouds appears for 1 turn at a random location
 | group:
 | condition: turns 1-6, hp <= 2
   | (37164:104) Dimension Formation - Moon Fox Clouds
   | A 3×3 square of clouds appears for 1 turn at a random location, Deal 1,000% damage