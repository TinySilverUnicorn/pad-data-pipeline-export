#304937 - Shadow Moon
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 304937
approved: False

level: 1
 type: PASSIVE
 | (43362:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (43502:68) This is war.
 | Poison skyfall +15% for 2 turns
 group:
 | group:
   | condition: 25% chance
   | (43358:92) Shadow Beam
   | Spawn 5 random Wood orbs, Deal 100% damage
   | condition: 25% chance
   | (43359:108) Satan Sabre
   | Change all Light orbs to Dark orbs, Deal 100% damage
   | condition: 25% chance
   | (43360:15) Shadow Punch
   | Deal 100% damage
   | condition: 25% chance
   | (43361:92) Harsh Yellow Flare
   | Spawn 5 random Light orbs, Deal 101% damage
 | group:
 | condition: hp <= 1
   | (43355:83) Stone of the Moon, King Stone + Destruction Ray
   | Enemy recover 45% HP + Change all Wood and Light orbs to Dark orbs, Deal 100% damage