#208295 - Reincarnated Dagda
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 208295
approved: False

level: 1
 type: PASSIVE
 | (44013:129) Super Resolve
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (44026:83) Recovery Field + Recovery Tornado
 | No skyfall for 2 turns + Random 2 orbs change every 1.0s for 1 turn
 group:
 | (44012:109) Recovery Tornado
 | Random 1 orbs change every 1.0s for 1 turn, Deal 100% damage