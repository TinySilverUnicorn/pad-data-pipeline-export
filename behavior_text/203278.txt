#203278 - Alt. Blue-Winged Ray Dragon Caller, Navi
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 203278
approved: False

level: 1
 type: PASSIVE
 | (41531:118) ＿Navi Dragon Halved
 | Reduce damage from Dragon types by 50%
 type: PREEMPT
 | (41532:83) Stealthy Element + Ray Dragon Crest
 | No description set + Void damage >= 3,000,000,000 for 3 turns
 group:
 | group:
 | condition: turns 1-2
   | condition: 20% chance
   | (41535:83) Harmonic Feather + Sequana
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Change all orbs to Fire, Water, Wood, Light, Dark, and Heal, Deal 85% damage
   | condition: 20% chance
   | (41538:83) Harmonic Feather + Varisque
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Lock 10 random orbs, Deal 100% damage
   | condition: 20% chance
   | (41540:83) Harmonic Feather + Équateur
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Random 3 orbs change every 1.0s for 1 turn, Deal 95% damage
   | condition: 20% chance
   | (41542:83) Harmonic Feather + Nufanga
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Deal 105% damage (5 hits, 21% each), Deal 105% damage (5 hits, 21% each)
   | condition: 20% chance
   | (41544:83) Harmonic Feather + Dahaka
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + No description set, Deal 97% damage
 | group:
 | condition: turn 3
   | (41546:83) I must keep my guard up! + Dragonfire Order
   | Voids status ailments for 15 turns + Absorb damage when damage >= 1,000,000,000 for 3 turns, Deal 110% damage
 | group:
   | condition: 20% chance
   | (41535:83) Harmonic Feather + Sequana
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Change all orbs to Fire, Water, Wood, Light, Dark, and Heal, Deal 85% damage
   | condition: 20% chance
   | (41538:83) Harmonic Feather + Varisque
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Lock 10 random orbs, Deal 100% damage
   | condition: 20% chance
   | (41540:83) Harmonic Feather + Équateur
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Random 3 orbs change every 1.0s for 1 turn, Deal 95% damage
   | condition: 20% chance
   | (41542:83) Harmonic Feather + Nufanga
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Deal 105% damage (5 hits, 21% each), Deal 105% damage (5 hits, 21% each)
   | condition: 20% chance
   | (41544:83) Harmonic Feather + Dahaka
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + No description set, Deal 97% damage