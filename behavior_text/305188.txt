#305188 - Gore Magala
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 305188
approved: False

level: 1
 type: PASSIVE
 | (42405:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (42395:83) Roar + Frenzy Burst
 | Voids status ailments for 999 turns + Change all orbs to Dark, Heal, Jammer, and Poison
 group:
 | group:
   | condition: 25% chance
   | (42401:15) Continuous Aerial Lunge
   | Deal 102% damage (3 hits, 34% each)
   | condition: 25% chance
   | (42402:102) Close Range Frenzy Burst
   | Spawn 3 random locked Bomb orbs, Deal 100% damage
   | condition: 25% chance
   | (42403:71) Aerial Lunge
   | Void damage >= 300,000,000 for 1 turn, Deal 100% damage
   | condition: 25% chance
   | (42404:15) Tackle Slam
   | Deal 100% damage
 | group:
 | condition: hp <= 1
   | (42398:83) Frenzy + Frenzied Breath
   | Enemy recover 40% HP + Unable to match Dark and Heal orbs for 1 turn, Deal 101% damage