#608935 - Pang Nuan
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 608935
approved: False

level: 1
 type: PASSIVE
 | (39434:129) ＿Fixed Team Super Resolve
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (39101:83) I shall only judge... + ...whether thou art genuine.
 | Fire skyfall +15% for 4 turns + Haste both leaders' skills by 99 turns
 group:
 | group:
 | condition: turn 1
   | (39104:50) What is this?
   | Player -50% HP
 | group:
 | condition: turn 2
   | (39105:83) Naught of thee... + ...resounds in me.
   | Fix orb movement starting point to random position on the board + Reduce damage from all sources by 50% for 2 turns, Deal 100% damage
 | group:
 | condition: execute repeatedly, turn 1 of 2
   | (39108:67) Do not get cocky.
   | Absorb damage when combos <= 4 for 1 turn, Deal 85% damage
 | group:
 | condition: execute repeatedly, turn 2 of 2
   | (39109:130) Thou art a weakling.
   | ATK -50% for 1 turn, Deal 90% damage