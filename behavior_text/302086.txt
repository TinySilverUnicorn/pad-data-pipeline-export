#302086 - Hello Kitty & Tyrannos
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 302086
approved: False

level: 1
 type: PREEMPT
 | (41269:53) Apple Power!
 | Absorb Fire damage for 3 turns
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 3
   | (41270:86) The Energy of Spirit
   | Enemy recover 20% HP, Deal 100% damage
 | group:
 | condition: execute repeatedly, turn 2 of 3
   | (41271:92) Magma Breath
   | Spawn 8 random Fire orbs, Deal 105% damage
 | group:
 | condition: execute repeatedly, turn 3 of 3
   | (41272:15) Combination Attack
   | Deal 108% damage (4 hits, 27% each)