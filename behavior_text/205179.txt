#205179 - Howling Stormdragon, Rokks
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 205179
approved: False

level: 1
 type: PREEMPT
 | (43310:78) Howling Stormdragon's Roar
 | Change the 1st, 2nd, and 3rd rows to Fire orbs and the 4th and 5th rows to Jammer orbs
 group:
 | (43311:92) Howling Stormdragon's Dance
 | Spawn 2 random Fire and Heal orbs, Deal 100% damage