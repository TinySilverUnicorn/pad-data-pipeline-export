#400831 - Top Droidragon
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 400831
approved: False

level: 1
 type: PASSIVE
 | (41619:129) Super Resolve 50％
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (41612:83) Droid Shield + Droid Stage
 | Voids status ailments for 999 turns + Locked random skyfall +25% for 10 turns
 group:
 | group:
   | (41618:92) Droid Attack
   | Spawn 5 random Water and Wood orbs, Deal 100% damage
 | group:
 | condition: turn 1, hp <= 49
   | (41615:83) Two Att. Once + Lock on!
   | Spawn 15 random Water and Wood orbs + Fix orb movement starting point to random position on the board, Deal 100% damage