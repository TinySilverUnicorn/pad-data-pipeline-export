#609335 - Super Aurora Dragon
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 609335
approved: False

level: 1
 type: PASSIVE
 | (34042:129) Super Resolve
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (40361:137) Aurora Guard
 | Void damage >= 2,000,000,000 for 6 turns
 group:
 | group:
 | condition: turn 1
   | (40364:83) Dragon Charge + Aurora Scales
   | Increase damage to 250% for the next 5 turns + Reduce damage from all sources by 50% for 5 turns
 | group:
 | condition: turns 2-5
   | (40367:46) Elemental Aurora
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
   | (34046:15) Cosmic Destruction
   | Deal 5,000% damage (5 hits, 1,000% each)
 type: UNKNOWN_USE
 | (40362:6) Dragon Clear
 | Voids player buff effects
 | (40363:151) Crystal Aurora
 | No description set, Deal 100% damage