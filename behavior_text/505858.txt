#505858 - Alt. Mega Awoken Yog-Sothoth, the Everlasting
monster size: 5
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 505858
approved: False

level: 1
 type: PREEMPT
 | (44453:83) Concept of Boundless Worlds + He Who Shall Not Be Named + The Ultimate Origin of All Things
 | Voids status ailments for 999 turns + Bind awoken skills for 3 turns + Deal 72% damage (3 hits, 24% each), Deal 72% damage (3 hits, 24% each)
 group:
 | (44439:83) The One Who Trascends All Existence + The Ultimate Origin of All Things
 | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Deal 102% damage (3 hits, 34% each), Deal 102% damage (3 hits, 34% each)