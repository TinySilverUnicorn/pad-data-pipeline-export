#303932 - Drifting Dragon Caller, Enra
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 303932
approved: False

level: 1
 type: PASSIVE
 | (37039:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 | (42522:118) ＿Machine Type halved
 | Reduce damage from Machine types by 50%
 type: PREEMPT
 | (37175:83) Flame Jaws' Draco Summoning Shield + Crimson Rain + Flame Jaws
 | Voids status ailments for 999 turns + Fire and Wood skyfall +20% for 3 turns + Lock 15 random orbs, Deal 100% damage
 group:
 | group:
   | group:
   | condition: turns 1-6 while HP > 49
     | condition: 33% chance
     | (37187:83) Centipedification + Flame Jaws
     | Change own attribute to random one of Fire, Water, Wood, Light, or Dark + Lock 10 random orbs, Deal 100% damage
     | condition: 33% chance
     | (37190:83) Crimson Exoskeleton + Flame Jaws
     | Absorb Fire damage for 1 turn + Lock all Fire orbs, Deal 100% damage
     | condition: 34% chance
     | (37184:83) Blazing Legs + Flame Jaws
     | Spawn 4 random Fire and Wood orbs + Lock 10 random orbs, Deal 100% damage
   | group:
     | (37040:83) Hellfire Wave + Hellfire Blaze
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turn 1 while HP > 2, hp <= 49
   | (37180:83) Crimson Shadow Rain + Centipede Fire Expansion + Flame Jaws
   | For 3 turns, 30% chance for skyfall orbs to be blinded for turn + For 3 turns, 50% ATK for both leaders + Lock all orbs, Deal 100% damage
 | group:
 | condition: turns 1-6, hp <= 2
   | (37179:94) Huge Jaws
   | Lock all orbs, Deal 1,000% damage