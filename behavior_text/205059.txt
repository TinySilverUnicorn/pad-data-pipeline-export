#205059 - Seven Seas Protector Goddess, Tethys
monster size: 5
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 205059
approved: False

level: 1
 type: PREEMPT
 | (18966:83) I'm delighted that we could meet... + I hope you'll keep giving it your all.
 | Do nothing + Reduce self HP to 0