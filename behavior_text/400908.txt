#400908 - True Earth Baron
monster size: 3
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 400908
approved: False

level: 1
 group:
 condition: execute repeatedly, turn 1 of 2, hp <= 100
 | (45512:46) Changing Break
 | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage