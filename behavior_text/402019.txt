#402019 - 剛腕番長・ギガンテス
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 402019
approved: False

level: 5
 type: PREEMPT
 | (2029:20) 喧嘩上等！
 | Voids status ailments for 5 turns
 group:
 | group:
   | (2030:15) 愚ゥゥゥ例徒ォッ！！
   | Deal 100% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: turn 1
     | (2031:83) やっちゃってくだせぇアニキ！！ + うぉぉっしゃぁあああ！！！
     | Do nothing + Do nothing
   | group:
     | (2033:15) 天上天下唯我独尊！！！！
     | Deal 1,000% damage
 | group:
 | condition: hp <= 1
   | (2032:86) 根性じゃあああ！
   | Enemy recover 49% HP