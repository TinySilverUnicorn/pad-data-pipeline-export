#701543 - イエローシードラ
monster size: 2
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 701543
approved: False

level: 1
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 3
   | (47346:92) こうですか？
   | Spawn 3 random Light orbs
 | group:
 | condition: execute repeatedly, turn 2 of 3
   | (47348:92) こ、こうですか？
   | Spawn 3 random Light and Heal orbs
 | group:
 | condition: execute repeatedly, turn 3 of 3
   | condition: when required attributes on board
   | (47350:92) バッチリですか？
   | Spawn 3 random Water, Light, Dark, and Heal orbs