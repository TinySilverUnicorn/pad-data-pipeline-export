#9947 - テフェリー
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 9947
approved: False

level: 1
 type: PASSIVE
 | (48068:72) 水半減
 | Reduce damage from Water attrs by 50%
 type: PREEMPT
 | (48061:83) 定命の魔道士 + 時間の速度を半減させる
 | Voids status ailments for 999 turns + Movetime 50% for 3 turns
 group:
 | condition: 25% chance
 | (48064:104) 浮遊
 | A 2×2 square of clouds appears for 1 turn at a random location, Deal 100% damage
 | condition: 25% chance
 | (48065:15) 素晴らしい
 | Deal 102% damage (3 hits, 34% each)
 | condition: 25% chance
 | (48066:143) 完璧だ
 | For 1 turn, 50% ATK for 2 random subs, Deal 101% damage
 | condition: 25% chance
 | (48067:92) 休む時間はあるかい？
 | Spawn 4 random Heal orbs, Deal 100% damage