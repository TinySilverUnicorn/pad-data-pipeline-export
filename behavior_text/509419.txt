#509419 - Poised Guitarist, Sonia Clea
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 509419
approved: False

level: 1
 type: PREEMPT
 | (41136:83) Let's play some music! + Crescendo little by little...
 | Absorb Dark damage for 999 turns + Change all orbs to Fire, Water, Wood, Light, Dark, and Heal
 type: DEATH
 condition: when defeated
 | (41074:95) Hehe...!
 | Reduce self HP to 0
 group:
 | condition: when 1 combos last turn
 | (41148:83) Time to turn up the volume. + There's no stopping this thrill!
 | Change the 4th row to Light orbs and the 5th row to Dark orbs + Change the 1st, 2nd, and 3rd rows to Water and Heal orbs
 | (41139:83) You can keep going, right? + Crescendo little by little...
 | Unable to match Dark orbs for 1 turn + Change all orbs to Fire, Water, Wood, Light, Dark, and Heal
 type: UNKNOWN_USE
 | (41142:83) You can keep going, right? + There's no stopping this thrill!
 | Unable to match Dark orbs for 1 turn + Change all orbs to Fire, Water, Wood, Light, and Dark
 | (41151:83) Last round, here we go! + Let's get louder!
 | Change all orbs to Dark + Spawn 6 random Water, Light, and Heal orbs
 | (41145:83) You can keep going, right? + Last round, here we go!
 | Unable to match Dark orbs for 1 turn + Change all orbs to Fire, Water, Light, and Dark
 | (41154:40) Hehe...!
 | Reduce self HP to 0