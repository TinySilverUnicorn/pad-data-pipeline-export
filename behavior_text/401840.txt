#401840 - True Sea Dragon King, Volsung
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 401840
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (42938:83) Sea Dragon's Protection + Sea Dragon's Imperial Wrath + Ocean Waves
 | Voids status ailments for 999 turns + Void damage >= 1,000,000,000 for 7 turns + Change board size to 6x5 for 10 turns, Deal 100% damage
 group:
 | group:
   | group:
   | condition: turns 1-6 while HP > 49
     | condition: 50% chance
     | (42944:83) Water Storm + Aquatic Vortex
     | Haste all cards' skills by 1~2 turns + Random 2 orbs change every 1.0s for 1 turn, Deal 100% damage
     | condition: 50% chance
     | (42947:83) Water Storm + Waving Vortex
     | Delay active skills by 0~2 turns + Lock all orbs, Deal 100% damage
   | group:
     | (42622:83) Longevity Surge + Universal Demise
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turn 1 while HP > 2, hp <= 49
   | (42943:139) Deep Sea's Pressure
   | Haste both leaders' skills by 10 turns, Deal 125% damage
 | group:
 | condition: turns 1-6, hp <= 2
   | (42942:15) Streaming Vortex
   | Deal 1,000% damage (2 hits, 500% each)