#509631 - Illusionary Corrector, Elizer
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 509631
approved: False

level: 1
 type: PREEMPT
 | (43178:83) So many...colors. + I...make it beautiful.
 | Player recover 100% HP + No description set
 group:
 | group:
   | group:
   | condition: execute repeatedly, turn 1 of 4
     | (43182:104) Pure white...beautiful.
     | A 6×2 rectangle of clouds appears for 1 turn at 1st row, 1st column, Deal 90% damage
   | group:
   | condition: execute repeatedly, turn 2 of 4
     | (43183:107) No need for...other colors.
     | Unable to match Fire, Water, Wood, and Dark orbs for 1 turn, Deal 94% damage
   | group:
   | condition: execute repeatedly, turn 3 of 4
     | (43184:104) White snow, white clouds, white lilies.
     | A 6×4 rectangle of clouds appears for 1 turn at 1st row, 1st column, Deal 98% damage
   | group:
   | condition: execute repeatedly, turn 4 of 4
     | (43185:109) Let all become one...in the white.
     | Random 3 orbs change every 1.0s for 2 turns, Deal 80% damage
 | group:
 | condition: turn 1, hp <= 49
   | (43181:130) You stand in my way. Why?
   | ATK -50% for 3 turns