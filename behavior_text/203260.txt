#203260 - Yamato Flame Dragon Caller, Tsubaki
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 203260
approved: False

level: 1
 type: PREEMPT
 | (43993:94) Emperor God's Talisman
 | Lock all orbs
 group:
 | (43994:15) Yamato Barrage
 | Deal 102% damage (3 hits, 34% each)