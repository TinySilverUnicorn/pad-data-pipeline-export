#308467 - Alt. Reincarnated Spica
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 308467
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (44648:83) Spirit of the Virgo Goddess + Look... + What a breathtaking view...
 | Voids status ailments for 999 turns + Water and Wood skyfall +20% for 3 turns + For 6 turns, 1% ATK for both leaders
 group:
 | group:
   | group:
   | condition: turns 1-5 while HP > 49
     | condition: 50% chance
     | (44654:83) Mechanical Star God's Tactics + Cosmic Charge - Land
     | No skyfall for 1 turn + Change all Heal, Jammer, and Poison orbs to Wood orbs, Deal 100% damage
     | condition: 50% chance
     | (44657:83) Mechanical Star God's Tactics + Cosmic Charge - Earth
     | No description set + Change a random attribute to Wood orbs, Deal 100% damage
   | group:
     | (44524:83) Star Machine's Wave + Apocalyptic Star Crushing
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turn 1 while HP > 2, hp <= 49
   | (44653:53) Wooden Shell of the Virgo Goddess
   | Absorb Wood damage for 4 turns
 | group:
 | condition: turns 1-5, hp <= 2
   | (44652:15) Cosmic Charge - Earthland
   | Deal 1,050% damage (3 hits, 350% each)