#200207 - True Mercury Light Carbuncle
monster size: 4
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 200207
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (45520:107) Mercury Cure Light
 | Unable to match Water orbs for 3 turns, Deal 100% damage
 group:
 | group:
 | condition: turn 2
   | (45524:46) Jewelry Attack
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
 | condition: turn 4
   | (45524:46) Jewelry Attack
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
   | (45534:15) Doom of a Trillion Catastrophes
   | Deal 5,000% damage (5 hits, 1,000% each)
 type: REMAINING
 condition: when 2 enemies remain, hp <= 100
 | group:
 | condition: turn 1
   | (45536:19) Release of a Trillion Evil Omens
   | Increase damage to 1,000% for the next 999 turns
 | group:
 | condition: turns 2-6
   | (45524:46) Jewelry Attack
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage