#308976 - Han Ming
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0
monster_id: 308976
approved: False

level: 1
 type: PASSIVE
 | (39138:73) ＿Han Ming Resolve
 | Survive attacks with 1 HP when HP > 50%

level: 4
 type: PASSIVE
 | (39138:73) ＿Han Ming Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (39139:83) Ba-ba-ba-ba Ba Ba Bum + Who's the strongest!? + Han Ming!!
 | Do nothing + Delay active skills by 2 turns + Change all orbs to Wood, Light, and Heal
 group:
 | group:
   | group:
   | condition: turn 1
     | (39143:83) He shatters great boulders at breath! + He flattens mountains into the earth with a single swing!
     | ATK -50% for 3 turns + Change the 4th and 5th rows to Wood and Light orbs, Deal 80% damage
   | group:
   | condition: turn 2
     | (39146:83) The world's strongest. + A man among men!
     | Absorb damage when combos <= 8 for 1 turn + Lock all orbs, Deal 90% damage
   | group:
   | condition: turn 3
     | (39149:83) Who is he!? + Han Ming!!
     | Fix orb movement starting point to random position on the board + Increase damage to 200% for the next 999 turns
   | group:
     | (39171:48) Han Ming, Giant of Chu!
     | Change a random attribute to Wood orbs, Deal 55% damage
 | group:
 | condition: hp <= 1
   | group:
   | condition: execute repeatedly, turn 1 of 2
     | (39152:88) You'll pay. You'll pay for this!
     | Bind awoken skills for 1 turn
   | group:
   | condition: execute repeatedly, turn 2 of 2
     | (39153:15) It's over.
     | Deal 150% damage (5 hits, 30% each)