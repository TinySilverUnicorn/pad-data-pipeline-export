#309676 - Hyperanemon
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 309676
approved: False

level: 1
 type: PREEMPT
 | (45014:107) Spooky Aura
 | Unable to match Light orbs for 3 turns, Deal 100% damage
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 2
   | (45015:15) Normal Attack
   | Deal 90% damage (2 hits, 45% each)
 | group:
 | condition: execute repeatedly, turn 2 of 2
   | (45017:109) Gust Slash
   | Random 3 orbs change every 1.0s for 1 turn