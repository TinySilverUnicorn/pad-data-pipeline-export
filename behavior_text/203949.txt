#203949 - Daunting Wraith Dragonbound, Li
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 203949
approved: False

level: 1
 type: PASSIVE
 | (34043:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 | (37020:72) Dark reduced
 | Reduce damage from Dark attrs by 50%
 type: PREEMPT
 | condition: 25% chance
 | (37249:83) Dragon Fear Barrier + Dragon Shadow Shift + Energy Shift
 | Voids status ailments for 999 turns + Leader changes to random sub for 1 turn + Absorb Fire and Water damage for 1 turn
 | condition: 25% chance
 | (37272:83) Dragon Fear Barrier + Dragon Shadow Shift + Energy Shift
 | Voids status ailments for 999 turns + Leader changes to random sub for 1 turn + Absorb Fire and Wood damage for 1 turn
 | condition: 25% chance
 | (37276:83) Dragon Fear Barrier + Dragon Shadow Shift + Energy Shift
 | Voids status ailments for 999 turns + Leader changes to random sub for 1 turn + Absorb Water and Wood damage for 1 turn
 | condition: 25% chance
 | (37249:83) Dragon Fear Barrier + Dragon Shadow Shift + Energy Shift
 | Voids status ailments for 999 turns + Leader changes to random sub for 1 turn + Absorb Fire and Water damage for 1 turn
 group:
 | group:
   | group:
   | condition: turns 1-6 while HP > 49
     | condition: 20% chance
     | (37257:83) Energy Shift + Shadow Energy Claw
     | Absorb Fire damage for 1 turn + Change own attribute to Wood, Deal 100% damage
     | condition: 20% chance
     | (37260:83) Energy Shift + Shadow Energy Claw
     | Absorb Water damage for 1 turn + Change own attribute to Fire, Deal 100% damage
     | condition: 20% chance
     | (37263:83) Energy Shift + Shadow Energy Claw
     | Absorb Wood damage for 1 turn + Change own attribute to Water, Deal 100% damage
     | condition: 20% chance
     | (37266:83) Energy Shift + Shadow Energy Claw
     | Absorb Light damage for 1 turn + Change own attribute to Dark, Deal 100% damage
     | condition: 20% chance
     | (37269:83) Energy Shift + Shadow Energy Claw
     | Absorb Dark damage for 1 turn + Change own attribute to Light, Deal 100% damage
   | group:
     | (37040:83) Hellfire Wave + Hellfire Blaze
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turn 1 while HP > 2, hp <= 49
   | (37253:83) Energy Shift + Dragon Fear Protection + Dragon Shadow Fear
   | Absorb Light and Dark damage for 1 turn + Reduce damage from all sources by 50% for 999 turns + Movetime 50% for 3 turns, Deal 150% damage
 | group:
 | condition: turns 1-6, hp <= 2
   | (37280:15) Shadow Energy Burst
   | Deal 600% damage (3 hits, 200% each)