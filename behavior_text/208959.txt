#208959 - Yang Duanhe
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 208959
approved: False

level: 1
 type: PREEMPT
 | (39368:83) All troops: slaughter them! + Mountain Folk's Surprise Attack
 | No skyfall for 5 turns + Blind all orbs on the board, Deal 100% damage
 group:
 | group:
   | group:
   | condition: execute repeatedly, turn 1 of 2
     | (39371:130) Enough of your chattering.
     | ATK -75% for 1 turn, Deal 95% damage
   | group:
   | condition: execute repeatedly, turn 2 of 2
     | (39372:15) Kill them all!
     | Deal 105% damage (3 hits, 35% each)
 | group:
 | condition: turn 1, hp <= 49
   | (39373:83) Be silent, and let your names go down in history... + ...as the losers you are.
   | Bind awoken skills for 1 turn + Spawn 8 random Wood and Jammer orbs