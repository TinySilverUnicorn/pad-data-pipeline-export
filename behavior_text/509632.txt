#509632 - White-Veiled Illusionary Corrector, Elizer
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 509632
approved: False

level: 1
 type: PASSIVE
 | (43188:129) ＿Elizer Super Resolve
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (43189:83) Color Resetting + Don't need those...! + Don't want those!!
 | Player recover 100% HP + Absorb Fire, Water, and Wood damage for 3 turns + Reduce damage from all sources by 90% for 5 turns
 group:
 | group:
   | group:
   | condition: execute repeatedly, turn 1 of 4
     | (43195:151) Spotless Removal
     | No description set, Deal 95% damage
   | group:
   | condition: execute repeatedly, turn 2 of 4
     | (43196:77) White Veil
     | Change the 1st and 6th columns to Heal orbs, Deal 100% damage
   | group:
   | condition: execute repeatedly, turn 3 of 4
     | (43197:126) Milky Dye
     | Change board size to 7x6 for 2 turns, Deal 99% damage
   | group:
   | condition: execute repeatedly, turn 4 of 4
     | (43198:94) Clouds of Snow Lilies
     | Lock all orbs, Deal 105% damage
 | group:
 | condition: turn 1 while HP > 9, hp <= 49
   | (43192:83) White. + I want WHITE...!
   | No skyfall for 5 turns + A 6×4 rectangle of clouds appears for 1 turn at 1st row, 1st column, Deal 90% damage
 | group:
 | condition: hp <= 9
   | (43199:15) End of Vividness
   | Deal 132% damage (6 hits, 22% each)