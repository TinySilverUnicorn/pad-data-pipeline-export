#305945 - 裏転生ツクヨミ＝ドラゴン
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 305945
approved: False

level: 1
 type: PASSIVE
 | (45384:72) 光半減
 | Reduce damage from Light attrs by 50%
 type: PREEMPT
 | (45346:83) 夜刻神の風格 + 月光の導き + 暗黒の波動
 | Voids status ailments for 999 turns + Heal skyfall +20% for 20 turns + Player -150% HP
 group:
 | condition: 20% chance
 | (45350:83) 桂月の輝き + 暗黒の波動 + 月光の息吹
 | Absorb Fire damage for 1 turn + Player -99% HP + Deal 36% damage (3 hits, 12% each), Deal 36% damage (3 hits, 12% each)
 | condition: 20% chance
 | (45354:83) 桂月の輝き + 暗黒の波動 + 月光の息吹
 | Absorb Water damage for 1 turn + Player -99% HP + Deal 36% damage (3 hits, 12% each), Deal 36% damage (3 hits, 12% each)
 | condition: 20% chance
 | (45358:83) 桂月の輝き + 暗黒の波動 + 月光の息吹
 | Absorb Wood damage for 1 turn + Player -99% HP + Deal 36% damage (3 hits, 12% each), Deal 36% damage (3 hits, 12% each)
 | condition: 20% chance
 | (45362:83) 桂月の輝き + 暗黒の波動 + 月光の息吹
 | Absorb Light damage for 1 turn + Player -99% HP + Deal 36% damage (3 hits, 12% each), Deal 36% damage (3 hits, 12% each)
 | condition: 20% chance
 | (45366:83) 桂月の輝き + 暗黒の波動 + 月光の息吹
 | Absorb Dark damage for 1 turn + Player -99% HP + Deal 36% damage (3 hits, 12% each), Deal 36% damage (3 hits, 12% each)