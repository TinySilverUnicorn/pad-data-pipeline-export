#402014 - 生徒会長・ルシファー
monster size: 5
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 402014
approved: False

level: 5
 type: PREEMPT
 | (2025:20) 一年坊主か・・・
 | Voids status ailments for 4 turns
 group:
 | group:
 | condition: turn 1
   | (2026:65) この学園は甘くないぞ
   | Bind 1 random sub for 3 turns
 | group:
 | condition: turn 2
   | (2047:79) 一閃
   | Change the 3rd row to Light orbs, Deal 50% damage
 | group:
 | condition: turn 3
   | (2048:79) 二閃
   | Change the 2nd and 4th rows to Light orbs, Deal 100% damage
 | group:
 | condition: turn 4
   | (2049:79) 三閃
   | Change the 1st, 3rd, and 5th rows to Light orbs, Deal 150% damage
 | group:
   | (2028:15) 明けの明星
   | Deal 500% damage