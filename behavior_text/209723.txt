#209723 - νガンダム
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 209723
approved: False

level: 1
 type: PASSIVE
 | (46509:129) ＿ν超根性
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (46510:83) 邪気が来たか！ + フィン・ファンネル
 | Voids status ailments for 999 turns + Lock 15 random orbs, Deal 50% damage
 group:
 | group:
   | group:
   | condition: execute repeatedly, turn 1 of 2
     | (46517:83) ビーム・ライフル + ニュー・ハイパー・バズーカ
     | Change the 1st and 6th columns to Heal orbs + Delay active skills by 1 turn, Deal 50% damage
   | group:
   | condition: execute repeatedly, turn 2 of 2
     | (46520:83) フィン・ファンネルバリア + ビームサーベル
     | Void damage >= 500,000,000 for 1 turn + Change the 4th and 5th rows to Wood and Light orbs, Deal 100% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: turn 1
     | (46513:83) サイコフレームの共振 + νガンダムは伊達じゃない！
     | ATK -75% for 2 turns + Reduce damage from all sources by 75% for 2 turns, Deal 95% damage
   | group:
   | condition: turn 2
     | (46516:88) 離れろ！ガンダムの力はっ！
     | Bind awoken skills for 1 turn, Deal 80% damage