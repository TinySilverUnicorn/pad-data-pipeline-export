#606753 - Alt. Super Bubblie
monster size: 0
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 606753
approved: False

level: 1
 type: PREEMPT
 | (40375:152) Bubbling Up
 | No description set
 group:
 | group:
 | condition: turns 1-5
   | (40376:143) Lil' Flash
   | For 3 turns, 1% ATK for 1 random card
 | group:
 | condition: turn 6
   | (40377:6) Lil' Flash
   | Voids player buff effects
   | (34046:15) Cosmic Destruction
   | Deal 5,000% damage (5 hits, 1,000% each)
 | group:
   | (34046:15) Cosmic Destruction
   | Deal 5,000% damage (5 hits, 1,000% each)