#700093 - Mandrake
monster size: 3
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 700093
approved: False

level: 1
 type: PREEMPT
 | (39009:92) Please, help me out!
 | Spawn 10 random Jammer orbs
 group:
 | group:
 | condition: turns 1-9
   | (39010:92) I need some help!
   | Spawn 2 random Jammer orbs
   | (39013:40) Thank you so much!
   | Reduce self HP to 0
 | group:
 | condition: turn 10
   | (39014:92) I need some help!
   | Spawn 30 random Jammer orbs
 | group:
   | (39010:92) I need some help!
   | Spawn 2 random Jammer orbs
   | (39013:40) Thank you so much!
   | Reduce self HP to 0