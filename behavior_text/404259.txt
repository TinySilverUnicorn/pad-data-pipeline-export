#404259 - Thunderous Demon Princess, Raijin
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 404259
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 | (42616:72) Dark reduced
 | Reduce damage from Dark attrs by 50%
 type: PREEMPT
 | (42842:83) Protection of the Demon Princess + Yellow-Horned Dark Thunder + Purging Thunderdrums
 | Voids status ailments for 999 turns + Absorb Light and Dark damage for 3 turns + Void damage >= 1,000,000,000 for 3 turns, Deal 100% damage
 group:
 | group:
   | group:
   | condition: turns 1-2 while HP > 2
     | condition: 50% chance
     | (42848:83) Current of the Amber Princess + Thunder Drums
     | Seal the 1st row for 1 turn + Lock 15 random orbs, Deal 100% damage
     | condition: 50% chance
     | (42851:83) Current of the Amber Princess + Thunder Drums
     | Seal the 5th row for 1 turn + Lock 15 random orbs, Deal 100% damage
   | group:
   | condition: turn 3 while HP > 2
     | (42847:138) Thunderbolt Drums
     | Absorb damage when damage >= 1,000,000,000 for 4 turns, Deal 125% damage
   | group:
   | condition: turns 4-6 while HP > 2
     | condition: 50% chance
     | (42848:83) Current of the Amber Princess + Thunder Drums
     | Seal the 1st row for 1 turn + Lock 15 random orbs, Deal 100% damage
     | condition: 50% chance
     | (42851:83) Current of the Amber Princess + Thunder Drums
     | Seal the 5th row for 1 turn + Lock 15 random orbs, Deal 100% damage
   | group:
     | (42622:83) Longevity Surge + Universal Demise
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turns 1-6, hp <= 2
   | (42846:15) Thunderous Destruction
   | Deal 1,000% damage (10 hits, 100% each)