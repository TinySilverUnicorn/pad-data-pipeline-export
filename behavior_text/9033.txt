#9033 - Dada
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 9033
approved: False

level: 1
 type: PASSIVE
 | (38535:72) Dark Halved
 | Reduce damage from Dark attrs by 50%
 type: PREEMPT
 | (38531:101) Teleportation
 | Fix orb movement starting point to random position on the board
 group:
 | condition: 33% chance
 | (38532:15) Kick Up
 | Deal 102% damage (3 hits, 34% each)
 | condition: 50% chance
 | (38533:130) Micronize
 | ATK -50% for 1 turn, Deal 100% damage
 | (38534:67) Possession
 | Absorb damage when combos <= 6 for 1 turn, Deal 101% damage