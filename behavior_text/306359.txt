#306359 - Reincarnated Oichi
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 306359
approved: False

level: 1
 type: PREEMPT
 | (39983:83) Spirit of the Virtuous Goddess + Quince Crest + Virtuous Prayer
 | Voids status ailments for 999 turns + Bind awoken skills for 3 turns + Absorb damage when combos <= 10 for 3 turns
 group:
 | group:
   | group:
   | condition: turns 1-2 while HP > 2
     | condition: 50% chance
     | (39995:83) Virtuous Compassion + Black Skull
     | Haste all cards' skills by 1 turn + Blind random 6~8 orbs for 1 turn, Deal 100% damage
     | condition: 50% chance
     | (39998:83) Eerie Skull + Adamant Skull
     | Delay both leaders' skills by 1~2 turns + Lock 15 random orbs, Deal 100% damage
   | group:
   | condition: turn 3 while HP > 2
     | (39988:83) Virtuous Prayer + Virtuous Anguish + Eerie Cranium
     | Absorb damage when combos <= 9 for 3 turns + Void damage >= 500,000,000 for 3 turns + No description set
   | group:
   | condition: turns 4-5 while HP > 2
     | condition: 50% chance
     | (39995:83) Virtuous Compassion + Black Skull
     | Haste all cards' skills by 1 turn + Blind random 6~8 orbs for 1 turn, Deal 100% damage
     | condition: 50% chance
     | (39998:83) Eerie Skull + Adamant Skull
     | Delay both leaders' skills by 1~2 turns + Lock 15 random orbs, Deal 100% damage
   | group:
   | condition: turn 6 while HP > 2
     | (39992:83) Eerie Skull + Virtuous Wrath
     | Delay random sub's skills by 2~4 turns + Increase damage to 1,000% for the next 1 turn
   | group:
     | (39739:83) Azure Sky Surge + Thousandfold Demise
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turns 1-6, hp <= 2
   | (39987:15) Vile Skeleton Figure
   | Deal 1,000% damage (2 hits, 500% each)