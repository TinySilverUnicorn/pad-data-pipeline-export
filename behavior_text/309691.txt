#309691 - 超転生デスファリオン
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 309691
approved: False

level: 1
 type: PASSIVE
 | (31386:129) Super Resolve
 | Damage which would reduce HP from above 50% to below 50% is nullified
 | (44517:72) Light Reduced
 | Reduce damage from Light attrs by 50%
 type: PREEMPT
 | (44842:83) Remember the name Despharion! + Ali Protettrici + Pioggia di Oscurità
 | Voids status ailments for 999 turns + Absorb damage when damage >= 2,000,000,000 for 999 turns + For 15 turns, 15% chance for skyfall orbs to be blinded for turn, Deal 135% damage
 group:
 | group:
   | condition: 25% chance
   | (44853:83) Muro dell'Eternità + Chiave a Spina
   | For 1 turn, 1% ATK for both leaders + Lock 10 random orbs, Deal 80% damage
   | condition: 25% chance
   | (44856:83) Muro dell'Eternità + Cambiamento
   | For 1 turn, 50% ATK for 4 random subs + Specific orbs change every 1.0s for 1 turn, Deal 100% damage
   | condition: 25% chance
   | (44859:83) Muro dell'Eternità + Oscurità Avvolgente
   | Reduce damage from all sources by 50% for 1 turn + RCV 50% for 1 turn, Deal 80% damage
   | condition: 25% chance
   | (44862:83) Muro dell'Eternità + Pioggia d'Ali
   | ATK -75% for 1 turn + No skyfall for 1 turn
 | group:
 | condition: turn 1 while HP > 4, hp <= 49
   | (44846:83) Accelerazione + Avvelenamento + Arma Bianca
   | Haste both leaders' skills by 15 turns + Change all orbs to Heal, Jammer, and Poison + Player -100% HP, Deal 200% damage
 | group:
 | condition: hp <= 4
   | (44850:83) Nessuno Spazio + Innumerevoli Morti
   | Bind awoken skills for 1 turn + Deal 5,000% damage (10 hits, 500% each), Deal 5,000% damage (10 hits, 500% each)