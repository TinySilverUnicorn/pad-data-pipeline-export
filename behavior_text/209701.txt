#209701 - Beloved Martial Artist, Deena
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 209701
approved: False

level: 1
 type: PREEMPT
 | (24983:83) H-Hi... + B-But y'know!! It's 'cause... + ...Valentine's Day! + ...I tried my best, okay?! + Here goes! This is for you!
 | Do nothing + Do nothing + Heal skyfall +15% for 1 turn + Do nothing + Change the 4th and 5th rows to Heal orbs, Deal 10% damage
 group:
 | (24989:66) ...How embarrassing!
 | Do nothing