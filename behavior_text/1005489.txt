#1005489 - Great Witch of the Beach, Veroah
monster size: 5
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 1005489
approved: False

level: 1
 type: PREEMPT
 | (38588:67) Sand Castle
 | Absorb damage when combos <= 8 for 1 turn
 group:
 | (38588:67) Sand Castle
 | Absorb damage when combos <= 8 for 1 turn