#204911 - Masked Rider Black RX
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0
monster_id: 204911
approved: False

level: 1
 type: PASSIVE
 | (43383:129) Super Resolve 70%
 | Damage which would reduce HP from above 70% to below 70% is nullified
 type: PREEMPT
 | (43374:83) I am the child of the sun! + Macro Eye
 | Voids status ailments for 999 turns + RCV 50% for 3 turns
 type: DEATH
 condition: when defeated
 | (43387:95) As long as there is light in this world... + ...Kamen Rider Black RX is immortal!
 | Do nothing + Jammer skyfall +15% for 3 turns
 group:
 | group:
   | condition: 50% chance
   | (43381:15) Revolcrash
   | Deal 102% damage (3 hits, 34% each)
   | (43382:92) RX Kick
   | Spawn 5 random Jammer orbs, Deal 100% damage
 | group:
 | condition: hp <= 69
   | group:
   | condition: turn 1
     | (43377:83) Riding Arrow + RX Punch
     | Change all orbs to Wood and Light + Lock all Wood orbs, Deal 50% damage
   | group:
   | condition: turn 2
     | (43380:86) Sun Bask
     | Enemy recover 30% HP