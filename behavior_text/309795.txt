#309795 - ジオング
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 309795
approved: False

level: 1
 type: PASSIVE
 | (46566:129) ＿ジオング超根性
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (46567:83) ＿ジオング演出先制 + オールレンジ攻撃 + 私もニュータイプのはずだ！
 | No description set + Unable to match Heal orbs for 3 turns + Voids status ailments for 999 turns
 type: DEATH
 condition: when defeated
 | (46583:95) ＿ジオング演出死亡
 | No description set
 group:
 | group:
   | group:
   | condition: execute repeatedly, turn 1 of 3
     | (46577:83) 沈めい！ + 腰部メガ粒子砲
     | ATK -75% for 1 turn + Deal 100% damage (5 hits, 20% each), Deal 100% damage (5 hits, 20% each)
   | group:
   | condition: execute repeatedly, turn 2 of 3
     | (46580:151) 頭部メガ粒子砲
     | No description set, Deal 102% damage
   | group:
   | condition: execute repeatedly, turn 3 of 3
     | (46581:85) 腕部有線式5連装メガ粒子砲
     | Change all orbs to Fire, Water, Wood, Light, and Dark, Deal 110% damage
 | group:
 | condition: turn 1, hp <= 49
   | (46572:83) ＿ジオング50以下演出 + ええい！ + 奴め………バケモノか！！
   | No description set + Absorb damage when combos <= 13 for 3 turns + Change board size to 7x6 for 3 turns, Deal 105% damage