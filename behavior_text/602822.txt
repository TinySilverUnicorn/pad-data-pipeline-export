#602822 - 新聞部の新人・ウォーロン
monster size: 4
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 602822
approved: False

level: 1
 type: PREEMPT
 | (47343:83) 一瞬のかがやき… + スクープの命でやんす！
 | Fix orb movement starting point to random position on the board + Specific orbs change every 0.5s for 1 turn
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 3
   | (47347:101) 魂をこめるでやんす！
   | Fix orb movement starting point to random position on the board, Deal 100% damage
 | group:
 | condition: execute repeatedly, turn 2 of 3
   | (47349:101) 角度が命でやんす！
   | Fix orb movement starting point to random position on the board, Deal 100% damage
 | group:
 | condition: execute repeatedly, turn 3 of 3
   | (47351:101) まだまだ練習が必要でやんす！
   | Fix orb movement starting point to random position on the board, Deal 100% damage