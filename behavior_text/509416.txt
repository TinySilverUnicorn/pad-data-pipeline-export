#509416 - 響想のボーカリスト・イデアル
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 509416
approved: False

level: 1
 type: PREEMPT
 | (47371:83) ふふっ + 盛り上がっていきましょう！
 | Absorb damage when combos <= 8 for 5 turns + For 20 turns, None% chance for combo orb skyfall.
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 3
   | (47374:143) まだまだいきます！
   | For 1 turn, 50% ATK for ???, Deal 85% damage
 | group:
 | condition: execute repeatedly, turn 2 of 3
   | (47375:46) テンションあげていきますよっ！
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 80% damage
 | group:
 | condition: execute repeatedly, turn 3 of 3
   | (47376:151) もう息切れしちゃいましたか？
   | No description set, Deal 90% damage