#306240 - Reincarnated Date Masamune
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 306240
approved: False

level: 1
 type: PREEMPT
 | (40041:83) Sacrificing Sword Stroke + Sendai Bamboo Protective Crest + "Saddle Cutter" Kagehide
 | Increase damage to 300% for the next 1 turn + No description set + Blind random 7~10 orbs for 1 turn, Deal 100% damage
 group:
 | group:
   | group:
   | condition: turn 1 while HP > 2
     | (40046:83) Spirit of the One-Eyed God + Sendai Bamboo Protective Crest + "Great Kulika"
     | Voids status ailments for 999 turns + No description set + Void damage >= 500,000,000 for 6 turns
   | group:
   | condition: turns 2-6 while HP > 2
     | condition: 50% chance
     | (40050:83) "New Life" Masamune + "Great Kulika" Hiromitsu
     | Change the 1st row to Wood orbs + No description set, Deal 150% damage
     | condition: 50% chance
     | (40053:83) "New Life" Masamune + "Great Kulika" Hiromitsu
     | Change the 5th row to Wood orbs + Lock all Wood orbs, Deal 150% damage
   | group:
     | (39739:83) Azure Sky Surge + Thousandfold Demise
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turns 1-6, hp <= 2
   | (40045:15) "Candlestick Cutter" Mitsutada
   | Deal 1,000% damage (2 hits, 500% each)