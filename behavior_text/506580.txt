#506580 - PEMDra (3 Pulls)
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 506580
approved: False

level: 1
 type: PREEMPT
 | (7947:74) Pull!
 | Reduce damage from all sources by 75% for 1 turn
 group:
 | (7951:66) Its arm snaps back into place
 | Do nothing