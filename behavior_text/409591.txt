#409591 - Ermes Costello & Smack
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 409591
approved: False

level: 1
 type: PREEMPT
 | (43813:139) Ushaaaa!
 | Haste both leaders' skills by 15 turns
 group:
 | group:
   | condition: 50% chance
   | (43820:15) Kick
   | Deal 101% damage
   | (43821:92) I found you!
   | Spawn 1 random Fire and Wood orb, Deal 100% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: execute repeatedly, turn 1 of 2
     | (43814:83) Place a sticker. + Create a copy of the object.
     | Seal the 1st column for 1 turn + Specific orbs change every 1.0s for 1 turn, Deal 101% damage
   | group:
   | condition: execute repeatedly, turn 2 of 2
     | (43817:83) Peel the sticker. + It causes a fissure and you can destroy it.
     | No description set + Deal 102% damage (3 hits, 34% each), Deal 102% damage (3 hits, 34% each)