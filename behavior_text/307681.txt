#307681 - Mega Awoken Goddess of Power, Kali's Gem
monster size: 4
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 307681
approved: False

level: 1
 type: PASSIVE
 | (37038:129) Super Resolve
 | Damage which would reduce HP from above 50% to below 50% is nullified
 | (37019:72) Light reduced
 | Reduce damage from Light attrs by 50%
 type: PREEMPT
 | (37362:92) Dark Shining Gem
 | Spawn 15 random Dark orbs
 group:
 | group:
   | (37363:46) Aurora Flash
   | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage
 | group:
 | condition: turn 1, hp <= 49
   | (42523:143) Mystic Light Source
   | For 3 turns, 1% ATK for 1 random card, Deal 200% damage
 type: REMAINING
 condition: when 1 enemies remain, always turn 1
 | (37364:17) Gem's Miracle
 | Increase damage to 1,000% for the next 999 turns