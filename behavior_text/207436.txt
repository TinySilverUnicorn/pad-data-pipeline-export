#207436 - 海馬瀬人＆青眼の究極竜
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 207436
approved: False

level: 1
 type: PASSIVE
 | (47146:118) 体力半減
 | Reduce damage from Physical types by 50%
 type: PREEMPT
 | (47140:83) デュエル！ + 罠カード「最終突撃命令」
 | Voids status ailments for 999 turns + Lock 7 random orbs, Deal 100% damage
 group:
 | condition: 33% chance
 | (47143:15) アルティメット・バースト
 | Deal 102% damage (3 hits, 34% each)
 | condition: 50% chance
 | (47144:105) 貴様を葬り去る
 | RCV 50% for 1 turn, Deal 100% damage
 | (47145:108) アルティメット・ドラゴンの攻撃
 | Change all Wood orbs to Water orbs, Deal 100% damage