#500520 - Snow Globe Dragon Rouge
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 500520
approved: False

level: 1
 type: PREEMPT
 | (42979:83) Flame Gift + Chimney Flames
 | RCV 25% for 1 turn + Heal skyfall +25% for 1 turn
 group:
 | (42982:85) Gift Purification
 | Change all orbs to Fire, Heal, and Jammer, Deal 100% damage