#805067 - Zela's Magical Hat, Smokk
monster size: 5
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 805067
approved: False

level: 1
 group:
 | group:
 | condition: turns 1-5
   | condition: 40% chance
   | (37575:50) Howling Fear
   | Player -50% HP
   | condition: 60% chance
   | (37576:4) Wood Storm
   | Change a random attribute to Wood orbs
 | group:
   | (37574:92) Wood Storm
   | Spawn 10 random Wood orbs, Deal 100% damage