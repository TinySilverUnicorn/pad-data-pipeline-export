#300516 - Duke Vampire Lord
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 300516
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (45630:83) Protection of Darkness + Duke's Armor + Bloody Cloud
 | Voids status ailments for 999 turns + Void damage >= 1,000,000,000 for 4 turns + A 3×1 rectangle of clouds appears for 4 turns at a random location, Deal 130% damage
 group:
 | group:
   | group:
   | condition: turns 1-3 while HP > 2
     | condition: 50% chance
     | (45636:83) Blood Spear + Draining Fangs
     | Blind random 7 orbs for 1 turn + Enemy recover 10% HP, Deal 100% damage
     | condition: 50% chance
     | (45639:83) Blood Spear + Draining Fangs
     | No description set + Enemy recover 5%~15% HP, Deal 80% damage
   | group:
   | condition: turn 4 while HP > 2
     | (45635:138) Power of Darkness
     | Absorb damage when damage >= 1,000,000,000 for 3 turns
   | group:
   | condition: turns 5-6 while HP > 2
     | condition: 50% chance
     | (45636:83) Blood Spear + Draining Fangs
     | Blind random 7 orbs for 1 turn + Enemy recover 10% HP, Deal 100% damage
     | condition: 50% chance
     | (45639:83) Blood Spear + Draining Fangs
     | No description set + Enemy recover 5%~15% HP, Deal 80% damage
   | group:
     | (45532:83) Wave of a Trillion Evil Omens + Doom of a Trillion Catastrophes
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turns 1-6, hp <= 2
   | (45634:15) Bloody Nightmare
   | Deal 1,000% damage (5 hits, 200% each)