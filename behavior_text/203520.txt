#203520 - Sly Masquerade Demon Princess, Mel
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 203520
approved: False

level: 1
 type: PREEMPT
 | (38851:105) Healing Cut
 | RCV 25% for 5 turns
 group:
 | (38852:92) Mocking Sly Masquerade
 | Spawn 9 random Heal orbs, Deal 100% damage