#403288 - Alt. Frostmare
monster size: 2
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 403288
approved: False

level: 1
 type: PREEMPT
 | (41500:94) Freezing Thorns
 | Lock all orbs
 group:
 | group:
 | condition: turn 1
   | (41501:96) Powder Snow
   | Locked random skyfall +100% for 2 turns, Deal 93% damage
 | group:
   | (41502:15) Nightmare Barrage
   | Deal 88% damage (4 hits, 22% each)
 type: REMAINING
 condition: when 1 enemies remain, hp <= 100
 | group:
 | condition: turn 1
   | (41503:89) Glacial Solitude
   | Delay active skills by 5 turns, Deal 100% damage
 | group:
 | condition: turn 2
   | (41501:96) Powder Snow
   | Locked random skyfall +100% for 2 turns, Deal 93% damage