#9165 - ニコ・ロビン
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0
monster_id: 9165
approved: False

level: 1
 type: PREEMPT
 | (40690:83) 私の前では + スピードは無力…!!
 | ATK -50% for 1 turn + Delay active skills by 2 turns, Deal 70% damage
 group:
 | group:
 | condition: turn 1
   | (40693:83) 受け流す蝶の花 + 千紫万紅 胡蝶蘭!!!
   | Absorb damage when combos <= 8 for 1 turn + Void damage >= 500,000,000 for 1 turn
 | group:
 | condition: turn 2
   | (40696:83) すぐ終わるわ + 大人しくして…
   | Blind all orbs on the board + ATK -75% for 2 turns, Deal 75% damage
 | group:
 | condition: turn 3
   | (40699:83) 体咲き!! + W・クラッチ!!!
   | Bind active skills for 5 turns + Deal 120% damage (2 hits, 60% each), Deal 120% damage (2 hits, 60% each)
 | group:
 | condition: execute repeatedly, turn 1 of 2
   | (40702:109) 九輪咲き ツイスト!!
   | Random 3 orbs change every 1.0s for 1 turn
 | group:
 | condition: execute repeatedly, turn 2 of 2
   | (40703:83) 四十輪咲き!! + 四本樹!!
   | Change the 1st and 2nd columns to Wood and Dark orbs + Lock all orbs, Deal 50% damage