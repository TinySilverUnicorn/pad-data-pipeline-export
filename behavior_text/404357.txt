#404357 - Manticore
monster size: 5
new AI: True
start/max counter: 15
counter increment: 0
monster_id: 404357
approved: False

level: 1
 type: PASSIVE
 | (42621:129) Super Resolve
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (42627:83) Hard Shell's Protection + Beastly Wingbeats
 | Voids status ailments for 3 turns + Change board size to 5x4 for 4 turns
 type: DEATH
 condition: when defeated
 | (42635:95) Poisonous Sting Attack
 | Spawn 6 random Mortal Poison orbs
 group:
 | group:
 | condition: turn 1
   | (42631:15) Beastly Claws
   | Deal 100% damage (2 hits, 50% each)
 | group:
 | condition: turn 2
   | (42632:15) Beastly Claws
   | Deal 100% damage (2 hits, 50% each)
 | group:
 | condition: turn 3
   | (42633:17) Combustion
   | Increase damage to 1,000% for the next 999 turns
 | group:
 | condition: turns 4-6
   | (42634:15) Beastly Claws
   | Deal 100% damage (2 hits, 50% each)
 | group:
   | (42622:83) Longevity Surge + Universal Demise
   | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)