#303061 - Destruction Cannon Mechanical Star God, Castor
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 303061
approved: False

level: 1
 type: PASSIVE
 | (42619:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 | (44517:72) Light Reduced
 | Reduce damage from Light attrs by 50%
 type: PREEMPT
 | (44553:83) Mechanical Destruction Star of Gemini + Meteor Shower of the Stars + Twin Star Shadow
 | Voids status ailments for 999 turns + Jammer skyfall +20% for 3 turns + Void damage >= 1,000,000,000 for 6 turns, Deal 100% damage
 group:
 | group:
   | group:
   | condition: turns 1-5 while HP > 49
     | condition: 50% chance
     | (44559:83) Mechanical Star God's Tactics + Astrocharge - Dark
     | No skyfall for 1 turn + Change all Heal, Jammer, and Poison orbs to Dark orbs, Deal 100% damage
     | condition: 50% chance
     | (44562:83) Mechanical Star God's Tactics + Astrocharge - Darkness
     | No description set + Change a random attribute to Dark orbs, Deal 100% damage
   | group:
     | (44524:83) Star Machine's Wave + Apocalyptic Star Crushing
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turn 1 while HP > 2, hp <= 49
   | (44558:109) Dark Hole
   | Random 2 orbs change every 0.5s for 3 turns, Deal 150% damage
 | group:
 | condition: turns 1-5, hp <= 2
   | (44557:15) Astrocharge - Darkness
   | Deal 1,050% damage (3 hits, 350% each)