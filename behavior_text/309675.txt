#309675 - Grim Keeper
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 309675
approved: False

level: 1
 type: PREEMPT
 | (45009:68) Venom Mist
 | Mortal Poison skyfall +15% for 3 turns
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 2
   | (45010:15) Normal Attack
   | Deal 94% damage (2 hits, 47% each)
 | group:
 | condition: execute repeatedly, turn 2 of 2
   | (45012:62) Lingering Malhaze
   | Blind all orbs on the board, Deal 100% damage