#206520 - Panacean Peacock Queen, Yurisha
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 206520
approved: False

level: 1
 type: PASSIVE
 | (44023:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 type: PREEMPT
 | (44019:83) Chaos' Feather + Peacock Queen's Domain
 | Movetime 50% for 5 turns + Change board size to 7x6 for 2 turns
 group:
 | (44022:85) Panacean Formation
 | Change all orbs to Fire, Water, Wood, Light, Dark, Heal, Jammer, and Poison, Deal 100% damage