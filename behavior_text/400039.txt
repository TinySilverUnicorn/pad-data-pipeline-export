#400039 - Alt. Big Bubblie
monster size: 2
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 400039
approved: False

level: 1
 group:
 condition: execute repeatedly, turn 2 of 2, hp <= 100
 | (45505:46) Big Lil' Flash
 | Change own attribute to random one of Fire, Water, Wood, Light, or Dark, Deal 100% damage