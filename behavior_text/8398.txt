#8398 - Super Earth Knight
monster size: 4
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 8398
approved: False

level: 1
 type: PASSIVE
 | (41858:106) Enemy's next turn changed
 | Enemy turn counter change to 3 when HP <= 50%
 | (41859:129) Super Resolve 50%
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (41841:83) Earth Knight's Willpower + Knight Magic
 | Voids status ailments for 999 turns + Unable to match Fire orbs for 1 turn
 group:
 | group:
   | condition: 17% chance
   | (41852:74) Armor of the Knight
   | Reduce damage from all sources by 75% for 1 turn, Deal 100% damage
   | condition: 17% chance
   | (41853:107) Knight Magic
   | Unable to match Fire orbs for 1 turn, Deal 101% damage
   | condition: 17% chance
   | (41854:107) Knight Magic
   | Unable to match Water orbs for 1 turn, Deal 101% damage
   | condition: 17% chance
   | (41855:107) Knight Magic
   | Unable to match Wood orbs for 1 turn, Deal 101% damage
   | condition: 16% chance
   | (41856:107) Knight Magic
   | Unable to match Light orbs for 1 turn, Deal 100% damage
   | condition: 16% chance
   | (41857:107) Knight Magic
   | Unable to match Dark orbs for 1 turn, Deal 100% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: turn 1
     | (41844:83) Armor of the Knight + Earth Knight's Prayer + Earth Impact
     | Reduce damage from all sources by 50% for 3 turns + No description set + Change all orbs to Wood, Deal 110% damage
   | group:
     | (41848:83) Wave of the Knight + Guard Break - Wood
     | Bind awoken skills for 1 turn + Deal 770% damage (7 hits, 110% each), Deal 770% damage (7 hits, 110% each)