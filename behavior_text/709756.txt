#709756 - ジ・O
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 709756
approved: False

level: 1
 type: PREEMPT
 | (46747:83) プレッシャー + 不愉快だな………この感覚は！
 | Delay active skills by 1 turn + No description set
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 3
   | (46750:100) 道を誤ったのだよ！貴様は！！
   | Seal the 1st row for 1 turn
 | group:
 | condition: execute repeatedly, turn 2 of 3
   | (46751:85) 墜ちろ！カトンボッ！！
   | Change all orbs to Light and Dark, Deal 100% damage
 | group:
 | condition: execute repeatedly, turn 3 of 3
   | (46752:110) ビーム・ソード
   | Specific orbs change every 1.0s for 1 turn, Deal 105% damage