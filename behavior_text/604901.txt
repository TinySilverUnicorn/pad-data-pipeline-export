#604901 - Alt. Super Baddie
monster size: 0
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 604901
approved: False

level: 1
 type: PREEMPT
 | (40459:143) Darkening Up
 | For 3 turns, 1% ATK for 2 random cards
 group:
 | (40461:143) Darkening Up
 | For 3 turns, 1% ATK for 2 random cards, Deal 100% damage