#403656 - Nargacuga
monster size: 5
new AI: True
start/max counter: 7
counter increment: 0
monster_id: 403656
approved: False

level: 1
 type: PASSIVE
 | (42296:72) Dark Halved
 | Reduce damage from Dark attrs by 50%
 type: PREEMPT
 | (42282:83) Menace + Tail Slam
 | Voids status ailments for 999 turns + Change all orbs to Dark
 group:
 | group:
 | condition: always turn 1
   | (42285:83) Its tail is stuck and can't move! + Enraged
   | Do nothing + Increase damage to 150% for the next 2 turns
 | group:
   | condition: 33% chance
   | (42293:15) Cutwing
   | Deal 102% damage (3 hits, 34% each)
   | condition: 50% chance
   | (42294:62) Springing Strike
   | Blind all orbs on the board, Deal 101% damage
   | (42295:15) Roar
   | Deal 100% damage
 | group:
 | condition: hp <= 49
   | group:
   | condition: turn 2
     | (42288:83) Tail Sweep + Spiked Tail Slam
     | Change the 3rd row to Dark orbs + Deal 51% damage (3 hits, 17% each), Deal 52% damage
   | group:
     | condition: 50% chance
     | (42291:74) Dash
     | Reduce damage from all sources by 75% for 1 turn, Deal 100% damage
     | (42292:92) Tailspike Shot
     | Spawn 3 random Jammer orbs, Deal 100% damage