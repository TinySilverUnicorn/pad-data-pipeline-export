#302924 - True Rebellious Demon, Oda Nobunaga
monster size: 5
new AI: True
start/max counter: 15
counter increment: 0
monster_id: 302924
approved: False

level: 1
 type: PASSIVE
 | (39730:72) Light reduced
 | Reduce damage from Light attrs by 50%
 type: PREEMPT
 | (40101:83) Demon's Unifying Force + Dragon Scale Armor + The Hour of Destruction
 | Voids status ailments for 999 turns + Void damage >= 500,000,000 for 4 turns + No description set
 group:
 | group:
 | condition: turn 1
   | (40105:83) Demonic Dragon Guns + One Gunpoint Dragon lit up
   | No description set + Lock 15 random orbs
 | group:
 | condition: turn 2
   | (40108:83) Demonic Dragon Wings + Every Gunpoint Dragon lit up
   | No description set + Lock all orbs
 | group:
 | condition: turn 3
   | (40111:83) Demonic Dragon Release + Great power is being built up
   | Delay active skills by 4 turns + Increase damage to 500% for the next 999 turns
 | group:
   | (40114:15) Dragons' Magic Bullets
   | Deal 300% damage (6 hits, 50% each)