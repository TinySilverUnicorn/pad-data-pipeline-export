#705066 - Reeche's Magical Hat, Frow
monster size: 5
new AI: True
start/max counter: 0
counter increment: 0
monster_id: 705066
approved: False

level: 1
 group:
 | group:
 | condition: turns 1-5
   | condition: 40% chance
   | (37569:1) Fragile Bind
   | Bind 1 random card for 2 turns
   | condition: 60% chance
   | (37570:4) Fragile Aqua
   | Change a random attribute to Water orbs
 | group:
   | (37568:92) Fragile Aqua
   | Spawn 10 random Water orbs, Deal 100% damage