#404999 - Alt. Scholarly God of Treasure, Ganesha
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 404999
approved: False

level: 1
 type: PASSIVE
 | (44177:129) ＿Ganesha 50 Super Resolve
 | Damage which would reduce HP from above 50% to below 50% is nullified
 type: PREEMPT
 | (44178:83) It's finally here! A new year! + Another year of overflowing treasure!
 | Reduce damage from all sources by 50% for 3 turns + Void damage >= 2,000,000,000 for 3 turns, Deal 50% damage
 group:
 | group:
   | group:
   | condition: execute repeatedly, turn 1 of 3
     | (44184:83) Make it rain a thousand coins! + Fortune Roulette!
     | Change all orbs to Light + Specific orbs change every 1.0s for 1 turn, Deal 80% damage
   | group:
   | condition: execute repeatedly, turn 2 of 3
     | (44187:83) Business will boom this year! + Loosen those purse strings!
     | Change board size to 7x6 for 1 turn + No description set, Deal 90% damage
   | group:
   | condition: execute repeatedly, turn 3 of 3
     | (44190:83) To a long life of abundant wealth! + Lift your lucky mallet to the heavens!
     | Delay both leaders' skills by 2 turns + Deal 99% damage (3 hits, 33% each), Deal 99% damage (3 hits, 33% each)
 | group:
 | condition: turn 1, hp <= 49
   | (44181:83) It's only just begun! + Discard all wicked thoughts!
   | Absorb damage when combos <= 8 for 3 turns + Mortal Poison skyfall +15% for 3 turns, Deal 100% damage