#600178 - King Metal Dragon
monster size: 4
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 600178
approved: False

level: 1
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 4
   | (39034:66) Gya!
   | Do nothing
 | group:
 | condition: execute repeatedly, turn 2 of 4
   | (39035:66) Gya! Gya!
   | Do nothing
 | group:
 | condition: execute repeatedly, turn 3 of 4
   | (39036:66) Gya! Gya! Gya!
   | Do nothing
 | group:
 | condition: execute repeatedly, turn 4 of 4
   | (39023:50) Metal Attack
   | Player -150% HP