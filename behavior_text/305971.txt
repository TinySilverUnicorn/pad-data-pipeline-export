#305971 - Watchful Grove Dragon Healer, Alynna
monster size: 5
new AI: True
start/max counter: 127
counter increment: 0
monster_id: 305971
approved: False

level: 1
 type: PASSIVE
 | (39629:73) Resolve
 | Survive attacks with 1 HP when HP > 1%
 type: PREEMPT
 | (39518:83) Wish for Peace + Nature's Grove + Here I gooo!
 | Voids status ailments for 999 turns + Bind awoken skills for 4 turns + Void damage >= 500,000,000 for 999 turns
 group:
 | group:
 | condition: turn 1
   | (39527:83) Light Butterflies + Nature's Call
   | Change all orbs to Fire, Water, Wood, Light, Dark, and Heal + Unable to match Fire orbs for 1 turn, Deal 100% damage
 | group:
 | condition: turn 2
   | (39530:83) Light Butterflies + Nature's Call
   | Change all orbs to Fire, Water, Wood, Light, Dark, and Heal + Unable to match Water orbs for 1 turn, Deal 125% damage
 | group:
 | condition: turn 3
   | (39533:83) Light Butterflies + Nature's Call
   | Change all orbs to Fire, Water, Wood, Light, Dark, and Heal + Unable to match Wood orbs for 1 turn, Deal 150% damage
 | group:
 | condition: turn 4
   | (39536:83) Light Butterflies + Nature's Call
   | Change all orbs to Fire, Water, Wood, Light, Dark, and Heal + Unable to match Light orbs for 1 turn, Deal 175% damage
 | group:
 | condition: turn 5
   | (39539:83) Light Butterflies + Nature's Call
   | Change all orbs to Fire, Water, Wood, Light, Dark, and Heal + Unable to match Dark orbs for 1 turn, Deal 200% damage
 | group:
 | condition: turn 6
   | (39522:83) Oh my...! + I'll do my best! + Butterfly Guard
   | RCV 50% for 5 turns + Bind awoken skills for 1 turn + Reduce damage from all sources by 75% for 1 turn
 | group:
   | (39526:15) Nature's Forest
   | Deal 1,000% damage (10 hits, 100% each)