#202907 - the norn urd
monster size: 5
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 202907
approved: False

level: 1
 type: PASSIVE
 | (40283:73) ＿Norn Resolve
 | Survive attacks with 1 HP when HP > 50%
 | (40284:72) ＿Urd Fire Water Halved
 | Reduce damage from Fire and Water attrs by 50%
 type: PREEMPT
 | (40285:137) space-time barrier
 | Void damage >= 2,000,000,000 for 5 turns, Deal 50% damage
 group:
 | group:
 | condition: execute repeatedly, turn 1 of 2
   | (40286:6) dimension extinction
   | Voids player buff effects, Deal 70% damage
   | (40287:89) tales of yore
   | Delay active skills by 4 turns, Deal 90% damage
 | group:
 | condition: execute repeatedly, turn 2 of 2
   | (40286:6) dimension extinction
   | Voids player buff effects, Deal 70% damage
   | (40288:77) space-time tuning
   | Change the 1st column to Fire orbs and the 6th column to Water orbs, Deal 100% damage