#205625 - Halloween Speciality, Pumpkin Devil
monster size: 4
new AI: True
start/max counter: 1
counter increment: 0
monster_id: 205625
approved: False

level: 1
 type: PREEMPT
 | (41408:83) Pumpkin Devil Drain + Your gaze is captivated...
 | Absorb Fire, Water, and Wood damage for 3 turns + Forces attacks to hit this enemy for 99 turns
 type: REMAINING
 condition: when 1 enemies remain, turn 1, hp <= 100
 | (41411:52) Resurrecting Light
 | Enemy ally recover 100% HP