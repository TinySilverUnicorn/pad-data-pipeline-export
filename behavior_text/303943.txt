#303943 - Violent Destruction Arm Dragonbound, No.6
monster size: 5
new AI: True
start/max counter: 3
counter increment: 0
monster_id: 303943
approved: False

level: 1
 type: PASSIVE
 | (34043:73) Resolve
 | Survive attacks with 1 HP when HP > 50%
 | (42574:106) Enemy's next turn changed
 | Enemy turn counter change to 1 when HP <= 50%
 | (42519:118) ＿God Type halved
 | Reduce damage from God types by 50%
 type: PREEMPT
 | (42550:83) Violent Dragon's Protection + Eternal Dragon Control - Vand + Blazing Inferno
 | Voids status ailments for 999 turns + Absorb Fire and Water damage for 4 turns + Unable to match Wood orbs for 4 turns, Deal 150% damage
 group:
 | group:
   | group:
   | condition: turns 1-6 while HP > 49
     | (42554:85) Innocent Inferno Flare
     | Change all orbs to Fire, Deal 1,000% damage
   | group:
     | (34044:83) Cosmic Wave + Cosmic Destruction
     | Bind awoken skills for 1 turn + Deal 5,000% damage (5 hits, 1,000% each), Deal 5,000% damage (5 hits, 1,000% each)
 | group:
 | condition: turn 1, hp <= 49
   | (42555:83) Inferno Power + Flame Burned
   | Increase damage to 1,000% for the next 999 turns + No description set